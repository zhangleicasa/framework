/OS=$(shell uname)$(shell uname -a | sed -n '1p' | perl -nle 'print $$1 if /\s+([0-9]\.\d+)/')
GCC=$(shell gcc --version | sed -n '1p' | perl -nle 'print $$1 if /\s+([0-9]\.\d+)/')
VER_PT=$(shell bit=`getconf LONG_BIT`;if [ $$bit -eq 64 ];  then echo 'X86-64'; else echo 'X86'; fi;)

OS=$(shell uname)$(shell uname -a | sed -n '1p' | perl -nle 'print $$1 if /\s+([0-9]\.\d+)/')
GCC=$(shell gcc --version | sed -n '1p' | perl -nle 'print $$1 if /\s+([0-9]\.\d+)/')
CC=/usr/bin/g++  
VER=1.0.0

TF=./ts
UT=./unittest

DIR_LIST = $(TF) $(TF)/event $(TF)/mem $(TF)/net $(TF)/thirds $(TF)/common $(TF)/sys $(TF)/sql $(UT)/ts/common 

DIR_LIST += $(UT)/ts

OutPut=build

NEW_CODE_PATH=./

#SOURCE CODE
CC_SRC=$(shell find  $(DIR_LIST)   -name "*.cc" )
CC_SRC2=$(shell find  $(DIR_LIST)   -name "*.c" )
CC_SRC3=$(shell find  $(DIR_LIST)   -name "*.cpp" )

#OBJECTS
CC_OBJS=$(patsubst %.cc,./$(OutPut)/%.o,$(CC_SRC))
CC_OBJS2=$(patsubst %.c,./$(OutPut)/%.o,$(CC_SRC2))
CC_OBJS3=$(patsubst %.cpp,./$(OutPut)/%.o,$(CC_SRC3))
OBJS=$(CC_OBJS)
OBJS2=$(CC_OBJS2)
OBJS3=$(CC_OBJS3)
#DEPS
DEPS=$(patsubst %.o,%.d,$(OBJS))

define OBJ_MKDIR
  OBJ_DIRS+=./$(OutPut)/$(1)
endef
CC_DIRS=$(shell find $(DIR_LIST) -type d|sed -e '/.svn/d')
$(foreach dir,$(CC_DIRS),$(eval $(call OBJ_MKDIR,$(dir))))


#DEPS
DEPS=$(patsubst %.o,%.d,$(OBJS))
INC_DIR=
#INCLUDE DIR
define SAFE_MKDIR
  INC_DIR+=-I $(1)
endef
$(foreach dir,$(CC_DIRS),$(eval $(call SAFE_MKDIR,$(dir))))

# add include dir
INC_DIR+=-I/usr/include  
INC_DIR+=-I/home/zhanglei_bak/mysql/install/include
#add link dir
LIB_DIR= -L/usr/local/lib -L/home/zhanglei_bak/mysql/install/lib

#linked libraries -ltcmalloc
LIBS=-lc -lm -lz -lrt -lpthread -lgtest -lglog -lgflags -Wall -ldl -lmysqlclient -fno-elide-constructors

DFLAGS=-DLINUX -DGCC -DDEBUG
LDFLAGS=$(LIB_DIR) $(LIBS)
CPPFLAGS=$(INC_DIR) $(DFLAGS)  -DTIXML_USE_STL -march=native
CFLAGS= -fPIC -O0 -std=c++11

#target name
EXE1=exe

all:$(EXE1)
$(shell mkdir -p $(sort $(OBJ_DIRS)))
include $(DEPS)

$(EXE1):$(OBJS) $(OBJS2) $(OBJS3) 
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)


./$(OutPut)/%.o:%.cc
	$(CC) -o $@ $(CFLAGS) -c $< $(CPPFLAGS)
./$(OutPut)/%.d:%.cc
	@set -e; rm -f $@; \
	$(CC) -MM $(CPPFLAGS) $< > $@.$$$$; \
	sed 's,.*\.o[ :]*,$(patsubst %.d,%.o,$@) $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$

./$(OutPut)/%.o:%.cpp
	@echo $@
	$(CC) -o $@ $(CFLAGS) -c $< $(CPPFLAGS)
./$(OutPut)/%.d:%.cpp
	@set -e; rm -f $@; \
	$(CC) -MM $(CPPFLAGS) $< > $@.$$$$; \
	sed 's,.*\.o[ :]*,$(patsubst %.d,%.o,$@) $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$

./$(OutPut)/%.o:%.c
	$(CC) -o $@ $(CFLAGS) -c $< $(CPPFLAGS)
./$(OutPut)/%.d:%.c
	@set -e; rm -f $@; \
	$(CC) -MM $(CPPFLAGS) $< > $@.$$$$; \
	sed 's,.*\.o[ :]*,$(patsubst %.d,%.o,$@) $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$


clean:
	rm -Rf $(OutPut)
	rm -rf $(EXE1)

test:
	./exe

codelen:
	find $(NEW_CODE_PATH) \( -name "*.cc"  -name "*.cpp" -o -name "*.h" -o -name "*.c" \) -exec cat {} \;|sed -e 's/\"/\n\"\n/g;s/\([^\/]\)\(\/\*\)/\1\n\2\n/g;'|sed  -e '/^\"/{:a;N;/\".*\"/!ba;s/\".*\".*//;N;/\"/!d;b}' -e '/^\/\*/{s/\/\*.*\*\///;/\/\*/{:b;N;/\/\*.*\*\//!bb;s/\/\*.*\*\///}}' -e 's/\/\/.*//g' |sed -e '/^[[:space:]]*$$/d'|wc -l
srczip:
	zip -r ./$(EXE1)_src_$(VER).zip * -x *.o *.d *.svn *.zip *.a *.so $(EXE1) *.svn-work *.svn-base *.so.* *.d.* *.svn

