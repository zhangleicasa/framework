#include <gtest/gtest.h>
#include "controller.h"

#ifdef TEST_SQL_PARSER

#include "public.h"
#include "ast_node.h"
#include "sql_parser.h"

#include <glog/logging.h>
#include <glog/raw_logging.h>

#ifndef DEBUG
void main() {
#else
TEST(TS_SQL_PARSER, Run){
#endif
	printf("select * from t2, t3 where t2.a = t3.a and t2.b < 4");
	Parser* my_parser = new Parser("select t2.a, t3.a from t2, t3 where t2.a = t3.a\n");
	AstNode* my_ast = my_parser->GetRawAST();
	my_ast->Print();
}

#endif
