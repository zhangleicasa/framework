#include <gtest/gtest.h>
#include "controller.h"

#include "common/xml_parser.h"
#include "nginx_access_log_scanner.h"

#ifdef TEST_ACCESS_LOG_SCANNER

TEST(Scanner, Run)
{
    ts::common::xml_parser::get_instance()->parse_file("./data/person.xml");
    const char *access_log_path = ts::common::xml_parser::get_instance()->get_parameter("path").c_str();
    ts::db::nginx_access_log_scanner *scanner = new ts::db::nginx_access_log_scanner(access_log_path);
    scanner->init();
    while(1)
    {
    	scanner->scan_access_log();
    }

}

#endif
