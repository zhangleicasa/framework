#include <gtest/gtest.h>
#include "controller.h"

#include "type_cast_func.h"
#include "schema.h"
#include "column_type.h"
#include "time_helper.h"

using namespace ts::data;
using namespace ts::expr;

#ifdef TEST_SCHEMA

#ifndef DEBUG
void main() {
#else
TEST(db_schema, Run) {
#endif

	/**
	 * inctrade:
	 * 		1.  TradingDay                      string         8+1        0
	 * 		2.  SettlementGroupID               string         8+1        8
	 * 		3.  SettlementID                    int                       16
	 * 		4.  TradeID                         string         12+1       20
	 * 		5.  Direction                       char           1          32
	 * 		6.  OrderSysID                      string         12+1       33
	 * 		7.  ParticipantID                   string         10+1       45
	 * 		8.  ClientID                        string         10+1       55
	 * 		9.  TradingRole                     char           1          65
	 * 		10. AccountID                       string         12+1       66
	 * 		11. InstrumentID                    string         30+1       78
	 * 		12. OffsetFlag                      char           1          108
	 * 		13. HedgeFlag                       char           1          109
	 * 		14. Price                           double                    110
	 * 		15. Volume                          int                       118
	 * 		16. TradeTime                       string         8+1        122
	 * 		17. TradeType                       char           1          130
	 * 		18. PriceSource                     char           1          131
	 * 		19. UserID                          string         15+1       132
	 * 		20. OrderLocalID                    string         12+1       147
	 * 		21. ClearingPartID                  string         10+1       159
	 * 		22. BusinessUnit                    string         20+1       169
	 * 		23. ClientHedgeFlag                 char           1          189
	 * 		24. TradeSource                     char           1          190
	 * 		25. PriceEx                         double                    191
	 * 		26. TID                             int                       199
	 * **/

	std::vector<column_type> columns;
	columns.push_back(column_type(t_string, 8));
	columns.push_back(column_type(t_string, 8));
	columns.push_back(column_type(t_int));
	columns.push_back(column_type(t_string, 12));
	columns.push_back(column_type(t_string, 1));
	columns.push_back(column_type(t_string, 12));
	columns.push_back(column_type(t_string, 10));
	columns.push_back(column_type(t_string, 10));
	columns.push_back(column_type(t_string, 1));
	columns.push_back(column_type(t_string, 12));
	columns.push_back(column_type(t_string, 30));
	columns.push_back(column_type(t_string, 1));
	columns.push_back(column_type(t_string, 1));
	columns.push_back(column_type(t_double));
	columns.push_back(column_type(t_int));
	columns.push_back(column_type(t_string, 8));
	columns.push_back(column_type(t_string, 1));
	columns.push_back(column_type(t_string, 1));
	columns.push_back(column_type(t_string, 15));
	columns.push_back(column_type(t_string, 12));
	columns.push_back(column_type(t_string, 10));
	columns.push_back(column_type(t_string, 20));
	columns.push_back(column_type(t_string, 1));
	columns.push_back(column_type(t_string, 1));
	columns.push_back(column_type(t_double));
	columns.push_back(column_type(t_int));

	schema *s = new schema(columns);
	s->show_offset();

}

#endif
