#include <gtest/gtest.h>
#include "controller.h"

#ifdef TEST_BUSINESS_DATA_FACTORY

#include "test_table.h"
#include "business_data_factories.h"

typedef struct student_info
{
	char student_name[12];
	int age;
}student_info;

#ifndef DEBUG
int main() {
#else
TEST(Business_db, Run) {
#endif
	ts::mem::memory_allocator * ar = new ts::mem::memory_allocator();
    ar->init();
	ts::db::business_data_factory<student_info> *p =
			new ts::db::business_data_factory<student_info>(16, 16, ar);
	student_info info;
	for(int i = 0; i < 32; ++i)
	{
		sprintf(info.student_name, "%d%d||||", i, i);
		info.age = i;
		p->inernal_insert(&info);
	}
	p->print_memory_layout();
}

#endif
