#include <gtest/gtest.h>
#include "controller.h"
#include "x_log.h"

#ifdef TEST_MYSQL_WRITER

TEST(MYSQL, Run)
{
#ifndef WIN32
	MYSQL *my_con = (MYSQL *)malloc(sizeof(MYSQL));
	MYSQL_RES *my_res;
	MYSQL_FIELD *my_field;
	MYSQL_ROW my_row;
	mysql_init(my_con);
	//建立连接
	my_con = mysql_real_connect(my_con, "127.0.0.1", "root", "", "test", 3309, NULL, CLIENT_FOUND_ROWS);
	if(NULL == my_con)
		ts::common::xlog::log(ts::common::LOG_ALL, "connect mysql failed.\n");
	else
		ts::common::xlog::log(ts::common::LOG_ALL, "connect mysql success.\n");
	int res;
//	//执行sql语句
//	res = mysql_query(my_con, "insert into test values(1, 1)");
//	if(0 != res)
//		common::xlog::log(LOG_ALL, "insert error.\n");
	res = mysql_affected_rows(my_con);
	res = mysql_query(my_con, "select * from test;");
	ts::common::xlog::log(ts::common::LOG_ALL, "result: %d\n", res);
	my_res = mysql_store_result(my_con);
	int rows = mysql_num_fields(my_res);
	printf("rows: %d\n", rows);

	my_field = mysql_fetch_fields(my_res);
	for(int i = 0; i < rows; i++)
		printf("%s\t\t", my_field[i].name);
//	printf("\n===============================\n");
	printf("\n");

	while(1)
	{
		my_row = mysql_fetch_row(my_res);
		if(NULL == my_row)
			break;
		for(int i = 0; i < rows; i++)
		{
			if(my_row[i] == NULL)
				printf("NULL\t");
			else
				printf("%s\t", my_row[i]);
		}
		printf("\n");
	}
#endif
}


#endif
