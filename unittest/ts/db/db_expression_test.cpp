#include <gtest/gtest.h>
#include "controller.h"

#ifdef TEST_DB_EXPRESSION

#include "schema.h"
#include "column_type.h"
#include "expression.h"
#include "type_cast_func.h"
#include "const_expression.h"
#include "column_expression.h"
#include "binary_expression.h"

using namespace ts::expr;
using namespace ts::data;
using namespace ts::mem;

#ifndef DEBUG
void main() {
#else
TEST(db_expression, Run) {
#endif
	////////////////////////////////////////////////////////////////////////////////////////////////////
	//simulate the buffer and schema.
	////////////////////////////////////////////////////////////////////////////////////////////////////
	std::vector<column_type> vec_columns;
	vec_columns.push_back(column_type(t_int));
	vec_columns.push_back(column_type(t_float));
	schema * s = new schema(vec_columns);
	buffer *p = new buffer(s->get_total_size() + 4);
	int a = 99;
	float b = 88.8;
	p->append((char *)&a, sizeof(int));       // void * buffer(8): |int|float| while ->
	p->append((char *)&b, sizeof(float));
	entity_desc *entity = new entity_desc(0, p, s);
	////////////////////////////////////////////////////////////////////////////////////////////////////


	/**init the expression prelude work. system level.**/
	init_type_banding_eval_func();
	init_type_cast_func();

	////////////////////////////////////////////////////////////////////////////////////////////////////
	//1. column expression must be defined as a type.
	//2. binary expression must be defined as a actual type, get type, expression type and operator type.
	////////////////////////////////////////////////////////////////////////////////////////////////////

	expression *column_expr = new column_expression(t_float, "order", "limit_price", entity);//todo: delete entity.
	expression *const_expr = new const_expression(t_int, "1");
	expression *add_expr = new binary_expression(t_float, t_float, t_expr_cal, t_add, const_expr, column_expr);
	expression *column_expr1 = new column_expression(t_int, "order", "volume", entity);//todo: delete entity.
	expression *add_expr1 = new binary_expression(t_float, t_float, t_expr_cal, t_add, add_expr, column_expr1);

	expression *const_expr1 = new const_expression(t_int, "197");
	expression *comp_expr = new binary_expression(t_boolean, t_float, t_expr_cmp, t_large_then, add_expr1, const_expr1);

    //expression: 1(int) + order.limit_price(float) + order.volume(int)
//	add_expr1->output();// output for debugging.
//	getchar();
//	add_expr1->init_expression_execution(t_float);// init the execution expression.
//	while(true)//event
//	{
//		printf("eval: %.2f\n", *(float *)(add_expr1->evaluate(entity)));
//	}

	//expression: 1(int) + order.limit_price(float) + order.volume(int) > 199
	comp_expr->init_expression_execution(t_boolean);
//	while(true)
//	{
//		printf("eval: %d\n", *(bool *)(comp_expr->evaluate(entity)));
//	}

}

#endif
