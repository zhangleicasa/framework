#include <gtest/gtest.h>
#include "controller.h"

#include "type_cast_func.h"
#include "schema.h"
#include "column_type.h"
#include "time_helper.h"
#include "data_processing.h"

using namespace ts::data;
using namespace ts::expr;

#ifdef TEST_DATA_PROCESSING

#ifndef DEBUG
void main() {
#else
TEST(db_schema, Run) {
#endif

	unsigned long long tick = 0L;
	startTimer(&tick);
	data_processing *d = new data_processing("data/IncTrade.csv", "data/IncTrade.csv.p");
	d->open();
	printf("time: %f\n", getMilliSecond(tick));
}

#endif
