#include <gtest/gtest.h>

#include "time_helper.h"
#include "controller.h"
#include "rte_memcpy.h"
#include "ioperator.h"
#include "tablescan.h"
#include "thread.h"
#include "x_log.h"
#include "public.h"
#include <string>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <fcntl.h>

using ts::physical::ioperator;
using ts::physical::tablescan;
using ts::sys::thread;

#ifdef TEST_PYHSICAL_OPERATOR

typedef struct l_s_position
{
	int long_position;
	int short_position;
	l_s_position &operator +=(l_s_position &p)
	{
		this->long_position  += p.long_position;
		this->short_position += p.short_position;
		return *this;
	}
	l_s_position &operator -=(l_s_position &p)
	{
		this->long_position  -= p.long_position;
		this->short_position -= p.short_position;
		return *this;
	}
	l_s_position &operator =(l_s_position &p)
	{
		this->long_position  = p.long_position;
		this->short_position = p.short_position;
		return *this;
	}
	inline void reset_position()
	{
		this->long_position  = 0;
		this->short_position = 0;
	}
}l_s_position_;

static const int l_s_position_size = sizeof(l_s_position_);

typedef struct i_trade
{
	uint32_t partid;
	uint32_t clientid;
	uint32_t product;
	uint32_t instrument;
	uint32_t tradeid;

	char insname[16];

	i_trade() : partid(0), clientid(0), product(0), instrument(0), tradeid(0) {}

	bool compare(i_trade &p)
	{
		if(partid==p.partid && clientid==p.clientid && tradeid==p.tradeid /*&& product==p.product && instrument==p.instrument*/)
			return true;
		else
			return false;
	}

	void clear()
	{
		partid = 0;
	}

	i_trade& operator =(i_trade &p)
	{
		this->partid  = p.partid;
		this->clientid = p.clientid;
		this->product = p.product;
		this->instrument = p.instrument;
		this->tradeid = p.tradeid;
		return *this;
	}

	void debug()
	{
		printf("partid: %d, clientid: %d, tradeid: %d\n\n\n",partid, clientid, tradeid);
	}
}i_trade;

static const int i_trade_size = sizeof(i_trade);

static int offsets[256] = {0};
static int product[256][256] = {0};

typedef struct compare_key
{
	bool operator()(const i_trade &a, const i_trade &b) const
	{
		if(a.partid != b.partid)
			return a.partid < b.partid;
		if(a.clientid != b.clientid)
			return a.clientid < b.clientid;
		if(a.product != b.product)
			return a.product < b.product;
		if(a.instrument != b.instrument)
			return a.instrument < b.instrument;
		return false;
	}
}compare_key;

void add_long_pos(int &l, int &s, int vol)
{
	l = vol;
}
void del_long_pos(int &l, int &s, int vol)
{
	l = -vol;
}
void add_short_pos(int &l, int &s, int vol)
{
	s = vol;
}
void del_short_pos(int &l, int &s, int vol)
{
	s = -vol;
}
void (*pos_func[64][64])(int &l, int &s, int vol);

typedef struct begin_end
{
	int i;
	char *begin_;
	char *end_;
	int count_;

	char *trade_id_buf_;
	uint32_t trade_id_size_;
	begin_end(): i(0), begin_(0), end_(0), count_(0), trade_id_buf_(NULL), trade_id_size_(0){}
}begin_end;

static char *parse(char *buffer, i_trade &trade, int &l, int &s, i_trade &pre_trade, begin_end* be)
{

	int o = 11;
	int offset = 0;
	char *p = buffer;
	char *q = NULL;
	char volume[8] = "";
	char offsetflag;

	char direaction = p[29];

	trade.tradeid = 0;
	for(int i = 16; i < 28; i++)
	{
		if(p[i] != ' ')
			trade.tradeid = 10 * trade.tradeid + (p[i] - '0');
	}
	trade.partid = 0;
	for(int i = 44; i < 48; i++)
	{
		trade.partid = 10 * trade.partid + (p[i] - '0');
	}
	trade.clientid = 0;
	for(int i = 49; i < 57; i++)
	{
		trade.clientid = 10 * trade.clientid + (p[i] - '0');
	}
	trade.product = product[buffer[67]][buffer[68]];

	trade.instrument = 0;
	int ins_len = offsets[buffer[68]];
	int endpos = 67 + ins_len;
	for(int i = 69; i < endpos; i++)
	{
		trade.instrument = 10 * trade.instrument + (p[i] - '0');
	}
	rte_memcpy(trade.insname, p + 67, ins_len);
	trade.insname[ins_len] = '\0';

	offsetflag = p[68+offsets[buffer[68]]];
	char *v = strchr(&(p[72+offsets[buffer[68]]]), ',');

	int vol = 0;
	++v;
	while(*v != ',')
		vol = 10 * vol + (*v++ - '0');
	(*pos_func[direaction][offsetflag])(l, s, vol);

	if(trade.compare(pre_trade))
	{
		rte_memcpy(be->trade_id_buf_ + be->trade_id_size_, p+16, 12);
		(be->trade_id_buf_)[be->trade_id_size_+12] = '\n';
		be->trade_id_size_ += 13;
	}
	pre_trade = trade;

	return v;
}

#define THREAD_NUM 8
std::map<i_trade, l_s_position_, compare_key> results[THREAD_NUM];
//std::vector<uint32_t> trade_ids[THREAD_NUM];
static char** trade_ids_;


typedef struct off_info
{
	uint32_t offset;
	uint32_t id;
	off_info(uint32_t off = 0, uint32_t i = 0) : offset(off), id(i) {}
}off_info;
static const int off_info_size = sizeof(off_info);

uint32_t interval = 5000000;
uint32_t count[THREAD_NUM];
uint32_t tmp_file_id[8] = {0, 0, 0, 0, 0, 0, 0, 0};
char map_tmp[THREAD_NUM*2][16];
char tid_tmp[THREAD_NUM*2][16];
char offset_tmp[THREAD_NUM][16];
char trading_day[9] = "";
//char map_tmp[16][16] = {"./tmp/map1.1", "./tmp/map1.2", "./tmp/map2.1", "./tmp/map2.2",
//"./tmp/map3.1", "./tmp/map3.2","./tmp/map4.1", "./tmp/map4.2","./tmp/map5.1", "./map5.2","./map6.1", "./map6.2",
//"./map7.1", "./map7.2","./map8.1", "./map8.2",};
//char tid_tmp[16][16] = {"./tid1.1", "./tid1.2", "./tid2.1", "./tid2.2",
//"./tid3.1", "./tid3.2","./tid4.1", "./tid4.2","./tid5.1", "./tid5.2","./tid6.1", "./tid6.2",
//"./tid7.1", "./tid7.2","./tid8.1", "./tid8.2",};
//char offset_tmp[8][16] = {"off1","off2","off3","off4","off5","off6","off7", "off8"};

void init_tmp_file_name()
{
	for(int i = 0; i < THREAD_NUM; i++)
	{
		snprintf(map_tmp[2*i], 16, "./tmp/map%d.1", i);
		snprintf(map_tmp[2*i + 1], 16, "./tmp/map%d.2", i);
		snprintf(tid_tmp[2*i], 16, "./tmp/tid%d.1", i);
		snprintf(tid_tmp[2*i + 1], 16, "./tmp/tid%d.2", i);
		snprintf(offset_tmp[i], 16, "./tmp/off%d", i);
	}
}

void save_temp_file(begin_end *be, uint32_t offset, i_trade &pre_trade)
{
	int thread_id = be->i;
	int file_id = tmp_file_id[thread_id] + thread_id * 2;
	// save client position map
	FILE *map_fp = ::fopen(map_tmp[file_id], "w+b");
	std::map<i_trade, l_s_position_, compare_key>::iterator itr = (results[thread_id]).begin();
	for(; itr != (results[thread_id]).end(); ++itr)
	{
		fwrite(&(itr->first), i_trade_size, 1, map_fp);
		fwrite(&(itr->second), l_s_position_size, 1, map_fp);
		fflush(map_fp);
	}
	fclose(map_fp);

	// save trade id buf
	FILE *tid_fp = ::fopen(tid_tmp[file_id], "w+b");
	fwrite(&(be->trade_id_size_), sizeof(be->trade_id_size_), 1, tid_fp);
	fwrite(&pre_trade, i_trade_size, 1, tid_fp);
	fwrite(be->trade_id_buf_, 1, be->trade_id_size_, tid_fp);
	fflush(tid_fp);
	fclose(tid_fp);

	printf("save thread[%d] offset[%u] tidfile[%s] sie[%u]\n",
			thread_id, offset, tid_tmp[file_id], be->trade_id_size_);

	// save offset
	FILE *off_fp = ::fopen(offset_tmp[thread_id], "w+b");
	off_info offsets(offset, tmp_file_id[thread_id]);
	fwrite(&(offsets), off_info_size, 1, off_fp);
	fflush(off_fp);
	fclose(off_fp);
	tmp_file_id[thread_id] = (tmp_file_id[thread_id] == 0) ? 1 : 0;
}

uint32_t load_temp_file(begin_end *be, i_trade &pre_trade)
{
	int thread_id = be->i;
	uint32_t offset = 0;

	// load offset
	FILE *off_fp = ::fopen(offset_tmp[thread_id], "r");
	if(off_fp == NULL)
	{
		//printf("open [%s] failed\n", offset_tmp[thread_id]);
		return 0;
	}
	off_info offset_info;
	fread(&offset_info, off_info_size, 1, off_fp);


	// load ClientPosition
	FILE *map_fp = ::fopen(map_tmp[offset_info.id + thread_id * 2], "r");
	i_trade key;
	l_s_position value;
	while(fread(&key, i_trade_size, 1, map_fp))
	{
		fread(&value, l_s_position_size, 1, map_fp);
		(results[thread_id])[key] = value;
	}

	// load TradeID
	FILE *tid_fp = ::fopen(tid_tmp[offset_info.id + thread_id * 2], "r");
	fread(&(be->trade_id_size_), sizeof(be->trade_id_size_), 1, tid_fp);
	fread(&pre_trade, i_trade_size, 1, tid_fp);
	fread(be->trade_id_buf_, 1, be->trade_id_size_, tid_fp);

	printf("!!!!!restart thread_id[%d] offset[%u] file[%s] size[%u]\n",
			thread_id, offset_info.offset,tid_tmp[offset_info.id + thread_id * 2], be->trade_id_size_);

	fclose(map_fp);
	fclose(tid_fp);
	return offset_info.offset;
}

static void *thread_func(void *params)
{//params defines to class thread
	begin_end* be = (begin_end *)params;
	printf("begin: %p, end: %p\n", be->begin_, be->end_);
	l_s_position_ position;
	char *begin, *end = NULL;
	begin = be->begin_;
	i_trade i_trade_;
	i_trade temp;
	int offset = be->i;

	be->count_ = 0;
	be->trade_id_buf_ = trade_ids_[offset];
	be->trade_id_size_ = 0;
	begin += load_temp_file(be, i_trade_);

	while(begin != NULL)
	{
		position.reset_position();
		parse(begin, temp, position.long_position, position.short_position, i_trade_, be);
		results[offset][temp] += position;
		end = strchr(begin, '\n');
		begin = end + 1;
		if(begin - be->begin_ >= be->end_ - be->begin_)
		{
			if(offset != THREAD_NUM - 1)
				results[offset][temp] -= position;
			break;
		}

		if(++be->count_ == interval)
		{	printf("thread[%d] count[%d]\n", offset, be->count_);
			save_temp_file(be, begin - be->begin_, i_trade_);
			be->count_ = 0;
		}
	}
    return NULL;
}

#ifndef DEBUG
void main() {
#else
TEST(db_physical, Run) {
#endif

	offsets['O'] = 13;
	offsets['F'] = 6;
	offsets['1'] = 5; // T1701

	product['I']['F'] = 1;
	product['I']['O'] = 2;
	product['T']['1'] = 3;
	product['T']['F'] = 4;

	pos_func['0']['0'] = &add_long_pos;
	pos_func['0']['1'] = &del_long_pos;
	pos_func['1']['0'] = &add_short_pos;
	pos_func['1']['1'] = &del_short_pos;

	init_tmp_file_name();

//	char *buffer = (char *)malloc(1024);
//	aggregation *p = new aggregation("data/IncTrade.csv");
//
	unsigned long long tick = 0UL;
	startTimer(&tick);

	trade_ids_ = (char**)malloc(THREAD_NUM * sizeof(char *));
	for(int i = 0; i < THREAD_NUM; ++i)
		trade_ids_[i] = (char *)malloc(10240000 * 80);
//	trade_ids_ = (uint32_t**)malloc(THREAD_NUM * 50240000 * 4);
//	p->open();
//	while(p->next(buffer) == true);
//	p->close();
//	printf("time: %f ms\n", getMilliSecond(tick));

	//////////////////////////////////////////////////////////////////////////////////////////
	std::string path_ = "data/IncTrade.csv";
	int fd = 0;
	if((fd = ::open(path_.c_str(), O_RDWR)) == -1)
	{
		ts::common::xlog::log(ts::common::LOG_ERROR, "open file failed.\n");
		exit(1);
	}
	int r = 0;
	struct stat st;
	if((r = fstat(fd, &st)) == -1)
	{
		ts::common::xlog::log(ts::common::LOG_ERROR, "get file size failed.\n");
		exit(1);
	}
	uint64_t len = st.st_size;
	char *start = (char *)::mmap(NULL, len, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	if(start == NULL || start == (void *) - 1)
	{
		ts::common::xlog::log(ts::common::LOG_ERROR, "mmap failed.\n");
		exit(1);
	}

	if(-1 == madvise(start, len, MADV_WILLNEED | MADV_SEQUENTIAL))
	{
		ts::common::xlog::log(ts::common::LOG_ERROR, "madvise failed.\n");
		exit(1);
	}

	begin_end offsets[THREAD_NUM];

	start += 260;
	uint64_t current = 0;
	char *temp = 0;
	len -= 260;
	memcpy(trading_day, start, 8);
	trading_day[8] = '\0';
	uint64_t average = (len/THREAD_NUM);
	for(int i = 0; i < THREAD_NUM - 1; ++i)
	{
		offsets[i].i = i;
		offsets[i].begin_ = start + current;
		temp = start + current + average;
		while(*temp++ != '\n');
		current = temp - start;
		while(*temp++ != '\n');
		offsets[i].end_ = temp - 1;
	}
	offsets[THREAD_NUM-1].i = THREAD_NUM-1;
	offsets[THREAD_NUM-1].begin_ = start + current;
	offsets[THREAD_NUM-1].end_   = start + len;

	pthread_t threads_[THREAD_NUM];
	for(int i = 0; i < THREAD_NUM; ++i)
	{
		(::pthread_create(&threads_[i], NULL, &thread_func, &offsets[i]) == 0);
	}

	for(int i = 0; i < THREAD_NUM; ++i)
	{
		pthread_join(threads_[i], NULL);
	}

	std::map<i_trade, l_s_position_, compare_key> result;
	for(int i = 0; i < THREAD_NUM; ++i)
	{
		std::map<i_trade, l_s_position_, compare_key>::iterator itr = results[i].begin();
		char buf[128];
		for(; itr != results[i].end(); ++itr)
		{
			result[itr->first] += itr->second;
		}
	}

	/////////////////////////////////////////
	int fd_cp = 0;
	if((fd_cp = ::open("./ClientPosition.csv", O_CREAT|O_RDWR, 00666)) == -1)
	{
		ts::common::xlog::log(ts::common::LOG_ERROR, "open file failed.\n");
		exit(1);
	}
	uint64_t file_size_cp = 1024*10;
	ftruncate(fd_cp, file_size_cp);
	char *output_cp = (char *)::mmap(NULL, file_size_cp, PROT_WRITE, MAP_SHARED, fd_cp, 0);
	if(output_cp == NULL || output_cp == (void *) - 1)
	{
		ts::common::xlog::log(ts::common::LOG_ERROR, "mmap failed.\n");
		exit(1);
	}
	memcpy(output_cp, "TradingDay,ParticipantID,ClientID,InstrumentID,LongPosition,ShortPosition\n", 74);
	output_cp += 74;
	uint64_t len_cp = 0;
	std::map<i_trade, l_s_position_, compare_key>::iterator itr = result.begin();
	char buf_cp[128];
	for(; itr != result.end(); ++itr)
	{
		char *p = output_cp + len_cp;
		int buf_len = snprintf(buf_cp, 128, "%s,%d,%08d,%s,%d,%d\n", trading_day, (itr->first).partid, (itr->first).clientid, (itr->first).insname,
			(itr->second).long_position, (itr->second).short_position);
		memcpy(p, buf_cp, buf_len + 1);
		len_cp += buf_len;
	}

	munmap(output_cp, file_size_cp);
	ftruncate(fd_cp, len_cp + 74);
	::close(fd_cp);

	///////////////////////////////save SelfTrade.csv////////////////////////////////////////
	///////////////////////////////////////////
	int fd_ti = 0;
	if((fd_ti = ::open("./SelfTrade.csv", O_CREAT|O_RDWR, 00666)) == -1)
	{
		ts::common::xlog::log(ts::common::LOG_ERROR, "open file failed.\n");
		exit(1);
	}
	uint64_t file_size_ti = 1024*1024*1024;
	ftruncate(fd_ti, file_size_ti);
	char *output_ti = (char *)::mmap(NULL, file_size_ti, PROT_WRITE, MAP_SHARED, fd_ti, 0);
	if(output_ti == NULL || output_ti == (void *) - 1)
	{
		ts::common::xlog::log(ts::common::LOG_ERROR, "mmap failed.\n");
		exit(1);
	}
	memcpy(output_ti, "TradeID\n", 8);
	output_ti += 8;
	uint64_t len_ti = 0;
	for(int t = 0; t < THREAD_NUM; ++t)
	{
		memcpy(output_ti, offsets[t].trade_id_buf_, offsets[t].trade_id_size_);
		output_ti += offsets[t].trade_id_size_;
		len_ti += offsets[t].trade_id_size_;

	}
	munmap(output_ti, file_size_ti);
	ftruncate(fd_ti, len_ti + 8);
	::close(fd_ti);

	printf("time: %f ms\n", getMilliSecond(tick));
}

#endif
