#include <gtest/gtest.h>
#include "controller.h"

#ifdef TEST_FILE_OPERATOR

#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>

void delete_point(const char *new_file_name)
{

}

void scan_dir(const char *dir, int depth)
{
	DIR *dp;
	struct dirent *entry;
	struct stat statbuf;
	if(((dp) = opendir(dir)) == NULL)
	{
		printf("cant open dir[%s]\n", dir);
		return ;
	}
	char old_file_name[64] = "";
	char new_file_name[64] = "";
	chdir(dir);
	while((entry = readdir(dp)) != NULL)
	{
		lstat(entry->d_name, &statbuf);
		sprintf(old_file_name, "%*s%s", depth, "", entry->d_name);
		sprintf(new_file_name, "t_%s", old_file_name);
		printf("%s\n", new_file_name);
		rename(old_file_name, new_file_name);
		delete_point(new_file_name);
	}
	chdir("..");
	closedir(dp);
}

#ifndef DEBUG
void main() {
#else
TEST(file_operator, Run) {
#endif

	scan_dir("/cffex/zhanglei/environment/developTest/tkernel1/dump", 0);

}

#endif
