#include <gtest/gtest.h>
#include "controller.h"

#include <stdint.h>
#include <stdlib.h>
#include <cstdlib>
#include <map>
#include "../../../ts/common/time_helper.h"

#ifdef TEST_JUST_FOR_FUN

typedef struct linknode {
	linknode *next;
	int value;
}linknode;

linknode *create_linklist()
{
	linknode *head = (linknode *)malloc(sizeof(linknode));
	head->next  = NULL;
	head->value = 9;

	linknode *node = NULL;
	for(unsigned i = 0; i < 10; i++) {
		node = (linknode *)malloc(sizeof(linknode));
		node->next = head->next;
		node->value = 10 + i;
		head->next = node;
	}

	return head;
};

linknode *reverse_linklist(linknode *node)
{
	linknode *pnode = node;
	linknode *prev  = NULL; //ָ��NULL.
	linknode *next  = NULL;
	while(pnode != NULL) {
		next = pnode->next;
		pnode->next = prev;
		prev = pnode;

		pnode = next;
	}
	return prev;
}

void print(linknode *node)
{
	linknode *p = node;
	while(p != NULL)
	{
		printf("%d \n", p->value);
		p = p->next;
	}
}

typedef struct padding_test{
	int a;
	double b;
}padding_test;

void diff_char_and_unsigned_char()
{
	char a = 128;// 1000 0000
	unsigned char a1 = 256; //0001 0000 0000
	uint8_t b = 256;
	uint16_t b1 = 256;
	printf("char a = %d, a1 = %d, b = %d, b1 = %d\n", a, a1, b, b1);
	char c = 522;
	printf("char c = %d \n", c);
}

class text_book
{
public:
	text_book(const char * text): text_(text){printf("constructor function.\n");}
//	text_book(const text_book &t){this->text_ = t.text_;printf("copy constructor function.\n");}
	~text_book(){printf("desconstruct function.\n");}
//	char operator[](size_t position){return text_[position];}
	//the return value can not be valued.
	char &operator[](size_t position)
	{/*printf("not const function.\n");return text_[position];*/
		return const_cast<char &>(static_cast<const text_book &>(*this)[position]);
	}
	std::string& value(){return text_;}
	const char &operator[](size_t position) const {printf("in const function.\n");return text_[position];}
	const char *address() const{return text_.c_str();}
private:
	text_book(const text_book &t){this->text_ = t.text_;printf("copy constructor function.\n");}
private:
	std::string text_;
};

class text_book_1
{
public:
	text_book_1(char * text): text_(text){}
	~text_book_1(){}
	char &operator[](size_t position) const {return text_[position];}
//	const char &operator[](size_t position) const {return text_[position];}
	const char *address() const{return text_;}
private:
	char* text_;
};

void used_const()
{
	char greeting[] = "hello";
	char greeting1[] = "dafafadf";
	char *p = greeting;
	char *q = greeting1;
	const char *p1 = greeting;     //const data, non const pinter.
	char * const p2 = greeting;    //const pointer, non const data.
	const char * const p3 = greeting;//const pointer, const data.

	p1 = q;
	printf("successfully non const pointer, const data.\n");
	*p2 = '1';
	printf("successfully const pointer, non const data. %s\n", p2);

	//if text_book is const, t[1] = 'k' can not pass the compile.
	text_book t("hello");
	t[1] = 'k';
	printf("text book: %s\n", t.address());

	char *p11 = (char *)malloc(12);
	strcpy(p11, t.address());
	const text_book_1 t1(p11);
	t1[1] = 'b';
	printf("text book: %s\n", t1.address());

	char &c1 = t[3];
	const char &c2 = t[3];

	t[4] = c1;
	t[4] = c2;



}

int getvalue()
{
	static int i = 1;
	static int j = i ++;
	printf("enter get value. %d\n", i);
	return j;
}

void static_value()
{
	printf("static value: %d\n", getvalue());
}

void display(text_book &t)
{
	printf("text_book string: %s\n", t.value().c_str());
}

template<typename T>
class named_value
{
public:
	named_value(std::string &value, const T &t_value)
		:value_(value), t_value_(t_value){}
	named_value &operator=(const named_value<T> &v){//the copy assignment is needed.
		value_ = v.value_;
		t_value_ = v.t_value_;
		return *this;
	}
private:
	std::string &value_; // can not use the default copy assignment. compiler don't know how to value reference.
	const T t_value_;    // can not use the default copy assignment. compiler don't know how to value const member value.
};

const char *const_char_()
{
//	char buffer[4] = {'a', 'b', 'c', '\0'};
	char *buffer = new char[4];
	buffer[0] = 'a';
	buffer[1] = 'b';
	buffer[2] = 'c';
	buffer[3] = '\0';
	printf("buffer: %s\n", buffer);
	return buffer;
}

class test_static
{
public:
	void foo()
	{
		static int k = 4;
		printf("k: %p\n", &k);
	}

	void foo1()
	{
		int k = 4;
		printf("k: %p\n", &k);
	}

};

class Base
{
private:
	int x;
public:
	virtual void mf1() = 0;
	virtual void mf1(int){printf("Base::%s(int)\n", __FUNCTION__);};
	virtual void mf2(){printf("Base::%s\n", __FUNCTION__);};
	void mf3(){printf("Base::%s\n", __FUNCTION__);};
	void mf3(double){printf("Base::%s\n", __FUNCTION__);};
};

void Base::mf1()
{
	printf("Base::%s\n", __FUNCTION__);
}

class Derived: public Base
{
public:
//	using Base::mf1;
//	using Base::mf3;
	virtual void mf1(){printf("Derived::%s\n", __FUNCTION__);};
//	void mf3(){printf("Derived::%s\n", __FUNCTION__);};
//	void mf4(){printf("Derived::%s\n", __FUNCTION__);};
};

int(*((*ptr(int, int))))(int)
{
	return NULL;
}

int *ptr()
{
	return NULL;
}

//int (*)ptr()
//{
//	return NULL;
//}

char buffer[64] = "123456";

void get1(int id, void **pObject)
{
	id = 0;
	pObject = (void **)(&buffer);
}

void get2(int id, void **pObject)
{
	id = 0;
	*pObject = buffer;
}

char get_visible_char(char c) { char r = (c > 33) && (c < 127) ? c : '.'; return r; }

void printDump(const char *buffer)
{
	printf("buffer: %s\n", buffer);
	int size = strlen(buffer);
	while (size > 8) {
		printf("%02X%02X %02X%02X %02X%02X %02X%02X %02X%02X %02X%02X %02X%02X %02X%02X              %c%c %c%c %c%c %c%c %c%c %c%c %c%c %c%c\n",
				buffer[0]&0xFF, buffer[1]&0xFF, buffer[2]&0xFF,  buffer[3]&0xFF,  buffer[4]&0xFF,  buffer[5]&0xFF,  buffer[6]&0xFF,  buffer[7]&0xFF,
				buffer[8]&0xFF, buffer[9]&0xFF, buffer[10]&0xFF, buffer[11]&0xFF, buffer[12]&0xFF, buffer[13]&0xFF, buffer[14]&0xFF, buffer[15]&0xFF,
				get_visible_char(buffer[0]),  get_visible_char(buffer[1]),  get_visible_char(buffer[2]),  get_visible_char(buffer[3]),
				get_visible_char(buffer[4]),  get_visible_char(buffer[5]),  get_visible_char(buffer[6]),  get_visible_char(buffer[7]),
				get_visible_char(buffer[8]),  get_visible_char(buffer[9]),  get_visible_char(buffer[10]), get_visible_char(buffer[11]),
				get_visible_char(buffer[12]), get_visible_char(buffer[13]), get_visible_char(buffer[14]), get_visible_char(buffer[15])
		);
		buffer += 8;
		size   -= 8;
	}
	printf("\n");
}

#ifndef DEBUG
void main() {
#else
TEST(JustForFun, Run) {
#endif

//	void **p = (void **)malloc(4);
//	get1(1, p);
//	printf("p: %p, *p: %p, string: %s\n", p, *p, *p);
//	get2(2, p);
//	printf("p: %p, *p: %p, string: %s\n", p, *p, *p);
//
//	uint32_t a = 0x0001;
//	uint32_t b = 0x0000;
//	printf("[1|1] %x -- [0|1] %x -- [0|0] %x\n", a|a, a|b, b|b);
//	printf("[1&1] %x -- [0&1] %x -- [0&0] %x\n", a&a, a&b, b&b);
//	printf("[1^1] %x -- [0^1] %x -- [0^0] %x\n", a^a, a^b, b^b);
//
//	char buffer[16] = "abcdefgh1234567";
//	printDump(buffer);
//
//	char buffer1[16];
//	printDump(buffer1);
//	memset(buffer1, '*', 16);
//	printDump(buffer1);

//	diff_char_and_unsigned_char();
//	used_const();
//	static_value();
//	static_value();
//
////	int *ptr[2];
////	int (*)ptr[2];
//
//	text_book t("12");
////	text_book t1(t);//the copy constructor is private.
//	display(t);
//
//	std::string va("abc");
//	std::string vb("cbd");
//
//	named_value<int> p(va, 1);
//	named_value<int> q(vb, 2);
////	p = q;//copy assignment
//
//	std::map<const char *, int> m_map;
//	const char *r1_key = "abc";
//	const char *r2_key = const_char_();
//	const char *r3_key = const_char_();
//
//	char a[12] = "1234561234";
//	char *str = a;
//	char b;
//	while(b = *str++)
//		printf("%c\n", b);
//
//	printf("padding: %d\n", sizeof(padding_test));
//
//	m_map[r1_key] = 8;
//	m_map[r2_key] = 9;
//
//
//	printf("%u %d\n", m_map.size(), m_map[r3_key]);
//
//	std::map<const char *, int>::iterator it = m_map.begin();
//	for(; it != m_map.end(); ++it)
//		printf("%s, %d\n", it->first, it->second);
//
//
//
//	Derived D;
//	int x;
//	D.mf1();
//	D.Base::mf1();
//
//	double aa = 34.3544999;
//	printf("a: %.3lf\n", aa);
//
//	char t1 = '\r';
//	printf("t1: %d\n", t1);
//	D.mf1(x);
//	D.mf2();
//	D.mf3();
//	D.mf3(x);

	linknode *node = create_linklist();
	print(node);

}

#endif
