#include <gtest/gtest.h>
#include "controller.h"

#ifdef TEST_XML_PARSER

#include "xml_parser.h"

#ifndef DEBUG
void main() {
#else
TEST(XMLParser, ParseInt){
#endif
    ts::common::xml_parser::get_instance()->parse_file("./data/person.xml");
    int age = ts::common::xml_parser::get_instance()->get_parameter("age", 9999);
    EXPECT_EQ(age, 26);
}

#endif
