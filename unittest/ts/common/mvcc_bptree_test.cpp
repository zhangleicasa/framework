#include <gtest/gtest.h>
#include "controller.h"

#ifdef TEST_MVCC_TREE

#include "time_helper.h"
#include "bplus_tree.h"
#include "mem_table.h"
#include "allocator.h"
#include "test_table.h"

#include <algorithm>
#include <vector>
#include <sstream>

#include <stdlib.h>

ts::sys::rw_lock rw_lock_;

typedef struct ThreadInfo
{
    int thread_id;
    CTestMVCCBplusTable *table;
}ThreadInfo;

void *insert_thread(void *args)
{
    ThreadInfo* info = (ThreadInfo *)args;
    for(int i = 0; i < INSERT_COUNT; i++)
    {
        CTestRecord test_record;
        std::stringstream key;
        int value = INSERT_THREAD_COUNT*i + info->thread_id;
        key << value;
        strcpy(test_record.m_nID, key.str().c_str());
        strcpy(test_record.m_nValue, key.str().c_str());
        info->table->internalAdd(&test_record);
    }
    return NULL;
}

TEST(MVCCBPlusTree, Random)
{
    timespec time1, time2;
    clock_gettime(2, &time1);
    ts::mem::memory_allocator * ar = new ts::mem::memory_allocator();
    ar->init();
    CTestMVCCBplusTable testTable(1024000, ar);
    const int insert_number = INSERT_COUNT_DOUBLE;

    unsigned long long insert_time_spend = 0;
    std::vector<int> array, sorted_array, before_array;
    for(int i = 0; i < insert_number; i++)
    {
        array.push_back(i);
        before_array.push_back(i);
    }   
    std::random_shuffle(array.begin(), array.end());
    startTimer(&insert_time_spend);



    /** Insert the b tree by using thread. **/
    pthread_t threads[INSERT_THREAD_COUNT];
    
    for(int i = 0; i < INSERT_THREAD_COUNT; i++)
    {
        ThreadInfo *info = new ThreadInfo();
        info->table      = &testTable;
        info->thread_id  = i;
        pthread_create(&threads[i], NULL, insert_thread, info);
    }

    for(int i = 0; i < INSERT_THREAD_COUNT; i++)
        pthread_join(threads[i], 0);

    clock_gettime(2, &time2);

    common::xlog::log(LOG_ALL, "\n\n\n***********************************************\n");
    common::xlog::log(LOG_ALL, "          [SUCCESS]: Insert the %d records.\n", insert_number);
    common::xlog::log(LOG_ALL, "[INSERT SPEND TIME]: %f milli sec.\n", getMilliSecond(insert_time_spend));
    common::xlog::log(LOG_ALL, "    [CLOCK_GETTIME]: %d s - %d ns\n", diff(time1, time2).tv_sec, diff(time1, time2).tv_nsec);
    common::xlog::log(LOG_ALL, "***********************************************\n\n\n");
    //testTable.display();
    //testTable.display_time();




    /** Traverse the b tree by deep first traverse. **/
    std::vector<int>::iterator it = before_array.begin();
    
    std::queue<ts::common::mvcc_bplus_node *> df_traverse;
    df_traverse.push(testTable.get_root());
    
    int this_level = 1;
    int next_level = 0;
    
    ts::common::mvcc_bplus_node *temp_node = df_traverse.front();
    
    while(!temp_node->isleaf_)
    {
        for(int i=0; i<temp_node->capacity_+1; ++i)
        {
            if(i != temp_node->capacity_)
            {
                //common::xlog::log(LOG_ALL, " - ");
                //common::xlog::log(LOG_ALL, "%s", ((CTestRecord *)temp_node->keys_[i])->m_nID);
            }
            ts::common::mvcc_bplus_node *temp = temp_node->points_[i];
            if(temp == NULL)
                continue;
            df_traverse.push(temp);
            next_level++;
        }
        //common::xlog::log(LOG_ALL, " | ");
        df_traverse.pop();
        if(--this_level == 0)
        {
            this_level = next_level;
            //common::xlog::log(LOG_ALL, "\n");
            next_level=0;
        }
        temp_node = df_traverse.front();
    }
    
    int q_size = df_traverse.size();
    for(int i=0; i < q_size; ++i)
    {
        ts::common::mvcc_bplus_node *leaf_node = df_traverse.front();
        for(int j=0; j < leaf_node->capacity_; ++j)
        {
            //common::xlog::log(LOG_ALL, " - ");
            //common::xlog::log(LOG_ALL, "%s", ((CTestRecord *)leaf_node->keys_[j])->m_nID);
            int nID = atoi(((CTestRecord *)leaf_node->keys_[j])->m_nID);
            sorted_array.push_back(nID);
        }
        //common::xlog::log(LOG_ALL, " | ");
        df_traverse.pop();
    }


    /** Compare the expected and actrual value. **/
    int total_not_positive = 0;
    for(unsigned i = 0, j = 0; i < before_array.size(); )
    {
        //std::cout<<sorted_array[j]<<" - "<<before_array[i]<<std::endl;
        if(before_array[i] == -1)
        {
            i++;
            total_not_positive++;
        }
        else
        {
            EXPECT_EQ(before_array[i], sorted_array[j]);
            i++;
            j++;
        }
    }
    common::xlog::log(LOG_ALL, "%d + %d\n", sorted_array.size(), total_not_positive);

}

#endif

