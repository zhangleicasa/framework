#include <gtest/gtest.h>
#include "controller.h"

#ifdef TEST_ENHANCED_AVL_TREE

#include "enhanced_avl_tree.h"
#include "allocator.h"
#include "x_log.h"

using namespace ts::common;

typedef struct head {
    char a[13];
    bool operator >(const head h) const
    {
        if(strcmp(a, h.a) >0)
            return true;
        else 
            return false;
    }
    
    bool operator <(const head h) const
    {
        if(strcmp(a, h.a) <0)
            return true;
        else
            return false;
    }

    bool operator ==(const head h) const
    {
        if(strcmp(a, h.a) ==0)
            return true;
        else
            return false;
    }
}head;

typedef struct head_ptr {
    head * ptr;
    head_ptr(head *p):ptr(p){}
    bool operator > (const head_ptr h) const
    {
        if(strcmp(ptr->a, (h.ptr)->a) > 0)
            return true;
        else
            return false;
    }
    
    bool operator < (const head_ptr h) const
    {
        if(strcmp(ptr->a, (h.ptr)->a) < 0)
            return true;
        else
            return false;
    }

    bool operator == (const head_ptr h) const
    {
        if(strcmp(ptr->a, (h.ptr)->a) == 0)
            return true;
        else
            return false;
    }
}head_ptr;

#ifndef DEBUG
void main() {
#else
TEST(EnhancedAVLTree, Random){
#endif
    ts::common::enhanced_avl_tree<int> avl;
	avl.insert(6);
	avl.insert(7);
	avl.insert(10);
	enhanced_avl_tree<int>::_iterator it(avl.get_root());
	ts::common::xlog::log(ts::common::LOG_ALL, "it  : %d\n", *it);
	ts::common::xlog::log(ts::common::LOG_ALL, "it++: %d\n", *(++it));
	ts::common::xlog::log(ts::common::LOG_ALL, "it--: %d\n", *(--it));
	ts::common::xlog::log(ts::common::LOG_ALL, "it--: %d\n", *(--it));

	enhanced_avl_tree<int>::_iterator itr;
	itr = avl.find(10);
	ts::common::xlog::log(ts::common::LOG_ALL, "it--: %d\n", *(--itr));

	avl.erase(10);
	enhanced_avl_tree<int>::deep_traverse(avl.get_root());

    head a, b, c, d, e, f, g, h, i, j, k, l, m, n;
    strcpy(a.a, "aaaaaaaaaaa");
    strcpy(b.a, "bbbbbbbbbbb");
    strcpy(c.a, "ccccccccccc");
    strcpy(d.a, "ddddddddddd");
    strcpy(e.a, "eeeeeeeeeee");
    strcpy(f.a, "fffffffffff");
    strcpy(g.a, "ggggggggggg");
    strcpy(h.a, "hhhhhhhhhhh");
    strcpy(i.a, "iiiiiiiiiii");
    strcpy(j.a, "jjjjjjjjjjj");
    enhanced_avl_tree<head, head, std::less<head>, ts::mem::cfix_mem<head> > char_struct;
    char_struct.insert(a);
    char_struct.insert(b);
    char_struct.insert(c);
    char_struct.insert(d);
    char_struct.insert(e);
    char_struct.insert(f);
    char_struct.insert(g);
    char_struct.insert(h);
    char_struct.insert(i);
    char_struct.insert(j);
    char_struct.print_binary_info();
    char_struct.erase(j);
    char_struct.print_binary_info();
    enhanced_avl_tree<head>::_iterator itc(char_struct.get_root());
    ts::common::xlog::log(ts::common::LOG_ALL, "it  : %s\n", (*itc).a);
    ts::common::xlog::log(ts::common::LOG_ALL, "it++: %s\n", (*(++itc)).a);
    ts::common::xlog::log(ts::common::LOG_ALL, "it--: %s\n", (*(--itc)).a);
    char_struct.print_binary_info();

    //head_ptr ha(&a), hb(&b), hc(&c), hd(&d), he(&e), hf(&f), hg(&g), hh(&h), hi(&i), hj(&j);
    //enhanced_avl_tree<head_ptr, head_ptr, std::less<head_ptr>, ts::mem::cfix_mem<head_ptr> > ptr_struct;
    //common::xlog::log(LOG_ALL, "%p, %p, %p, %p, %p, %p, %p, %p, %p, %p\n", ha.ptr, hb.ptr, hc.ptr, hd.ptr, he.ptr, hf.ptr, hg.ptr, hh.ptr, hi.ptr, hj.ptr);
    //ptr_struct.insert(ha);
    //ptr_struct.insert(hb);
    //ptr_struct.insert(hc);
    //ptr_struct.insert(hd);
    //ptr_struct.insert(he);
    //ptr_struct.insert(hf);
    //ptr_struct.insert(hg);
    //ptr_struct.insert(hh);
    //ptr_struct.insert(hi);
    //ptr_struct.insert(hj);
    //enhanced_avl_tree<head_ptr>::_iterator itd(ptr_struct.get_root());
    //common::xlog::log(LOG_ALL, "\n\n\nptr it  : %s\n", (*itd).ptr->a);
    //common::xlog::log(LOG_ALL, "ptr it++: %s\n", (*(++itd)).ptr->a);
    //common::xlog::log(LOG_ALL, "ptr it--: %s\n", (*(--itd)).ptr->a);
    //ptr_struct.print_binary_info();

}

#endif
