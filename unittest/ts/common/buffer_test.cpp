#include <gtest/gtest.h>
#include "controller.h"

#include "log_helper.h"
#include "buffer.h"
#include "x_log.h"

#ifdef TEST_BUFFER

#define BUFFER_SIZE 16

#ifndef DEBUG
void main() {
#else
TEST(MEM, BUFFER){
#endif
    ts::mem::buffer *pbuffer = new ts::mem::buffer(BUFFER_SIZE);
    const char *a = "123456789";
    bool isok1 = pbuffer->enough_space(9);
    if(isok1) pbuffer->append(a, 9);
    EXPECT_EQ(isok1, true);
    const char *b = "1234567";
    bool isok2 = pbuffer->enough_space(7);
    if(isok2) pbuffer->append(b, 7);
    EXPECT_EQ(isok2, true);
    ts::common::xlog::log(ts::common::LOG_ALL, "%s\n", GetBinaryDumpInfo(pbuffer->address(), 128).c_str());
    bool isok3 = pbuffer->enough_space(7);
    EXPECT_EQ(isok3, false);
}

#endif
