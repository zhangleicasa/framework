#include <gtest/gtest.h>
#include "controller.h"

#ifdef TEST_BPLUS_TREE

#include "time_helper.h"
#include "bplus_tree.h"
#include "mem_table.h"
#include "allocator.h"
#include "test_table.h"

#include <algorithm>
#include <vector>
#include <sstream>

#include <stdlib.h>
#include <iostream>
using namespace std;

//#include <time.h>
//timespec diff(timespec start, timespec end)
//{
//  timespec temp;
//  if ((end.tv_nsec-start.tv_nsec)<0) {
//      temp.tv_sec = end.tv_sec-start.tv_sec-1;
//      temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
//  }
//  else
//  {
//      temp.tv_sec = end.tv_sec-start.tv_sec;
//      temp.tv_nsec = end.tv_nsec-start.tv_nsec;
//  }
//  return temp;
//}

#ifndef DEBUG
void main() {
#else
TEST(BPlusTree, Random){
#endif

    //timespec time1, time2;
    //int temp;

    //clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time1);

    timespec time1, time2;
    clock_gettime(2, &time1);
    ts::mem::memory_allocator * ar = new ts::mem::memory_allocator();
    ar->init();
    CTestBplusTable testTable(1024000, ar);
    const int insert_number = INSERT_COUNT_DOUBLE;
    const int delete_number = INSERT_COUNT_DOUBLE;
    unsigned long long insert_time_spend = 0;
    std::vector<int> array, sorted_array, before_array;
    for(int i = 0; i < insert_number; i++)
    {
        array.push_back(i);
        before_array.push_back(i);
    }   
    std::random_shuffle(array.begin(), array.end());
    startTimer(&insert_time_spend);
    for(int i = 0; i < insert_number; i++)
    {
        CTestRecord test_record;
        std::stringstream key;
        key << i;
        strcpy(test_record.m_nID, key.str().c_str());
        strcpy(test_record.m_nValue, key.str().c_str());
        testTable.internalAdd(&test_record);
    }

    ts::common::xlog::log(ts::common::LOG_ALL, "\n\n\n***********************************************\n");
    ts::common::xlog::log(ts::common::LOG_ALL, "          [SUCCESS]: Insert the %d records.\n", insert_number);
    ts::common::xlog::log(ts::common::LOG_ALL, "[INSERT SPEND TIME]: %f milli sec.\n", getMilliSecond(insert_time_spend));
    clock_gettime(2, &time2);
    ts::common::xlog::log(ts::common::LOG_ALL, "    [CLOCK_GETTIME]: %d s - %d ns\n", diff(time1, time2).tv_sec, diff(time1, time2).tv_nsec);

    std::vector<int>::iterator it = before_array.begin();
    
    int delete_array[3] = {28, 29, 27};

    unsigned long long delete_time_spend = 0;
    srand((unsigned)time(NULL));
    startTimer(&delete_time_spend);
    for(int i = 0; i < delete_number; ++i)
    {
        int rand_ = rand()%insert_number;
        *(it+rand_)  = -1;
        CTestRecord test_record;
        std::stringstream key;
        //key << delete_array[i];
        //std::cout<<"random: "<<delete_array[i]<<std::endl;

        key << rand_;
//        std::cout<<"random: "<<rand_<<std::endl;
        strcpy(test_record.m_nID, key.str().c_str());
        strcpy(test_record.m_nValue, key.str().c_str());
        testTable.internalDel(&test_record);        
        //testTable.display();
    }

    ts::common::xlog::log(ts::common::LOG_ALL, "          [SUCCESS]: Delete the %d records.\n", delete_number);
    ts::common::xlog::log(ts::common::LOG_ALL, "[DELETE SPEND TIME]: %f milli sec.\n", getMilliSecond(delete_time_spend));
    ts::common::xlog::log(ts::common::LOG_ALL, "***********************************************\n\n\n");
    
    std::queue<ts::common::bplus_node *> df_traverse;
    df_traverse.push(testTable.get_root());
    
    int this_level = 1;
    int next_level = 0;
    
    ts::common::bplus_node *temp_node = df_traverse.front();
    
    while(!temp_node->isleaf_)
    {
        for(int i=0; i<temp_node->capacity_+1; ++i)
        {
            if(i != temp_node->capacity_)
            {
                //common::xlog::log(LOG_ALL, " - ");
                //common::xlog::log(LOG_ALL, "%s", ((CTestRecord *)temp_node->keys_[i])->m_nID);
            }
            ts::common::bplus_node *temp = temp_node->points_[i];
            if(temp == NULL)
                continue;
            df_traverse.push(temp);
            next_level++;
        }
        //common::xlog::log(LOG_ALL, " | ");
        df_traverse.pop();
        if(--this_level == 0)
        {
            this_level = next_level;
            //common::xlog::log(LOG_ALL, "\n");
            next_level=0;
        }
        temp_node = df_traverse.front();
    }
    
    int q_size = df_traverse.size();
    for(int i=0; i < q_size; ++i)
    {
        ts::common::bplus_node *leaf_node = df_traverse.front();
        for(int j=0; j < leaf_node->capacity_; ++j)
        {
            //common::xlog::log(LOG_ALL, " - ");
            //common::xlog::log(LOG_ALL, "%s", ((CTestRecord *)leaf_node->keys_[j])->m_nID);
            int nID = atoi(((CTestRecord *)leaf_node->keys_[j])->m_nID);
            sorted_array.push_back(nID);
        }
        //common::xlog::log(LOG_ALL, " | ");
        df_traverse.pop();
    }

    int total_not_positive = 0;
    for(unsigned i = 0, j = 0; i < before_array.size(); )
    {
        //std::cout<<sorted_array[j]<<" - "<<before_array[i]<<std::endl;
        if(before_array[i] == -1)
        {
            i++;
            total_not_positive++;
        }
        else
        {
            EXPECT_EQ(before_array[i], sorted_array[j]);
            i++;
            j++;
        }
    }
    ts::common::xlog::log(ts::common::LOG_ALL, "%d + %d\n", sorted_array.size(), total_not_positive);

    //clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time2);

    //cout<<diff(time1,time2).tv_sec<<":"<<3500000000/(diff(time1,time2).tv_nsec)<< "  "<<CLOCK_PROCESS_CPUTIME_ID<<endl;

    //getchar();

    //  std::cout<<sorted_array[i]<<" - "<<std::endl;
   

    //EXPECT_EQ(26, 26);
}

#endif
