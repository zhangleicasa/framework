#include <gtest/gtest.h>
#include "controller.h"

#ifdef TEST_HASH_TABLE

#include "type_cast_func.h"
#include "hash_function.h"
#include "time_helper.h"
#include "column_type.h"
#include "hash_table.h"
#include "public.h"
#include "schema.h"
#include "buffer.h"
#include "x_log.h"

using ts::data::column_type;
using ts::data::schema;
using ts::mem::buffer;

typedef struct data{
	uint64_t a;
	char  b[7];
}data_;

TEST(HashTable, hashtable)
{
	std::vector<column_type> vec_columns;
	vec_columns.push_back(column_type(ts::expr::t_int));
	vec_columns.push_back(column_type(ts::expr::t_float));
	schema * s = new schema(vec_columns);

	ts::common::hash_table *table = new ts::common::hash_table(2, 64, 12);
	buffer *p = NULL;

	printf("int:%d float:%d\n", sizeof(int), sizeof(float));
	for(unsigned i = 0; i < 9; ++i)
	{
		p = new buffer(s->get_total_size() + 4);
		int a = 1+i;
		float b = 88.8;
		p->append((char *)&a, sizeof(int));       // void * buffer(8): |int|float| while ->
		p->append((char *)&b, sizeof(float));
		void *q = table->allocate(0);
		memcpy((char *)q, (char *)(p->address()), 12);
	}

	for(unsigned i = 0; i < 9; ++i)
	{
		p = new buffer(s->get_total_size() + 4);
		int a = 1+i;
		float b = 66.6;
		p->append((char *)&a, sizeof(int));       // void * buffer(8): |int|float| while ->
		p->append((char *)&b, sizeof(float));
		void *q = table->allocate(1);
		memcpy((char *)q, (char *)(p->address()), 12);
	}

	table->debug();

	ts::common::hash_table_iterator itr1 = table->create_hash_table_iterator();
	table->locate_hash_table_iterator(itr1, 0);
	void *tuple = NULL;
	while((tuple = itr1.next()) != NULL)
	{
		printf("0 tuple: %d, %f\n", *(int *)tuple, *(float *)((char *)tuple+4));
	}

	ts::common::hash_table_iterator itr2 = table->create_hash_table_iterator();
	table->locate_hash_table_iterator(itr2, 1);
	while((tuple = itr2.next()) != NULL)
	{
		printf("1 tuple: %d, %f\n", *(int *)tuple, *(float *)((char *)tuple+4));
	}

}

#endif
