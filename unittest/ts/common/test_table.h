#include "time_helper.h"
#include "bplus_tree.h"
#include "mvcc_bptree.h"
#include "avl_tree.h"
#include "synchronize.h"
#include "spin_lock.h"
#include "mem_table.h"
#include "allocator.h"

#define INSERT_COUNT        500
#define INSERT_THREAD_COUNT 2
#define INSERT_COUNT_DOUBLE 1000

struct CTestRecord
{
    char m_nID[12];
    char m_nValue[12];
};

class table
{
public:
    table(int unitsize, int maxunit, ts::mem::allocator *allc)
    {
        mem_ = new ts::mem::mem_table(unitsize, maxunit, allc);
        maxunit_ = maxunit;
    }
    
    virtual ~table()
    {
        delete mem_;
    }
    
    inline void *create_object()
    {
        return mem_->alloc();
    }
    
    inline void delete_object(const void *object)
    {
        mem_->free(object);
    }

private:
    ts::mem::mem_table *mem_;
    int maxunit_;
};

///pV1��pV2�󣬷���1
static inline int compareForID(const void *pV1, const void *pV2) 
{
    CTestRecord *p1,*p2;
    p1=(CTestRecord *)pV1;
    p2=(CTestRecord *)pV2;
    unsigned int pv1, pv2;
    pv1 = atoi(p1->m_nID);
    pv2 = atoi(p2->m_nID);
    if(pv1 > pv2)
        return 1;
    else if(pv1 < pv2)
        return -1;
    else
        return 0;
}

static void forceCopy(const void *target, const void *source, int size)
{
    memcpy((void *)target,source,size);
}

/** the table has b plus tree. **/
class CTestBplusTable: public table
{
public:
    CTestBplusTable(int maxUnit, ts::mem::allocator *pAllocator)
    :table(sizeof(CTestRecord), maxUnit, pAllocator) {
        init(maxUnit, pAllocator);
    };
    ~CTestBplusTable(void){};
    
    void init(int maxUnit,ts::mem::allocator *pAllocator)
    {
        m_pIDBPTreeIndex = new ts::common::bplus_tree(maxUnit, compareForID, pAllocator);
    };
    
    CTestRecord *internalAdd(CTestRecord *pRecord)
    {

        CTestRecord *pTarget = (CTestRecord *)create_object();
        forceCopy(pTarget, pRecord, sizeof(CTestRecord));
        m_pIDBPTreeIndex->t_insert(pTarget);
        
        return pTarget;
    }
    
    CTestRecord *internalDel(CTestRecord *pRecord)
    {
        m_pIDBPTreeIndex->t_delete(pRecord);
        //delete_object(pRecord);
        return NULL;
    }

    ts::common::bplus_node *get_root()
    {
        return m_pIDBPTreeIndex->get_root();
    }
    
    void display()
    {
        ts::common::bplus_tree::display_tree<CTestRecord>(m_pIDBPTreeIndex->get_root());
    }

public:
    ts::common::bplus_tree *m_pIDBPTreeIndex;
};

/** the table has b plus tree. **/
class CTestMVCCBplusTable: public table
{
public:
    CTestMVCCBplusTable(int maxUnit, ts::mem::allocator *pAllocator)
    :table(sizeof(CTestRecord), maxUnit, pAllocator) {
        init(maxUnit, pAllocator);
        time_float_1    = 0.0;
        time_float_2    = 0.0;
    };
    ~CTestMVCCBplusTable(void){};
    
    void init(int maxUnit,ts::mem::allocator *pAllocator)
    {
        m_pIDBPTreeIndex = new ts::common::mvcc_bptree(maxUnit, compareForID, pAllocator);
    };
    
    CTestRecord *internalAdd(CTestRecord *pRecord)
    {
        spin_lock_.acquire();
        CTestRecord *pTarget = (CTestRecord *)create_object();
        forceCopy(pTarget, pRecord, sizeof(CTestRecord));
        m_pIDBPTreeIndex->t_insert(pTarget);
        spin_lock_.release();
        return pTarget;
    }
    
    CTestRecord *internalDel(CTestRecord *pRecord)
    {
        m_pIDBPTreeIndex->t_delete(pRecord);
        //delete_object(pRecord);
        return NULL;
    }

    ts::common::mvcc_bplus_node *get_root()
    {
        return m_pIDBPTreeIndex->get_root();
    }

    void display_time()
    {
    	ts::common::xlog::log(ts::common::LOG_ALL, "t1: %f  ==  t2: %f\n", time_float_1, time_float_2);
        m_pIDBPTreeIndex->display_time();
    }

public:
    ts::common::mvcc_bptree *m_pIDBPTreeIndex;
    ts::sys::spin_lock       spin_lock_;
    double                   time_float_1;
    double                   time_float_2;
};


/** the table has avl tree. **/
class CTestAVLTable: public table
{
public:
    CTestAVLTable(int maxUnit, ts::mem::allocator *pAllocator)
    :table(sizeof(CTestRecord), maxUnit, pAllocator) {
        init(maxUnit, pAllocator);
    };
    ~CTestAVLTable(void){};
    
    void init(int maxUnit,ts::mem::allocator *pAllocator)
    {
        m_pIDAVLTreeIndex = new ts::common::avl_tree(maxUnit, compareForID, pAllocator);
    };
    
    CTestRecord *internalAdd(CTestRecord *pRecord)
    {
        CTestRecord *pTarget = (CTestRecord *)create_object();
        forceCopy(pTarget, pRecord, sizeof(CTestRecord));
        m_pIDAVLTreeIndex->t_insert(pTarget);
        return pTarget;
    }
    
    CTestRecord *internalDel(CTestRecord *pRecord)
    {
        m_pIDAVLTreeIndex->t_delete(pRecord);
        //delete_object(pRecord);
        return NULL;
    }

public:
    ts::common::avl_tree *m_pIDAVLTreeIndex;
        
};

