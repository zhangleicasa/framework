#include <gtest/gtest.h>
#include "controller.h"

#ifdef TEST_AVL_TREE

#include "time_helper.h"
#include "avl_tree.h"
#include "mem_table.h"
#include "test_table.h"
#include "allocator.h"

#include <algorithm>
#include <vector>
#include <sstream>

#include <stdlib.h>

struct CTestRecord1
{
    char m_nID[12];
    char m_nValue[12];
};

class table1
{
public:
    table1(int unitsize, int maxunit, ts::mem::allocator *allc)
    {
        mem_ = new ts::mem::mem_table(unitsize, maxunit, allc);
        maxunit_ = maxunit;
    }
    
    virtual ~table1()
    {
        delete mem_;
    }
    
    inline void *create_object()
    {
        return mem_->alloc();
    }
    
    inline void delete_object(const void *object)
    {
        mem_->free(object);
    }

private:
    ts::mem::mem_table *mem_;
    int maxunit_;
};

/////pV1��pV2�󣬷���1
//static inline int compareForID(const void *pV1, const void *pV2)
//{
//    CTestRecord1 *p1,*p2;
//    p1=(CTestRecord1 *)pV1;
//    p2=(CTestRecord1 *)pV2;
//    unsigned int pv1, pv2;
//    pv1 = atoi(p1->m_nID);
//    pv2 = atoi(p2->m_nID);
//    if(pv1 > pv2)
//        return 1;
//    else if(pv1 < pv2)
//        return -1;
//    else
//        return 0;
//}
//
//static void forceCopy(const void *target, const void *source, int size)
//{
//    memcpy((void *)target,source,size);
//}


class CTestTable1: public table1
{
public:
    CTestTable1(int maxUnit, ts::mem::allocator *pAllocator)
    :table1(sizeof(CTestRecord1), maxUnit, pAllocator) {
        init(maxUnit, pAllocator);
    };
    ~CTestTable1(void){};
    
    void init(int maxUnit,ts::mem::allocator *pAllocator)
    {
        m_pIDBPTreeIndex = new ts::common::avl_tree(maxUnit, compareForID, pAllocator);
    };
    
    CTestRecord1 *internalAdd(CTestRecord1 *pRecord)
    {
        CTestRecord1 *pTarget = (CTestRecord1 *)create_object();
        forceCopy(pTarget, pRecord, sizeof(CTestRecord1));
        m_pIDBPTreeIndex->t_insert(pTarget);
        return pTarget;
    }
    
    CTestRecord1 *internalDel(CTestRecord1 *pRecord)
    {
        m_pIDBPTreeIndex->t_delete(pRecord);
        //delete_object(pRecord);
        return NULL;
    }

    void display()
    {
    	m_pIDBPTreeIndex->debug(m_pIDBPTreeIndex->get_root());
    }

public:
    ts::common::avl_tree *m_pIDBPTreeIndex;
        
};

#ifndef DEBUG
void main() {
#else
TEST(AVLTree, Random){
#endif
    ts::mem::memory_allocator * ar = new ts::mem::memory_allocator();
    ar->init();
    CTestAVLTable testTable(1024000, ar);
    const int insert_number = INSERT_COUNT_DOUBLE;
    const int delete_number = INSERT_COUNT_DOUBLE;
    unsigned long long insert_time_spend = 0;
    std::vector<int> array, sorted_array, before_array;
    for(int i = 0; i < insert_number; i++)
    {
        array.push_back(i);
        before_array.push_back(i);
    }   
    std::random_shuffle(array.begin(), array.end());
    startTimer(&insert_time_spend);
    for(int i = 0; i < insert_number; i++)
    {
        CTestRecord test_record;
        std::stringstream key;
        key << array[i];
        strcpy(test_record.m_nID, key.str().c_str());
        strcpy(test_record.m_nValue, key.str().c_str());
        testTable.internalAdd(&test_record);
    }
    ts::common::xlog::log(ts::common::LOG_ALL, "[INSERT SPEND]: %f\n", getMilliSecond(insert_time_spend));
    ts::common::xlog::log(ts::common::LOG_ALL, "[SUCCESS] Insert the %d records.\n", insert_number);

    std::vector<int>::iterator it = before_array.begin();
    
    unsigned long long delete_time_spend = 0;
    srand((unsigned)time(NULL));
    startTimer(&delete_time_spend);
    for(int i = 0; i < delete_number; ++i)
    {
        int rand_ = rand()%insert_number;
        *(it+rand_)  = -1;
        CTestRecord test_record;
        std::stringstream key;
        key << rand_;
        //std::cout<<"random: "<<rand_<<std::endl;
        strcpy(test_record.m_nID, key.str().c_str());
        strcpy(test_record.m_nValue, key.str().c_str());
        testTable.internalDel(&test_record);        
        //testTable.display();
    }
    ts::common::xlog::log(ts::common::LOG_ALL, "[DELETE SPEND]: %f\n", getMilliSecond(delete_time_spend));
    ts::common::xlog::log(ts::common::LOG_ALL, "[SUCCESS] Delete the %d records.\n", delete_number);

    //  std::cout<<sorted_array[i]<<" - "<<std::endl;
   

    //EXPECT_EQ(26, 26);
}

#endif
