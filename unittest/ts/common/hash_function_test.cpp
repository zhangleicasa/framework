#include <gtest/gtest.h>
#include "controller.h"

#ifdef TEST_HASH_FUNCTION

#include "hash_function.h"
#include "time_helper.h"
#include "x_log.h"

#ifndef DEBUG
void main() {
#else
TEST(HASH, Murmurhash){
#endif
    const char *instrument_id = "IF1603";
    unsigned long long secs1 = 0l;
    startTimer(&secs1);
    uint32_t hash_value   = ts::common::hash_function::murmurhash1(instrument_id, strlen(instrument_id), 0);
    ts::common::xlog::log(ts::common::LOG_ALL, "time: %f\n", getMilliSecond(secs1));
    uint32_t bucket_value = hash_value % 6151;

    unsigned long long secs2 = 0l;
    startTimer(&secs2);
    unsigned long hash_value1 = ts::common::hash_function::tshash(instrument_id, 0);
    ts::common::xlog::log(ts::common::LOG_ALL, "time: %f\n", getMilliSecond(secs2));

    EXPECT_EQ(bucket_value, 1030);
}

#endif
