#include <gtest/gtest.h>
#include "controller.h"

#include "cache_flow.h"
#include "log_helper.h"
#include "file_flow.h"

#ifdef TEST_CACHE_FLOW
#ifndef DEBUG
void main() {
#else
TEST(CacheFlow, Append){
#endif
    ts::common::cache_flow *cacheflow = new ts::common::cache_flow(2, 16);
    ts::common::file_flow  *fileflow  = new ts::common::file_flow("data/test");
    cacheflow->attach_under_flow(fileflow);
    cacheflow->append("123456789", 10);
    ts::common::xlog::log(ts::common::LOG_ALL, "pos_t length: %d\n", sizeof(fpos_t));
//    common::xlog::log(LOG_ALL, "%s\n", GetBinaryDumpInfo(cacheflow->get_buffer(0), 128).c_str());
    cacheflow->append("123", 4);
    cacheflow->append("12345679", 9);
//    common::xlog::log(LOG_ALL, "%s\n", GetBinaryDumpInfo(cacheflow->get_buffer(0), 128).c_str());
//    common::xlog::log(LOG_ALL, "%s\n", GetBinaryDumpInfo(cacheflow->get_buffer(1), 128).c_str());
    void *cc = new char[16];
    cacheflow->get(0, cc, 16);
    ts::common::xlog::log(ts::common::LOG_ALL, "0 buffer: %s\n", cc);
    cacheflow->get(1, cc, 16);
    ts::common::xlog::log(ts::common::LOG_ALL, "1 buffer: %s\n", cc);
}

#endif
