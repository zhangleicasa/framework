#include <gtest/gtest.h>
#include "controller.h"

#ifdef TEST_THREAD

#include "thread.h"
#include "synchronize.h"

#define PRINT_TIMES 3

const char *result = "running in the thread.";

class global_variable
{
public:
    global_variable() {global_variable_ = 0;};
    virtual ~global_variable() {};

private:
    int global_variable_;
#ifdef LINUX
    ts::sys::rw_lock rw_lock_;
#endif
};

class thread_test: public ts::sys::thread
{
public:
    thread_test() 
    {
        global_ = new global_variable();
    };
    ~thread_test() {};
    
    void run()
    {
        EXPECT_EQ("running in the thread.", result);
    }

    void read()
    {
        
    }

private:
    global_variable *global_;
};

#ifndef DEBUG
void main() {
#else
TEST(Thread, Run){
#endif
    thread_test othread;
    othread.create();
    othread.join();

}

void *func(void *args) {
	ts::sys::pthread_barrier *p = (ts::sys::pthread_barrier *)args;
	p->arrive();
	return NULL;
}

TEST(Condition, Run)
{
	const int count = 3;
	pthread_t threads[count];
	ts::sys::pthread_barrier *barrier = new ts::sys::pthread_barrier(count);
	for(int i = 0; i < count; ++i)
		pthread_create(&(threads[i]), NULL, func, barrier);
	for(int i = 0; i < count; ++i)
		pthread_join(threads[i], NULL);
	printf("\\\\\\\\\\\\\\\\\\..\n");
}

#endif
