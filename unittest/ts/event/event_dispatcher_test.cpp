#include <gtest/gtest.h>
#include "controller.h"

#ifdef TEST_EVENT_DISPATCHER

#include "epoll_reactor.h"
#include "event_handler.h"
#include "event_dispatcher.h"
#include "business_reactor.h"

class event_tester: public ts::event::event_handler
{
public:
	event_tester(ts::event::epoll_reactor *reactor)
		:event_handler(reactor) {}
	virtual int handle_event(int eventid, int nparam, void *params)
	{
		switch(eventid)
		{
		case 1:
			printf("[1] handle event in event tester.\n");
		case 2:
			printf("[2] handle event in event tester.\n");
		}
		return 0;
	}
    virtual int handle_input() { return 0; }
    virtual int handle_output() { return 0; }
    virtual int on_timer(int eventid) {return 0;}
};

class event_timer_tester: public ts::event::event_handler
{
public:
	event_timer_tester(ts::event::epoll_reactor *reactor)
		:event_handler(reactor) {}
	virtual int on_timer(int eventid)
	{
		printf("three second one time: 3333333333333333333333333\n");
		return 0;
	}
    virtual int handle_input() { return 0; }
    virtual int handle_output() { return 0; }
    virtual int handle_event(int eventid, int nparam, void *params) {return 0;}
};

class event_timer_tester1: public ts::event::event_handler
{
public:
	event_timer_tester1(ts::event::epoll_reactor *reactor)
		:event_handler(reactor) {}
	virtual int on_timer(int eventid)
	{
		printf("one second one time: 1111111111111111111111111\n");
		return 0;
	}
    virtual int handle_input() { return 0; }
    virtual int handle_output() { return 0; }
    virtual int handle_event(int eventid, int nparam, void *params) {return 0;}
};

class business_dispatcher_impl: public ts::event::business_reactor
{
public:
	business_dispatcher_impl(int time_interval):business_reactor(time_interval){}
	virtual void handle_business()
	{
		printf("two second on time handle_business: 2222222222\n");
	}
};

#ifdef DEBUG
TEST(EventDispatcher, Run) {
#else
void main() {
#endif
	ts::common::file_flow * fileflow = new ts::common::file_flow("./data/file.flow");
	ts::common::cache_flow* cache_flow = new ts::common::cache_flow(1024, 1024);
	cache_flow->attach_under_flow(fileflow);
    ts::event::epoll_reactor dispatcher("127.0.0.1", 5555);
    dispatcher.register_flow(cache_flow);
    dispatcher.create();
    event_tester *test = new event_tester(&dispatcher);
    test->post_event(2, 0, 0);
    event_timer_tester *test_timer = new event_timer_tester(&dispatcher);
    test_timer->set_timer(1, 3000);
    event_timer_tester1 *test_timer1 = new event_timer_tester1(&dispatcher);
    test_timer1->set_timer(2, 1000);
    ts::event::business_reactor *business = new business_dispatcher_impl(2000000);
    ts::event::event_interupter *inter = new ts::event::event_interupter(business);
    cache_flow->attach_interupter(inter);
    business->create();
    dispatcher.join();
    business->join();
}

#endif
