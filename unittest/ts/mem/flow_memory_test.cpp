#include <gtest/gtest.h>
#include "controller.h"

#ifdef FLOW_MEMORY_TEST

#include "x_log.h"
#include "allocator.h"
#include "smart_ptr.h"
#include "mem_table.h"
#include "buffer.h"

using namespace ts::common;

class pointer
{
public:
	pointer(int x, int y): x_(x), y_(y) {
		xlog::log(LOG_ALL, "create a memory of pointer.\n");
	}
	~pointer() {
		xlog::log(LOG_ALL, "last release the memory\n");
	}
	int x_;
	int y_;
};

#ifndef DEBUG
void main() {
#else
TEST(Allocator, flow){
#endif
    ts::common::xlog::log(ts::common::LOG_ALL, "[++++++++++] init in allocator.\n");
    ts::mem::memory_allocator * ar = new ts::mem::memory_allocator();
    ar->init();
    ar->allocate(1024*1024);
    ts::mem::mem_table *table = new ts::mem::mem_table(8, 16, ar);
	table->print_binary_info();
    pointer *p = new pointer(2, 3);
    printf("address: %p, %p, class address: %p\n", &(p->x_), &(p->y_), p);
	void *al = table->alloc();
	memset(al, 0x00, 8);
	strcpy((char *)al, (const char *)p);
	table->print_binary_info();
	{
		ts::common::smart_ptr<pointer> sp1(p);
		{
			ts::common::smart_ptr<pointer> sp2(sp1);
			{
				ts::common::smart_ptr<pointer> sp3 = sp2;
			}
		}
	}
	table->free(al);
	table->print_binary_info();
}

#endif
