#include <gtest/gtest.h>
#include "controller.h"

#ifdef TEST_MEMTABLE

#include "x_log.h"
#include "allocator.h"
#include "mem_table.h"

#ifndef DEBUG
void main() {
#else
TEST(MemTable, alloc){
#endif
	ts::common::xlog::log(ts::common::LOG_ALL, "[++++++++++] alloc in memtable.\n");
    const int maxsize = 1024;
    ts::mem::memory_allocator *ma = new ts::mem::memory_allocator();
    ma->init();
    ts::mem::mem_table *mt       = new ts::mem::mem_table(128, maxsize, ma);
    char *object = NULL;
    object =(char *)mt->alloc();

}

#endif
