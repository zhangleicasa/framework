#include <gtest/gtest.h>
#include "controller.h"

#ifdef TEST_ALLOCATOR

#include "x_log.h"
#include "allocator.h"

#ifndef DEBUG
void main() {
#else
TEST(Allocator, init){
#endif
    ts::common::xlog::log(ts::common::LOG_ALL, "[++++++++++] init in allocator.\n");
    ts::mem::memory_allocator * ar = new ts::mem::memory_allocator();
    ar->init();
    ar->allocate(12345);
}

#endif
