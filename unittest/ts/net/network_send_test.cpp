#include <gtest/gtest.h>
#include "controller.h"

#ifdef TEST_NETWORK_SEND

#include "epoll_reactor.h"
#include "ts_client.h"
#include "base_package.h"
#include "event_handler.h"

class heart_session: public ts::event::event_handler
{
public:
	heart_session(ts::event::event_dispatcher *reactor)
		: event_handler(reactor)
	{
		client_ = new ts::net::ts_client();
	}

	virtual int handle_event(int eventid, int nparam, void *params)
	{
		printf("handle event.....\n");
		return 0;
	}

	virtual int on_timer(int eventid)
	{
		printf("handle timer event..............\n");
		char buffer[32] = "abcdefghijklmn......";
		ts::protocol::base_package *package = new ts::protocol::base_package();
		package->construct_allocate(100, 200);
		package->write(buffer, 32);
		package->set_version(0Xfffe);
		package->make_package();
		client_->ts_send(package);
		return 0;
	}

    virtual int handle_input() {return 0;};
    virtual int handle_output() {return 0;};

private:
	ts::net::ts_client *client_;

};

#ifdef DEBUG
TEST(TS_CLIENT, Run) {
#else
void main() {
#endif
    ts::event::epoll_reactor dispatcher("127.0.0.1", 5555);
    dispatcher.create();
    heart_session *session_1 = new heart_session(&dispatcher);
    session_1->set_timer(1, 1000);
    dispatcher.join();
}


#endif
