/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_NONCOPYABLE_H_
#define TS_NONCOPYABLE_H_

namespace ts {
    class noncopyable
    {
    protected:
        noncopyable() {};
        ~noncopyable() {};
    
    private:
        noncopyable(const noncopyable &);
        const noncopyable& operator=(const noncopyable &);
    };
}//ts

#endif
