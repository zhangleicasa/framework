/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_PROTOCOL_BASE_PROTOCOL_H_
#define TS_PROTOCOL_BASE_PROTOCOL_H_

#include "protocol.h"
#include "file_flow.h"
#include "cache_flow.h"
#include "base_package.h"

namespace ts {
namespace protocol {
	class base_protocol : public protocol {
	public:
		base_protocol(net::channel *pchannel, common::cache_flow *cache_flow);
        bool handle_input();
	private:
        //todo: cache flow will be replaced by subscriber.
        common::cache_flow *cache_flow_;
        base_package  base_package_;
        net::channel *channel_;

	};
}
}

#endif /* TS_PROTOCOL_BASE_PROTOCOL_H_ */
