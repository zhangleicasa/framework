#include "base_package.h"

namespace ts {
namespace protocol {
	void base_package::construct_allocate(int capacity, int reserve)
	{
		buffer_ = new mem::buffer(capacity + reserve);
		reset_package_buffer();
	}

	void base_package::make_package()
	{
		int header_len = sizeof(base_header);
		head_ = head_ - header_len;
		set_length(tail_ - head_);
		header_.to_stream(head_);
	}

	void base_package::reset_package_buffer()
	{
		if(NULL == buffer_)
		{
			head_ = NULL;
			tail_ = NULL;
		}
		else
		{
			head_ = buffer_->address() + buffer_->get_length();
			tail_ = head_;
		}
	}

	bool base_package::read_from_channel(net::channel *pchannel)
	{
		int length = pchannel->read_data(buffer_);
		//todo: one more memory copy waste performance.
		//here we write the data into the cache flow.
		write(buffer_->address(), length);
		printf("binary: length:[%d]\n%s\n", length, GetBinaryDumpInfo(head_, length).c_str());
		return (length == 0) ? false : true;
	}

}
}

