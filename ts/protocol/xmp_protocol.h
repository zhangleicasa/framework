/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_PROTOCOL_XMP_PROTOCOL_H_
#define TS_PROTOCOL_XMP_PROTOCOL_H_

#include "x_log.h"
#include "public.h"
#include "protocol.h"

#include "stdint.h"
#include "string.h"

namespace ts {
namespace protocol {
    typedef struct xmp_header
    {
        uint8_t  type_;
        uint8_t  extension_len_;
        uint16_t length_;
    }xmp_header;
    
    class xmp_protocol : public protocol
    {
    public:
        xmp_protocol();
        virtual ~xmp_protocol();

        virtual int decode(char *buffer);

        virtual int header_to_struct(char *head);

    public:
        virtual void debug();

    private:
        xmp_header header_;

    };

}
}

#endif
