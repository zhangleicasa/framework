/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_PROTOCOL_BASE_PACKAGE_H_
#define TS_PROTOCOL_BASE_PACKAGE_H_

#include "buffer.h"
#include "channel.h"
#include "log_helper.h"
#include "socket_types.h"

namespace ts {
namespace protocol {
	struct base_header
	{
		int version_;
		int length_;
		void to_stream(char *buffer)
		{
			base_header * pheader = (base_header *)buffer;
			pheader->version_ = version_;
			pheader->length_  = length_;
		}
	};
	class base_package
	{
	public:
		base_package(): buffer_(NULL), head_(NULL), tail_(NULL) {}
		/** allocate a buffer to construct the package. **/
		void construct_allocate(int capacity, int reserve);
		/** add the package header to the buffer. **/
		void make_package();
		/** reset package buffer: ............head(tail) **/
		void reset_package_buffer();
		/** read the data from network channel. **/
		bool read_from_channel(net::channel *pchannel);

		/** inline set the version of the base package. **/
		inline void set_version(int version)
		{
			header_.version_ = version;
		}
		/** inline get the version of the base package. **/
		inline int get_version()
		{
			return header_.version_;
		}
		/** inline set the length of the base package. **/
		inline void set_length(int length)
		{
			header_.length_ = length;
		}
		/** inline get the length of the base package. **/
		inline int  get_length()
		{
			return tail_ - head_;
		}
		/** inline get start of the package buffer. **/
		inline char *address()
		{
			return head_;
		}
		/** inline write the buffer into package. **/
		inline void write(char *buffer, int n)
		{
			head_ = head_ - n;
			memcpy(head_, buffer, n);
		}

	protected:
		mem::buffer *buffer_;             //buffer of the package
		base_header  header_;              //header of the package
		char *head_;                      //head of the package buffer
		char *tail_;                      //tail of the package buffer
	};

}
}

#endif /* TS_PROTOCOL_BASE_PACKAGE_H_ */
