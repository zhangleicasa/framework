#include "xmp_protocol.h"

#include "stdio.h"

namespace ts {
namespace protocol {
    xmp_protocol::xmp_protocol()
    {

    }

    xmp_protocol::~xmp_protocol()
    {
        
    }

    int xmp_protocol::decode(char * buffer)
    {
        xmp_header *header = (xmp_header *)buffer;
        common::xlog::log(common::LOG_ALL, "package length: %x\n", header->length_);
		return 0;
    }

    int xmp_protocol::header_to_struct(char *head)
    {
    	memcpy(&header_, head, sizeof(xmp_header));

    	header_.type_           = swap_endian_p<uint8_t>(header_.type_);
    	header_.length_         = swap_endian_p<uint16_t>(header_.length_);
    	header_.extension_len_  = swap_endian_p<uint8_t>(header_.extension_len_);
		return 0;
    }

    void xmp_protocol::debug()
    {
    	common::xlog::log(common::LOG_ALL, "===xmp header===\n");
    	common::xlog::log(common::LOG_ALL, "   pid: %d\n", header_.type_);
    	common::xlog::log(common::LOG_ALL, "length: %d\n", header_.length_);
    	common::xlog::log(common::LOG_ALL, " exlen: %d\n", header_.extension_len_);
    	common::xlog::log(common::LOG_ALL, "===xmp header===\n\n\n");
    }

}
}

