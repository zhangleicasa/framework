/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_PROTOCOL_PROTOCOL_H_
#define TS_PROTOCOL_PROTOCOL_H_

#include "public.h"

namespace ts {
namespace protocol {
    class protocol
    {
    public:
        protocol(): head_(NULL), tail_(NULL) {}
        virtual ~protocol() {}
        virtual int decode(char *buffer){return 0;}
        virtual int encode(){return 0;}
        virtual int header_to_struct(char *head){return 0;}
        virtual void debug(){return;}

    protected:
        char*  head_;
        char*  tail_;
    
    };
}//protocol
}//ts

#endif /* TS_PROTOCOL_PROTOCOL_H_ */
