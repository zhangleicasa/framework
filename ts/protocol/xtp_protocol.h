/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_PROTOCOL_XTP_PROTOCOL_H_
#define TS_PROTOCOL_XTP_PROTOCOL_H_

#include "x_log.h"
#include "public.h"
#include "protocol.h"

#include <stdint.h>
#include <string.h>
#include <stdio.h>

namespace ts {
namespace protocol {
	typedef struct xtp_header
    {
		uint8_t   type_;
		uint8_t	  chain_;
		uint16_t  content_length_;
		uint32_t  subject_id_;
		uint32_t  tid_;
		uint32_t  comm_phase_no_;
		uint32_t  sequence_no_;
		uint32_t  request_id_;
		uint32_t  session_id_;
		uint32_t  front_id_;
		uint32_t  group_id_;
		uint16_t  old_ft_type_;
		uint16_t  ft_type_;
		uint16_t  publish_node_id_;
		uint16_t  match_id_;
		uint32_t  work_id_;
		uint32_t  old_subject_id_;
		uint32_t  convergence_id_;
		uint32_t  old_sequence_no_;
    }xtp_header;

    class xtp_protocol : public protocol
    {
    public:
        xtp_protocol();
        virtual ~xtp_protocol();
        int header_to_struct(char *head);

    public:
        virtual void debug();

    private:
        xtp_header header_;
    };
}
}

#endif
