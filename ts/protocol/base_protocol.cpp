#include "base_protocol.h"

namespace ts {
namespace protocol {
	base_protocol::base_protocol(net::channel *pchannel, common::cache_flow *cache_flow)
	{
		channel_ = pchannel;
		base_package_.construct_allocate(1000, 200);
		cache_flow_ = cache_flow;
	}

	bool base_protocol::handle_input()
	{
		base_package_.reset_package_buffer();
		bool is_receive = base_package_.read_from_channel(channel_);
//		printf("get: \n%s\n", GetBinaryDumpInfo(base_package_.address(), base_package_.get_length()).c_str());
		if(true == is_receive)
			cache_flow_->append(base_package_.address(), base_package_.get_length());
		return is_receive;
	}

}
}

