#include "xtp_protocol.h"

namespace ts {
namespace protocol {
    xtp_protocol::xtp_protocol()
    {
        
    }
    
    xtp_protocol::~xtp_protocol()
    {
        
    }

    int xtp_protocol::header_to_struct(char *head)
    {
    	memcpy(&header_, head, sizeof(xtp_header));
    	header_.type_            = swap_endian_p<uint8_t>(header_.type_);
    	header_.chain_           = swap_endian_p<uint8_t>(header_.chain_);
    	header_.content_length_  = swap_endian_p<uint16_t>(header_.content_length_);
    	header_.subject_id_      = swap_endian_p<uint32_t>(header_.subject_id_);
    	header_.tid_             = swap_endian_p<uint32_t>(header_.tid_);
    	header_.comm_phase_no_   = swap_endian_p<uint32_t>(header_.comm_phase_no_);
    	header_.sequence_no_     = swap_endian_p<uint32_t>(header_.sequence_no_);
    	header_.request_id_      = swap_endian_p<uint32_t>(header_.request_id_);
    	header_.session_id_      = swap_endian_p<uint32_t>(header_.session_id_);
    	header_.front_id_        = swap_endian_p<uint32_t>(header_.front_id_);
    	header_.group_id_        = swap_endian_p<uint32_t>(header_.group_id_);
    	header_.old_ft_type_     = swap_endian_p<uint16_t>(header_.old_ft_type_);
    	header_.ft_type_         = swap_endian_p<uint16_t>(header_.ft_type_);
    	header_.publish_node_id_ = swap_endian_p<uint16_t>(header_.publish_node_id_);
    	header_.match_id_        = swap_endian_p<uint16_t>(header_.match_id_);
    	header_.work_id_         = swap_endian_p<uint32_t>(header_.work_id_);
    	header_.old_subject_id_  = swap_endian_p<uint32_t>(header_.old_subject_id_);
    	header_.convergence_id_  = swap_endian_p<uint32_t>(header_.convergence_id_);
    	header_.old_sequence_no_ = swap_endian_p<uint32_t>(header_.old_sequence_no_);
		return 0;
    }

    void xtp_protocol::debug()
    {
    	common::xlog::log(common::LOG_ALL, "===xtp header===\n");
    	common::xlog::log(common::LOG_ALL, "            pid: %d\n", header_.type_);
    	common::xlog::log(common::LOG_ALL, "          chain: %d\n", header_.chain_);
    	common::xlog::log(common::LOG_ALL, " content_length: %d\n", header_.content_length_);
    	common::xlog::log(common::LOG_ALL, "     subject_id: %d\n", header_.subject_id_);
    	common::xlog::log(common::LOG_ALL, "            tid: %x\n", header_.tid_);
    	common::xlog::log(common::LOG_ALL, "  comm_phase_no: %d\n", header_.comm_phase_no_);
    	common::xlog::log(common::LOG_ALL, "    sequence_no: %d\n", header_.sequence_no_);
    	common::xlog::log(common::LOG_ALL, "     request_id: %d\n", header_.request_id_);
    	common::xlog::log(common::LOG_ALL, "     session_id: %d\n", header_.session_id_);
    	common::xlog::log(common::LOG_ALL, "       front_id: %d\n", header_.front_id_);
    	common::xlog::log(common::LOG_ALL, "       group_id: %d\n", header_.group_id_);
    	common::xlog::log(common::LOG_ALL, "    old_ft_type: %d\n", header_.old_ft_type_);
    	common::xlog::log(common::LOG_ALL, "        ft_type: %d\n", header_.ft_type_);
    	common::xlog::log(common::LOG_ALL, "publish_node_id: %d\n", header_.publish_node_id_);
    	common::xlog::log(common::LOG_ALL, "       match_id: %d\n", header_.match_id_);
    	common::xlog::log(common::LOG_ALL, "        work_id: %d\n", header_.work_id_);
    	common::xlog::log(common::LOG_ALL, " old_subject_id: %d\n", header_.old_subject_id_);
    	common::xlog::log(common::LOG_ALL, " convergence_id: %d\n", header_.convergence_id_);
    	common::xlog::log(common::LOG_ALL, "old_sequence_no: %d\n", header_.old_sequence_no_);
    	common::xlog::log(common::LOG_ALL, "===xtp header===\n\n\n");
    }
}
}
