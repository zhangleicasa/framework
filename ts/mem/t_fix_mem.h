/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_MEM_CFIX_MEM_H_
#define TS_MEM_CFIX_MEM_H_

#include "x_log.h"
#include "public.h"
#include "allocator.h"
#include "log_helper.h"

namespace ts {
namespace mem {
	/** point to start address of the free space. **/
	typedef struct free_struct
	{
		struct free_struct *next_;
	}free_struct;

	/** at the head of non-first mem_table. **/
	struct mem_linker
	{
		free_struct  *head_;
		mem_linker *next_;
	};

	/** at the head of first mem_table. **/
	typedef struct fix_head
	{
		int unitsize_;                                              //size of unit space.
		int maxunit_;                                               //how many units will be stored.
		int storeunitsize_;                                         //size of unit which align.
		int alloccount_;                                            //actually used chuck number.
		int chunknumber_;                                           //ever allocate unit number.
		free_struct *head_;
		mem_linker *nextchuck_;
	}fix_head;

	/** cfix_mem which will be used as std::allocator. **/
	template<typename K>
	class cfix_mem
	{
    public:
        template<typename K_Node>
        struct rebind
        { 
            typedef cfix_mem<K_Node> other; 
        };
	public:
		cfix_mem();
        virtual ~cfix_mem();

		void  initialmemoryblock(int blockid);
		void *allocate();
        void *allocate(int _n, const void* = 0);
		void  deallocate(const void *object, unsigned unitsize);
        void  destroy(const void *object);
		void  set_block_used_state(const void *object, bool used);
		bool  get_block_used_state(int blockid);
		void *get_object_by_id(int id);

        void print_binary_info()
        {
        	common::xlog::log(common::LOG_ALL, "fixmemhead[%p], headsize[%d], blockindexsize[%d], maxunit_*unitsize_[%d]\n%s\n",
        			head_, sizeof(fix_head), blockindexsize_, maxunit_*unitsize_,
        	GetBinaryDumpInfo((char *)head_, sizeof(fix_head) + blockindexsize_ + maxunit_*unitsize_).c_str());
        	for(mem_linker *link = head_->nextchuck_; link != NULL; link = link->next_)
        	{
            	common::xlog::log(common::LOG_ALL, "chunkhead[%p], fixmemlinkersize[%d], blockindexsize[%d], maxunit_*unitsize_[%d]\n%s\n",
            			link, sizeof(mem_linker), blockindexsize_, maxunit_*unitsize_,
            	GetBinaryDumpInfo((char *)link, sizeof(mem_linker) + blockindexsize_ + maxunit_*unitsize_).c_str());
        	}
        }

	private:
        int   get_block_id(const void *object);
        void  increase_used_block();
        void  decrease_used_block();

    private:
        std::vector<char *> memoryvec_;

        //allocator  *allocator_;                                     //allocator which will be invoked.
        fix_head   *head_;                                          //head of the mem_table.
        int         unitsize_;                                      //size of record unit.
        int         maxunit_;                                       //number of unit number.
        int         blockindexsize_;                                //index of used chunck state.
        char       *usedblockinfooffset_;                           //bit map of block usage.
	};

	template<typename K>
	cfix_mem<K>::cfix_mem()
    {
        //[todo]: the 32 is defined for binary tree, it should not be defined here.
        unitsize_            = ((sizeof(K)-1)/sizeof(void *) + 1)*sizeof(void *);
        maxunit_             = 4;
        g_memory_allocator::get_instance()->init();
        blockindexsize_      = 0;// use bit map to record the block index.
        head_                = NULL;
        usedblockinfooffset_ = NULL;

        initialmemoryblock(0);
    }

	template<typename K>
	cfix_mem<K>::~cfix_mem()
    {

    }

	template<typename K>
    void
    cfix_mem<K>::initialmemoryblock(int blockid)
    {
        free_struct *cur    = NULL;
        free_struct *next   = NULL;

        /** calculate how much space this mem_table object need.**/
        int storeunitsize = 0;
        int totalsize     = 0;

        storeunitsize     = ((unitsize_-1)/8 + 1)*8;
        blockindexsize_   = ((maxunit_-1)/64 + 1)*8;

        if(blockid == 0)
            totalsize = storeunitsize*maxunit_ + sizeof(fix_head) + blockindexsize_;
        else
            totalsize = storeunitsize*maxunit_ + sizeof(mem_linker) + blockindexsize_;

        /** allocate totalsize space from allocator module and set the layout for it. **/
        char *mem = NULL;
        mem = (char *)(g_memory_allocator::get_instance()->allocate(totalsize));

        if(blockid == 0)
            head_ = (fix_head *)mem;

        /** we need init the head of mem_table if it's the first block. **/
        if(blockid == 0)
        {
            /**  bit map of block usage.  **/
            usedblockinfooffset_ = (char *)(head_ + 1);
            ::memset(usedblockinfooffset_, 0, blockindexsize_);

            /**  address of the used space address.  **/
            memoryvec_.push_back((char *)(usedblockinfooffset_ + blockindexsize_));

            head_->unitsize_       = unitsize_;
            head_->maxunit_        = maxunit_;
            head_->storeunitsize_  = storeunitsize;
            head_->alloccount_     = 0;
            head_->chunknumber_    = 1;
            head_->head_           = (free_struct *)memoryvec_[0];
            common::xlog::log(common::LOG_ALL, "head: %p\n", head_->head_);
            cur                    = (free_struct *)memoryvec_[0];
            head_->nextchuck_      = NULL;
        }
        else
        {
            mem_linker *linker = (mem_linker *)mem;
            linker->next_        = NULL;
            linker->head_        = (free_struct *)(mem + sizeof(mem_linker) + blockindexsize_);
            ::memset(mem + sizeof(mem_linker), 0, blockindexsize_);
            memoryvec_.push_back((char *)linker->head_);
            cur = linker->head_;

            mem_linker **linknext = &(head_->nextchuck_);
            for(int i = 1; i< head_->chunknumber_; i++)
                linknext = &(*linknext)->next_;

            *linknext = linker;
            head_->chunknumber_++;

            head_->head_ = cur;

        }

        for(int j = 0; j < maxunit_; j++)
        {
            if(j == maxunit_ - 1)
                next = NULL;
            else
                next = (free_struct *)((char *)(cur) + storeunitsize);

            cur->next_  = next;
            cur         = next;
        }
    }


	template<typename K>
    void *
    cfix_mem<K>::allocate()
    {
        free_struct *ret 	= NULL;
        if(head_->head_ == NULL)
            initialmemoryblock(head_->chunknumber_);

        ret             = head_->head_;
        head_->head_    = head_->head_->next_;

        increase_used_block();
        set_block_used_state(ret, true);

        return ret;
    }

    template<typename K>
    void *
    cfix_mem<K>::allocate(int _n, const void* o)
    {
        free_struct *ret 	= NULL;
        if(head_->head_ == NULL)
            initialmemoryblock(head_->chunknumber_);

        ret             = head_->head_;
        head_->head_    = head_->head_->next_;

        increase_used_block();
        set_block_used_state(ret, true);

        return ret;        
    }

	template<typename K>
    void
    cfix_mem<K>::deallocate(const void *object, unsigned unitsize)
    {
        free_struct *cur  = NULL;
        cur             = (free_struct *)object;
        cur->next_      = head_->head_;
        head_->head_    = cur;
        decrease_used_block();
        set_block_used_state(object, false);
    }

    template<typename K>
    void
    cfix_mem<K>::destroy(const void *object)
    {
        free_struct *cur  = NULL;
        cur             = (free_struct *)object;
        cur->next_      = head_->head_;
        head_->head_    = cur;
        decrease_used_block();
        set_block_used_state(object, false);
    }

	template<typename K>
    void
    cfix_mem<K>::set_block_used_state(const void *object, bool used)
    {
        int blockid = get_block_id(object);
        int chunckid = blockid/maxunit_;
        char *p = memoryvec_[chunckid] - blockindexsize_ + ((blockid%maxunit_)/8);
        int bit = blockid%maxunit_%8;
        if(used)
            (*p) |= (1 << (7-bit));
        else
            (*p) &= ~(1 << (7-bit));
    }

	template<typename K>
    bool
    cfix_mem<K>::get_block_used_state(int blockid)
    {
        int chunckid = blockid / maxunit_;
        char *p = memoryvec_[chunckid] - blockindexsize_ + (blockid%maxunit_)/8;
        int bit = blockid%maxunit_%8;
        return (((*p) & (1<<(7-bit))) != 0);
    }

	template<typename K>
    int
    cfix_mem<K>::get_block_id(const void *object)
    {
        int offset = 0;
        std::vector<char *>::iterator it = find(memoryvec_.begin(), memoryvec_.end(), object);
        /** todo: compare the address by binary search. **/
        if(it == memoryvec_.end())
            it --;
        else
        {
            if(*it != object)
                it --;
        }
        offset = distance(memoryvec_.begin(), it);
        return offset*maxunit_ + ((char *)object - memoryvec_[offset])/head_->storeunitsize_;
    }

	template<typename K>
	void *
    cfix_mem<K>::get_object_by_id(int id)
    {
        free_struct *pcur = NULL;
        pcur = (free_struct *)(memoryvec_[id/head_->maxunit_]+(id%head_->maxunit_)*(head_->storeunitsize_));
        if(!get_block_used_state(id))
            return NULL;
        else
            return pcur;
    }

	template<typename K>
	void
    cfix_mem<K>::increase_used_block()
    {
        head_->alloccount_++;
    }

	template<typename K>
	void
    cfix_mem<K>::decrease_used_block()
    {
        head_->alloccount_--;
    }

}
}

#endif
