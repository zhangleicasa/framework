#include "mem_table.h"

namespace ts {
namespace mem {
    mem_table::mem_table(int unitsize, int maxunit, allocator *allc)
    {
        unitsize_            = unitsize;
        maxunit_             = maxunit;
        allocator_           = allc;
        blockindexsize_      = 0;// use bit map to record the block index.
        head_                = NULL;
        usedblockinfooffset_ = NULL;
        init(0);
    }
    
    void mem_table::init(int blockid)
    {
        tfreehead *cur,*next = NULL;
        /** calculate how much space this mem_table object need.**/
        int storeunitsize = ((unitsize_-1)/8 + 1)*8;
        blockindexsize_   = ((maxunit_-1)/64 + 1)*8;
        int totalsize     = storeunitsize*maxunit_ + blockindexsize_;
        totalsize += (blockid == 0 ? sizeof(fixmemhead) : sizeof(fixmemlinker));
        /** allocate space from allocator module and set the layout for it. **/
        char *mem = (char *)(allocator_->allocate(totalsize));
        if(blockid == 0)
        {/** we need init the head of mem_table if it's the first block. **/
        	head_ = (fixmemhead *)mem;
            usedblockinfooffset_ = (char *)(head_ + 1);
            ::memset(usedblockinfooffset_, 0, blockindexsize_);
            /**  address of the used space address.  **/
            memoryvec_.push_back((char *)(usedblockinfooffset_ + blockindexsize_));
            head_->unitsize_       = unitsize_;
            head_->maxunit_        = maxunit_;
            head_->storeunitsize_  = storeunitsize;
            head_->alloccount_     = 0;
            head_->chunknumber_    = 1;
            head_->head_ = cur     = (tfreehead *)memoryvec_[0];
            head_->nextchuck_      = NULL;
        }
        else
        {/** we need init the head of mem_table if it's the first block. **/
            fixmemlinker *linker = (fixmemlinker *)mem;
            linker->next_        = NULL;
            linker->head_        = (tfreehead *)(mem + sizeof(fixmemlinker) + blockindexsize_);
            ::memset(mem + sizeof(fixmemlinker), 0, blockindexsize_);
            memoryvec_.push_back((char *)linker->head_);
            cur = linker->head_;
            fixmemlinker **linknext = &(head_->nextchuck_);
            for(int i = 1; i< head_->chunknumber_; i++)
                linknext = &(*linknext)->next_;
            *linknext = linker;
            head_->chunknumber_++;
            head_->head_ = cur;
        }
        for(int j = 0; j < maxunit_; ++j)
        {
        	next = j == maxunit_ - 1 ? NULL : (tfreehead *)((char *)(cur) + storeunitsize);
            cur->next_  = next;
            cur         = next;
        }
    }
    
    void *mem_table::alloc()
    {
        tfreehead *ret 	= NULL;
        if(head_->head_ == NULL)
        	init(head_->chunknumber_);
        ret             = head_->head_; 
        head_->head_    = head_->head_->next_;
        increase_used_block();
        set_block_used_state(ret, true);
        return ret;
    }
    
    void mem_table::free(const void *object)
    {
        tfreehead *cur  = NULL;
        cur             = (tfreehead *)object;
        cur->next_      = head_->head_;
        head_->head_    = cur;
        decrease_used_block();
        set_block_used_state(object, false);
    }
    
    void mem_table::set_block_used_state(const void *object, bool used)
    {
        int blockid = get_block_id(object);
        int chunckid = blockid/maxunit_;
        char *p = memoryvec_[chunckid] - blockindexsize_ + ((blockid%maxunit_)/8);
        int bit = blockid%maxunit_%8;
        (*p) = used == true ? (*p) | (1 << (7-bit)) : (*p) & ~(1 << (7-bit));
    }
    
    bool mem_table::get_block_used_state(int blockid)
    {
        int chunckid = blockid / maxunit_;
        char *p = memoryvec_[chunckid] - blockindexsize_ + (blockid%maxunit_)/8;
        int bit = blockid%maxunit_%8;
        return (((*p) & (1<<(7-bit))) != 0);
    }
    
    int mem_table::get_block_id(const void *object)
    {
        int offset = 0;
        std::vector<char *>::iterator it = find(memoryvec_.begin(), memoryvec_.end(), object);
        /** todo: compare the address by binary search. **/
        if(it == memoryvec_.end() || *it != object)
            it --;
        offset = distance(memoryvec_.begin(), it);
        return offset*maxunit_ + ((char *)object - memoryvec_[offset])/head_->storeunitsize_;
    }
    
    const void *mem_table::get_object_by_id(const int id)
    {
        tfreehead *p = NULL;
        p  = (tfreehead *)(memoryvec_[id/head_->maxunit_]+(id%head_->maxunit_)*(head_->storeunitsize_));
        return get_block_used_state(id) ? p  : NULL;
    }
    
    void mem_table::print_binary_info()
    {
    	common::xlog::log(common::LOG_ALL, "fix mem head==========================\n"
    			"fixmemhead[%p], headsize[%d], blockindexsize[%d], maxunit_*unitsize_[%d]\n%s\n",
    			head_, sizeof(fixmemhead), blockindexsize_, maxunit_*unitsize_,
    	GetBinaryDumpInfo((char *)head_, sizeof(fixmemhead) + blockindexsize_ + maxunit_*unitsize_).c_str());
    	for(fixmemlinker *link = head_->nextchuck_; link != NULL; link = link->next_)
    	{
        	common::xlog::log(common::LOG_ALL, "chunkhead[%p], fixmemlinkersize[%d], blockindexsize[%d], maxunit_*unitsize_[%d]\n%s\n",
        			link, sizeof(fixmemlinker), blockindexsize_, maxunit_*unitsize_,
        	GetBinaryDumpInfo((char *)link, sizeof(fixmemlinker) + blockindexsize_ + maxunit_*unitsize_).c_str());
    	}
    }

}
}
