#include "allocator.h"

namespace ts {
namespace mem {
    memory_allocator::memory_allocator(): head_(NULL), start_(NULL), end_(NULL)
    {//check whether the bit line is 4 or 8
        if(sizeof(void *) == 4)
        {
            memory_size_   = DEFAULT_MEMORY_SIZE_32*1024l*1024l;
            maxblockcount_ = DEFAULT_MAX_BLOCK_COUNT_32;
        }
        else if(sizeof(void *) == 8)
        { 
            memory_size_   = DEFAULT_MEMORY_SIZE_64*1024l*1024l;
            maxblockcount_ = DEFAULT_MAX_BLOCK_COUNT_64;
        }
        else
        {
        	common::xlog::log(common::LOG_ALL, "unknown machine word size.\n");
        }
        common::xlog::log(common::LOG_ALL, "memory size of allocator is %d MB\n", DEFAULT_MEMORY_SIZE_64);
        //[TODO:] support global configuration setting.
    }

    void *memory_allocator::allocate(int size)
    {
        void *empty            = head_->emptyaddress_;
        head_->emptyaddress_   = (char *)empty + size;
        if(head_->emptyaddress_ > end_)
        	common::xlog::log(common::LOG_ALL, "insufficient memory space controcled by allocator\n");
        head_->blockvec_[head_->usedblocks_].startposition_ = empty;
        head_->usedblocks_++;
        return empty;
    }
    
    void memory_allocator::init()
    {
    	start_               = new char[memory_size_];
        head_                = (memory_allocator_head *)start_;
        head_->version_      = 1;
        head_->usedblocks_   = RESERVE_BLOCK_ID;
        head_->emptyaddress_ = start_ + sizeof(memory_allocator_head) + sizeof(usedblockstruct)*(maxblockcount_ - 1);
        /********************************************memory_allocator_head***********************************************/
        /***|start_(point_)  |version_  |usedblocks_  |emptyaddress_ |blockvec_[0] |blockvec_[1] |blockvec_[2] |      ***/
        /***----------------------------------------------------------------------------------------------------------***/                         
        /***|blockvec_[3] |blockvec_[4] |blockvec_[5].........from block[5], it point to the certain data.            ***/
        /****************************************************************************************************************/
        for(int i = 0; i < maxblockcount_; i++)
            head_->blockvec_[i].startposition_ = NULL;
        end_ = start_ + memory_size_;
    }

}
}
