/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_MEM_ALLOCATOR_H_
#define TS_MEM_ALLOCATOR_H_

#include "x_log.h"
#include "public.h"
#include "singleton.h"

#define DEFAULT_MEMORY_SIZE_32 			(512)
#define DEFAULT_MEMORY_SIZE_64			(4*1024)
#define DEFAULT_MAX_BLOCK_COUNT_32		(16*1024)
#define DEFAULT_MAX_BLOCK_COUNT_64		(1024*1024)

#define	RESERVE_BLOCK_ID				5 

namespace ts {
namespace mem {
    struct usedblockstruct
    {
        void *startposition_;
    };
    
    struct memory_allocator_head
    {
        int version_;                                     //version of this allocator.
        int usedblocks_;                                  //used blocks
        void *emptyaddress_;                              //pointer of empty address.
        usedblockstruct blockvec_[1];                     //pointer to used blocks.
    };
    
    class allocator
    {
    public:
    	virtual ~allocator() {}
        virtual void *allocate(int size) = 0;
        virtual void init() = 0;
    };
    
    class memory_allocator: public allocator
    {//todo: shared allocator not supported yet.
    public:
        memory_allocator();
        virtual void *allocate(int size);
        virtual void init();
    protected:
        memory_allocator_head *head_;
        char                  *start_;
        char                  *end_;
        unsigned long          memory_size_;
        int                    maxblockcount_;
    };

    class g_memory_allocator: public memory_allocator,
    	public singleton<g_memory_allocator>{};
}
}

#endif
