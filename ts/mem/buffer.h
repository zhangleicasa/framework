/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_MEM_BUFFER_H_
#define TS_MEM_BUFFER_H_

#include "public.h"

#define FLOW_BUFFER_SIZE 8*1024

namespace ts {
namespace mem {
    class buffer
    {
    public:
        buffer(unsigned int capacity = FLOW_BUFFER_SIZE)
            :start_(NULL), capacity_(capacity), loc_(0)
        {
            start_ = (char *)malloc(capacity);
            memset(start_, 0, capacity_);
            start_[capacity] = '\0';
        }

        buffer(char *data, unsigned capacity): loc_(0)
        {
        	start_    = data;
        	capacity_ = capacity;
        }

        virtual ~buffer()
        {
            free(start_);
        }

        inline bool enough_space(int len)
        {
            return loc_+len > capacity_ ? false : true;
        }

        inline char *append(const char *p, int len)
        {
            char *current_address = start_ + loc_;
            memcpy(start_+loc_, p, len);
            loc_+=len;
            return current_address;
        }

        inline char *address()
        {
            return start_;
        }
        
        inline unsigned get_length()
        {
        	return capacity_;
        }

        inline char *get_top()  
        {
            return start_+loc_;
        }

    private:
        char            *start_;
        unsigned int     capacity_;
        unsigned int     loc_;
    };

}
}

#endif
