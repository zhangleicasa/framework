/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_MEM_MEM_TABLE_H_
#define TS_MEM_MEM_TABLE_H_

#include "public.h"
#include "allocator.h"
#include "log_helper.h"

namespace ts {
namespace mem {
    /** point to start address of the free space. **/
    typedef struct tfreeheadstruct
    {
        struct tfreeheadstruct *next_;
    }tfreehead;
    
    /** at the head of non-first mem_table. **/
    struct fixmemlinker
    {
        tfreehead *head_;
        fixmemlinker *next_;
    };
    
    /** at the head of first mem_table. **/
    typedef struct tfixmemheadstruct
    {
        int unitsize_;                     //size of unit space.
        int maxunit_;                      //how many units will be stored.
        int storeunitsize_;                //size of unit which align.
        int alloccount_;                   //actually used chuck number.
        int chunknumber_;                  //ever allocate unit number.
        tfreehead *head_;
        fixmemlinker *nextchuck_;
    }fixmemhead;
    
    /** mem_table allocate a space from allocator object. **/
    class mem_table
    {
    public:
        mem_table(int unitsize, int maxunit, allocator *allc);
		void init(int blockid);
        /** invoke the function to dispatch the space. **/
        void *alloc();
        void free(const void *object);
        void set_block_used_state(const void *object, bool used);
        bool get_block_used_state(int blockid);
        const void *get_object_by_id(const int id);
        void print_binary_info();
    private:
        int get_block_id(const void *object);
        inline void increase_used_block() { head_->alloccount_++; }
        inline void decrease_used_block() { head_->alloccount_--; }
    private:
        std::vector<char *> memoryvec_;
        allocator  *allocator_;                     //allocator which will be invoked.
        fixmemhead *head_;                          //head of the mem_table.
        int         unitsize_;                      //size of record unit.
        int         maxunit_;                       //number of unit number.
        int         blockindexsize_;                //index of used chunck state.
        char       *usedblockinfooffset_;           //bit map of block usage.
    };

}
}

#endif
