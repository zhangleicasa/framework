#include "xml_parser.h"

namespace ts{
namespace common{
    xml_parser::xml_parser() : xmldoc_(NULL), root_(NULL)
    {
    
    }
    
    xml_parser::~xml_parser()
    {
    
    }
    
    int xml_parser::parse_file(const char *filename)
    {
        if(tinyxml2::XML_SUCCESS != xmldoc_.LoadFile(filename))
        {
            return -1;
        }
        if(NULL == (root_ = xmldoc_.RootElement()))
        {
            return -1;
        }
        return 0;
    }
    
    int xml_parser::parse_buffer(const char *buffer)
    {
        xmldoc_.Parse(buffer);
        if(NULL == (xmldoc_.RootElement()))
        {
            return -1;
        }
        return 0;
    }
    
    std::string xml_parser::get_parameter(const char *path, const char *defaultvalue)
    {
        std::string result = get_parameter(path);
        return result.empty() ? defaultvalue : result;
    }
    
    int xml_parser::get_parameter(const char *path, int defaultvalue)
    {
        std::string result = get_parameter(path);
        return result.empty() ? defaultvalue : atoi(result.c_str());
    }
    
    std::string xml_parser::get_parameter(const char *path)
    {
        std::string pathstring(path);
        
        ts::tinyxml2::XMLElement *element = root_;
        if(element == NULL)
        {
            return "";
        }
        
        std::string key;
        std::size_t begin = 0, end = 0;
        while(std::string::npos != (end = pathstring.find("/", begin)))
        {
            key = pathstring.substr(begin, end - begin);
            begin = end + 1;
            if (key.empty()) {
                common::xlog::log(LOG_ALL, "xml_config_parser::%s, key is empty\n", __FUNCTION__);
                return "";
            }
            if (NULL == (element = element->FirstChildElement(key.c_str()))) {
                common::xlog::log(LOG_ALL, "xml_config_parser::%s, no element for key[%s]\n", __FUNCTION__, key.c_str());
                return "";
            }
        }
        
        key = pathstring.substr(begin);
        if(key.empty() || NULL == (element = element->FirstChildElement(key.c_str())))
        {
            return "";
        }
        
        ts::tinyxml2::XMLPrinter printer;
        element->InnerAccept(&printer);
        return printer.CStr();
    }
}   
}
