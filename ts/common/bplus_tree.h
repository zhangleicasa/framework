/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_COMMON_BPLUS_TREE_H_
#define TS_COMMON_BPLUS_TREE_H_

/* 
 *  1. b+ tree must be implemented on top of memory allocator
 *  2. we can define the key which has multiple attributes
 *  3. we must define the value which can be well originzed in allocator
 */ 

#include "allocator.h"
#include "mem_table.h"

//this is a M-btree
#define M 16
#define V_MAX M
#define V_MIN ((M+1)/2)
#define P_MAX V_MAX+1 
#define P_MIN V_MIN

namespace ts {
namespace common {
    typedef int (* compare_func)(const void *, const void *);
    
    struct bplus_linkednode                                                         //[todo] not be used currently.
    {
        const void *value_;
        bplus_linkednode *next_;
    };
    
    struct bplus_node
    {
        int capacity_;                                                              //how many keys in this node.
        bool isleaf_;                                                               //whether is leaf?
        const void *keys_[V_MAX];                                                   //key which 
        bplus_node *points_[P_MAX];                                                 //child nodes' points
        bplus_node *father_;                                                        //father node
        bplus_node *next_;                                                          //brother node, [todo] not be used currently.
    };
    
    class bplus_tree
    {
    public:
        bplus_tree(int max_unit, compare_func compareFunc, mem::allocator *allocator);
        virtual ~bplus_tree();
        
        bool t_insert(const void *object);
        bool t_delete(const void *object);
        bplus_node *t_search(const void *object);
        bplus_node *t_update(const void *object);
        
        bplus_node *get_root();
        
    public:
        template <typename T>
        static void display_tree(bplus_node *node);
    
    private:
        bool insert_into_leaf(bplus_node *node, const void *object);
        bool insert_into_leaf_with_split(bplus_node *node, const void *object);
        bool insert_into_parent(bplus_node *pnode, bplus_node *lnode, bplus_node *rnode, const void *object);
        bool insert_into_node(bplus_node *pnode, bplus_node *lnode, bplus_node *rnode, const void *object);
        bool insert_into_node_with_split(bplus_node *pnode, bplus_node *lnode, bplus_node *rnode, const void *object);
        
        bplus_node *delete_node(bplus_node *node, const void *key, void *value);
        bplus_node *delete_from_node(bplus_node *node, const void *key, void *value);
        bplus_node *coalesce_nodes(bplus_node *node, bplus_node *neighbor_node, int neighbor_offset, const void *object);
        bplus_node *distribute_nodes(bplus_node *node, bplus_node *neighbor_node, int neighbor_offset, int key_offset, const void *object);
    
    private:
        /* 
         * used in the b plus node, which decrease N to logN. 
         * if object can not be found, return the capacity of the node. no_equal_position is the position.
         * if object can be found, return the position.
         */
        inline int binary_search(bplus_node *node, int start, int end, const void *object, int &no_equal_position)
        {
            int position    = 0;
            int left        = start;
            int right       = end-1;
            
            while(left <= right)
            {
                position = (left+right)/2;
                int compare_result = compare_func_(object, node->keys_[position]);
                
                if(compare_result == 0)
                    return position;
                if(compare_result == -1)
                    right = position - 1;
                if(compare_result == 1)
                    left  = position + 1;
            }
            no_equal_position  = left;
            return end;
        }
        
        /** create a new node which is a leaf or a root. **/
        inline bplus_node * create_node(bool isleaf, bool isroot)
        {
            bplus_node *node = NULL;
            if(isroot && isleaf)
                node = (bplus_node *)(mem_table_->get_object_by_id(0));
            else
                node = (bplus_node *)(mem_table_->alloc());
            
            node->capacity_ = 0;
            node->isleaf_   = isleaf;
            node->father_   = NULL;
            for(int i = 0; i < V_MAX; ++i)
                node->keys_[i]   = NULL;
            for(int i = 0; i < V_MAX+1; ++i)
                node->points_[i] = NULL;
            
            if(isroot)
                root_node_  = node;
            
            return node;
        }
        
    private:
        mem::mem_table                          *mem_table_;
        bplus_node                              *root_node_;
        compare_func                             compare_func_;
    
    };
}
}

#include "bplus_tree.hpp"

#endif
