#include "cache_flow.h"

namespace ts {
namespace common {
    cache_flow::cache_flow(unsigned buffer_chain, unsigned buffer_size)
        :buffer_chain_(buffer_chain), buffer_size_(buffer_size)
    {
        for(unsigned i=0; i<buffer_chain_; i++)
        {
            ts::mem::buffer *pbuffer = new ts::mem::buffer(buffer_size_);
            buffer_vector_.push_back(pbuffer);
        }
        current_buffer_    = 0;
        under_flow_        = NULL;
        event_interupter_  = NULL;
    };

    cache_flow::~cache_flow()
    {
        for(unsigned i=0; i<buffer_chain_; i++)
            delete(buffer_vector_[i]);
    };

    bool cache_flow::append(const char *object, int length)
    {
        if(!buffer_vector_[current_buffer_]->enough_space(length))
            ++current_buffer_;
        /** todo: consider that all the buffer in vector is used up. **/
        if(current_buffer_ > buffer_chain_)
            return false;
        const char * address = buffer_vector_[current_buffer_]->append((char *)object, length);
        cache_flow_node node_;
        node_.address_ = address;
        node_.size_    = length;
        flow_node_list_.push_back(node_);
        if(NULL != under_flow_)
            under_flow_->append(object, length);
        if(NULL != event_interupter_)
        	event_interupter_->notify();
        return true;
    }

    int cache_flow::get(int id, void *object, int length)
    {
        memcpy(object, flow_node_list_[id].address_, flow_node_list_[id].size_);
        return flow_node_list_[id].size_;
    }

    bool cache_flow::attach_under_flow(flow *file_flow)
    {
    	if(NULL == under_flow_)
    	{
            under_flow_ = file_flow;
            return true;
    	}
    	return false;//todo: add error handle.
    }

    void cache_flow::attach_interupter(ts::event::event_interupter *handler)
    {//todo: interupter can support many handler.
    	event_interupter_ = handler;
    }
}
}

