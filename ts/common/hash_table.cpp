#include "hash_table.h"

namespace ts {
namespace common {

	void *hash_table_iterator::next()
	{
		void *ret = NULL;
		if(cur_ < free_)
		{
			ret  = cur_;
			cur_ = (char *)cur_ + tuple_size_;
		}
		else if(next_ != NULL)
		{
			ret  = next_;
			cur_ = (char *)ret + tuple_size_;
			free_= *(void **)((char *)next_ + bucket_size_);
			next_= *(void **)((char *)next_ + bucket_size_ + sizeof(void *));
		}
		return ret;
	}

    hash_table::hash_table(unsigned bucket_chain_count, unsigned bucket_size, unsigned tuple_size)
        : bucket_chain_count_(bucket_chain_count), bucket_size_(bucket_size), tuple_size_(tuple_size)
    {
        /** memory_repo_size_ can be used as page size. **/
        bucket_start_address_   = (void **)malloc(sizeof(void *) * bucket_chain_count);
        memory_repo_size_       = bucket_chain_count*get_cacheline_aligned_space(bucket_size);

        /** allocate first space for the hash table. **/
        alloc_space_for_bucket_in_hashtable();

        /** init the hash table, alloc the space for every bucket. **/
        for(unsigned i = 0; i < bucket_chain_count; i++)
        {
            /** make the enough space for the bucket allocate. **/
            if(get_cacheline_aligned_space(bucket_size) > memory_repo_size_ - current_repo_distribute_)
                alloc_space_for_bucket_in_hashtable();

            bucket_start_address_[i] = (void *)(current_repo_start_ + current_repo_distribute_);
            current_repo_distribute_ = current_repo_distribute_ + get_cacheline_aligned_space(bucket_size);

            void **free = (void **)((char *)bucket_start_address_[i] + bucket_size);
            void **next = (void **)((char *)bucket_start_address_[i] + bucket_size + sizeof(void *));
            *free       = bucket_start_address_[i];
            *next       = NULL;
        }
    }

    hash_table::~hash_table()
    {
        unsigned repo_size = memory_repo_list_.size();
        for(unsigned i = 0; i < repo_size; i++)
            free(memory_repo_list_[i]);
        free(bucket_start_address_);
    }

    hash_table_iterator hash_table::create_hash_table_iterator()
    {
    	return hash_table_iterator(bucket_size_, tuple_size_);
    }

    void hash_table::locate_hash_table_iterator(hash_table_iterator &itr, unsigned offset)
    {
    	assert(offset <= bucket_chain_count_);
    	void * buckets = bucket_start_address_[offset];
    	if(NULL == buckets)
    	{
    		itr.cur_  = NULL;
    		itr.free_ = NULL;
    		itr.next_ = NULL;
    	}
    	else
    	{
    		itr.cur_  = buckets;
    		itr.free_ = *(void **)((char *)buckets + bucket_size_);
    		itr.next_ = *(void **)((char *)buckets + bucket_size_ + sizeof(void *));
    	}
    }

    void *hash_table::allocate(unsigned offset)
    {
    	void *ret  = NULL;
    	void *data = bucket_start_address_[offset];
    	assert(NULL != data);
    	if(NULL != data)
    	{
    		void **free = (void **)((char *)data + bucket_size_);
    		if((char *)(*free) + tuple_size_ <=  (char *)data + bucket_size_)
    		{
    			ret  = *free;
    			*free = (char *)(*free) + tuple_size_;
    			return ret;
    		}
    	}

		/** allocate the space for new bucket. **/
    	if(current_repo_distribute_ + bucket_size_ >= memory_repo_size_)
            alloc_space_for_bucket_in_hashtable();
    	char *current_mother_page = memory_repo_list_.back();
		ret = current_mother_page + current_repo_distribute_;
		current_repo_distribute_ += get_cacheline_aligned_space(bucket_size_);

		/** a new bucket into the bucket chain. **/
		void **new_free = (void **)((char *)ret + bucket_size_);
		void **new_next = (void **)((char *)ret + bucket_size_ + sizeof(void *));
		*new_free = (char *)ret + tuple_size_;
		*new_next = data;
		bucket_start_address_[offset] = ret;
		return ret;
    }

    void hash_table::debug()
    {
    	for(unsigned i = 0; i < bucket_chain_count_; ++i)
    	{
    		printf("\nbucket[%d], address[%p]\n%s\n", i, bucket_start_address_[i],
    				GetBinaryDumpInfo((char *)(bucket_start_address_[i]), get_cacheline_aligned_space(bucket_size_)).c_str());
    		void **next = (void **)((char *)bucket_start_address_[i] + sizeof(void *) + bucket_size_);
    		for(; *next != NULL; next = (void **)((char *)(*next) + sizeof(void *) + bucket_size_))
    		{
        		printf("\nbucket[%d], address[%p]\n%s\n", i, bucket_start_address_[i],
        				GetBinaryDumpInfo((char *)(*next), get_cacheline_aligned_space(bucket_size_)).c_str());
    		}
    	}
    }

    void hash_table::debug(uint32_t offset)
    {
		printf("\nbucket[%d], address[%p]\n%s\n", offset, bucket_start_address_[offset],
				GetBinaryDumpInfo((char *)(bucket_start_address_[offset]), get_cacheline_aligned_space(bucket_size_)).c_str());
		void **next = (void **)((char *)bucket_start_address_[offset] + sizeof(void *) + bucket_size_);
		for(; *next != NULL; next = (void **)((char *)(*next) + sizeof(void *) + bucket_size_))
		{
			printf("\nbucket[%d], address[%p]\n%s\n", offset, bucket_start_address_[offset],
					GetBinaryDumpInfo((char *)(*next), get_cacheline_aligned_space(bucket_size_)).c_str());
		}
    }

}
}
