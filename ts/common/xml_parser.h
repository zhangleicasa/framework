/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_COMMON_XML_PARSER_H_
#define TS_COMMON_XML_PARSER_H_

#include "x_log.h"
#include "public.h"
#include "singleton.h"
#include "tinyxml2.h"

namespace ts {
namespace common {
    //XML parser implementation of tinyxml2(https://github.com/leethomason/tinyxml2).
    class xml_parser : public singleton<xml_parser>
    {
        friend class singleton<xml_parser>;
    public:
        xml_parser();
        ~xml_parser();
        
        int parse_file(const char *filename);
        int parse_buffer(const char *buffer);
        
        std::string get_parameter(const char *path, const char *defaultvalue);
        int get_parameter(const char *path, int defaultvalue);
        std::string get_parameter(const char *path);

    private:
        ts::tinyxml2::XMLDocument xmldoc_;
        ts::tinyxml2::XMLElement *root_;
    };
}//common
}//ts

#endif
