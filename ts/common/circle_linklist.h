/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_COMMON_CIRCLE_LINKLIST_H_
#define TS_COMMON_CIRCLE_LINKLIST_H_

namespace ts {
namespace common {
	template <typename TYPE>
	typedef struct linknode
	{
		TYPE value_;
		linknode *next_;
	}linknode;

	template <typename TYPE>
	class circle_linklist
	{
	public:
		circle_linklist();
		~circle_linklist();
	};
}
}

#endif
