#include "mvcc_bptree.h"

namespace ts {
namespace common {
    
    mvcc_bptree::mvcc_bptree(int max_unit, compare_func compareFunc, mem::allocator *allocator)
    {
        mem_table_      = new mem::mem_table(sizeof(mvcc_bplus_node), max_unit, allocator);
        compare_func_   = compareFunc;
        mem_table_->alloc();
        /** create a node which is a leaf either a root. **/
        create_node(true, true);
        time_spend_     = 0.0;
    }

    mvcc_bptree::~mvcc_bptree()
    {
        delete mem_table_;
    }

    mvcc_bplus_node *mvcc_bptree::get_root()
    {
        return root_node_;
    }

    bool mvcc_bptree::t_insert(const void *object)
    {
    	spin_lock_.acquire();
        mvcc_bplus_node *last_node     = NULL;
        mvcc_bplus_node *inserted_node = t_search(object);
        if(inserted_node->capacity_ < V_MAX)
        {
            insert_into_leaf(inserted_node, object);
        }
        else
        {
            last_node = insert_into_leaf_with_split(inserted_node, object);
        }
        /** todo: return true or false? whether to determine. **/
    	spin_lock_.release();
        return true;
    }

    mvcc_bplus_node *mvcc_bptree::t_search(const void *object)
    {
        /** get the root node and lock it [R-LOCK]. **/
        mvcc_bplus_node *temp_object     = get_root();

        if(temp_object->capacity_ != 0)
            while(!temp_object->isleaf_)
            {
                int search_position = 0;
                int position = binary_search(temp_object, 0, temp_object->capacity_, object, search_position);
                /** if no one found, search_position is the position. **/
                if(position == temp_object->capacity_)
                    position = search_position;
                temp_object = temp_object->points_[position];
            }

        return temp_object;
    }

    /** safe node, release the lock of accestors if neccessary. **/
    bool mvcc_bptree::insert_into_leaf(mvcc_bplus_node *node, const void *object)
    {
        int position = 0;
        /** if there are records in the tree. **/
        if(node->capacity_ != 0)
        {
            int insert_position = 0;
            position = binary_search(node, 0, node->capacity_, object, insert_position);
            /** if no one found, search_position is the position. **/
            if(position == node->capacity_)
                position = insert_position;
    
            /** adjust the space of the node. **/
            if(position < node->capacity_)
                /** insert into middle of the node. **/
                for(int j = node->capacity_-1; j >= position; --j)
                    node->keys_[j+1] = node->keys_[j];
        }
    
        /** insert the object in leaf node certainly. **/
        node->keys_[position]   = object;
        node->points_[position] = NULL;
        node->capacity_ ++;

        /** todo: return true or false? whether to determine. **/
        return true;
    }
    
    mvcc_bplus_node * mvcc_bptree::insert_into_leaf_with_split(mvcc_bplus_node *node, const void *object)
    {
        /** confirm the father of the node. **/
        if(node->father_ == NULL)
            node->father_   = create_node(false, true);
    
        /** create a split node which is the leaf node, but not root node. **/
        mvcc_bplus_node *split_node     = create_node(true, false);
        /** set the father of the split node as the node's father. **/
        split_node->father_        = node->father_;
        /** node's brother node as the own to split node. **/
        split_node->points_[V_MAX] = node->points_[V_MAX];
        /** todo: determine whether is 'split_node[0]' **/
        node->points_[V_MAX]       = split_node;
    
        /** find the position to insert the object. **/
        int position = compare_func_(object, node->keys_[V_MIN - 1]) == 1 ? V_MIN : V_MIN - 1;
    
        /** move the part objects of node to split node. **/
        for(int i = position, j = 0; i < V_MAX; ++i, ++j)
        {
            /** move the objects to split node. **/
            split_node->keys_[j] = node->keys_[i];
            split_node->capacity_++;
            /** delete the objects of node. **/
            node->keys_[i]       = NULL;
            node->capacity_--;
        }
    
        /** insert node into the leaf node, left or right. **/
        if(position == V_MIN - 1)
            insert_into_leaf(node, object);
        else
            insert_into_leaf(split_node, object);

        /** insert the left node, right node and middle key into the father node. **/
        return insert_into_parent(node->father_, node, split_node, node->keys_[V_MIN - 1]);
    }
    
    mvcc_bplus_node * mvcc_bptree::insert_into_parent(mvcc_bplus_node *pnode, mvcc_bplus_node *lnode, mvcc_bplus_node *rnode, const void *object)
    {
        mvcc_bplus_node * node = NULL;
        /** insert the object into node if capacity of pnode less than V_MAX **/
        if(pnode->capacity_ < V_MAX)
        {
            node = insert_into_node(pnode, lnode, rnode, object);
        }
        /** insert the object into node with splitting if capacity of pnode more than V_MAX **/
        else
            node = insert_into_node_with_split(pnode, lnode, rnode, object);
        /** todo: return true or false? whether to determine. **/
        return node;
    }
    
    mvcc_bplus_node * mvcc_bptree::insert_into_node(mvcc_bplus_node *pnode, mvcc_bplus_node *lnode, mvcc_bplus_node *rnode, const void *object)
    {
        /** fint the position of father node to insert the object. **/
        int insert_position = 0;
        int position = binary_search(pnode, 0, pnode->capacity_, object, insert_position);
        if(position == pnode->capacity_)
            position = insert_position;
    
        /** move the object of the node. **/
        for(int j = pnode->capacity_-1; j >= position; --j)
        {
            pnode->keys_[j+1]      = pnode->keys_[j];
            pnode->points_[j+2]    = pnode->points_[j+1];
        }
    
        /** insert the object to the father node. **/
        pnode->keys_[position]     = object;
        pnode->points_[position]   = lnode;
        pnode->points_[position+1] = rnode;
        pnode->capacity_++;
    
        /** adjust the father of left and right node. **/
        lnode->father_             = pnode;
        rnode->father_             = pnode;
    
        /** todo: return true or false? whether to determine. **/
        return pnode;
    }
    
    mvcc_bplus_node * mvcc_bptree::insert_into_node_with_split(mvcc_bplus_node *pnode, mvcc_bplus_node *lnode, mvcc_bplus_node *rnode, const void *object)
    {
        /** confirm the father of the node. **/
        if(pnode->father_ == NULL)
            pnode->father_ = create_node(false, true);
    
        /** create a split node which is not leaf node, but not root node. **/
        mvcc_bplus_node * split_node = create_node(false, false);
        /** set the father of the split node as the pnode's father. **/
        split_node->father_     = pnode->father_;
    
        /** find the position to insert the object. **/
        int position = compare_func_(object, pnode->keys_[V_MIN-1]) == 1 ? V_MIN : V_MIN-1;
        
        /** move the part objects of node to split node. **/
        for(int i = position, j=0; i < V_MAX; ++i, ++j)
        {
            split_node->keys_[j]            = pnode->keys_[i];
            split_node->points_[j]          = pnode->points_[i];
            split_node->capacity_ ++;
            pnode->keys_[i]                 = NULL;
            pnode->points_[i]               = NULL;
            pnode->capacity_ --;
        }
        split_node->points_[V_MAX-position] = pnode->points_[V_MAX];
        pnode->points_[V_MAX]               = NULL;
    
        /** adjust the father of the pnode and split nodes' children. **/
        for(int i=0; i < pnode->capacity_; ++i)
            pnode->points_[i]->father_      = pnode;
        for(int i=0; i < split_node->capacity_+1; ++i)
            if(split_node->points_[i] != NULL)
                split_node->points_[i]->father_ = split_node;
    
        /** insert node into the leaf node, left or right. **/
        if(position == V_MIN-1)
        {
            int no_equal_position = 0;
            int inter_position = binary_search(pnode, 0, pnode->capacity_, object, no_equal_position);
            if(inter_position == pnode->capacity_)
                inter_position = no_equal_position;
    
            /** handle with inter nodes situation. **/
            if(inter_position == pnode->capacity_)
            {
                pnode->keys_[inter_position]    = object;
                pnode->points_[inter_position]  = lnode;
                split_node->points_[0]          = rnode;
                pnode->capacity_ ++;
                /** adjust the father of left node and right node. **/
                lnode->father_          = pnode;
                rnode->father_          = split_node;
            }
            else
                /** handle with intra node situation. **/
                insert_into_node(pnode, lnode, rnode, object);
        }
        else
            insert_into_node(split_node, lnode, rnode, object);

        /** insert the left node, right node and middle key into the father node. **/
        return insert_into_parent(pnode->father_, pnode, split_node, pnode->keys_[V_MIN-1]);
    
    }
}
}

