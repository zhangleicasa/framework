/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_COMMON_DEBUG_STACK_H_
#define TS_COMMON_DEBUG_STACK_H_

#include "public.h"

namespace ts {
namespace common {
	typedef struct frame_debug_stack
	{
		const char * func_;
		const char * file_;
		unsigned level_;
		struct frame_debug_stack *next_;
	}frame_debug_stack_;

	static frame_debug_stack_ *frame_debug_stack_node = NULL;

	void _db_enter_(const char *func, const char *file, unsigned line, frame_debug_stack_ *next);
	void _db_return_(const char *func, const char *file, unsigned line, frame_debug_stack_ *next);

	#define DEBUG_ENTER(a) frame_debug_stack_ stack_; \
		_db_enter_(a, __FILE__, __LINE__, &stack_);

	#define DEBUG_RETURN(a) frame_debug_stack_ stack_; \
		_db_return_(a, __FILE__, __LINE__, &stack_);
}
}


#endif /* TS_COMMON_DEBUG_STACK_H_ */
