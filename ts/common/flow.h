/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_COMMON_FLOW_H_
#define TS_COMMON_FLOW_H_

namespace ts {
namespace common {
    class flow
    {
    public:
        virtual ~flow(){}
        /** append a object to the cache flow. **/
        virtual bool append(const char *object, int length) = 0;
        /** get the id-th object from cache flow by certain length. **/
        virtual int  get(int id, void *object, int length) = 0;
    };
}
}

#endif
