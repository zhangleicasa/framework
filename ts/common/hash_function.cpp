#include "hash_function.h"

namespace ts {
namespace common {
    uint32_t hash_function::murmurhash1(const void *key, int len, uint32_t seed)
    {
        const unsigned int m = 0xc6a4a793;
        const int r = 16;
        unsigned int h = seed^(len*m);

        const unsigned char *data = (const unsigned char *)key;
        while(len >= 4)
        {
            unsigned int k = *(unsigned int *)data;
            h += k;
            h *= m;
            h ^= h >> 16;

            data += 4;
            len -= 4;
        }

        switch(len)
        {
        case 3:
            h += data[2] << 16;
        case 2:
            h += data[1] << 8;
        case 1:
            h += data[0];
            h *= m;
            h ^= h >> r;
        };

        h *= m;
        h ^= h >> 10;
        h *= m;
        h ^= h >> 17;

        return h;
    }

    unsigned long hash_function::tshash(const char *key, uint32_t seed)
    {
        unsigned long ret=seed;
        const char *str = key;
        
        if (*str == '\0')
        {
            return(ret);
        }
        long n = 0x100;
        while (*str)
        {
            unsigned long v = n|(*str);
            n += 0x100;
            int r = (int)((v>>2)^v)&0x0f;
            ret = (ret<<r)|(ret>>(32-r));
            ret &= 0xFFFFFFFFL;
            ret ^= v*v;
            str++;
        }
        return ((ret>>16)^ret);
    }
}
}

