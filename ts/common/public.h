/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_COMMON_PUBLIC_H_
#define TS_COMMON_PUBLIC_H_

#include <vector>
#include <queue>
#include <deque>
#include <string>
#include <algorithm>

#include <assert.h>
#include <memory.h>
#include <limits.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <malloc.h>
#include <time.h>
#include <stdarg.h>

#ifdef  LINUX
#define FPOS_SET(position, pos_value) (position.__pos = pos_value)
#define FPOS_GET(position) (position.__pos)
#elif   WIN32
#define FPOS_SET(position, pos_value) (position = pos_value)
#define FPOS_GET(position) (position)
#endif

//platform related system functions:
#ifdef LINUX

#include <sys/time.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <bits/socket.h>
#include <fcntl.h>
#include <errno.h>
#include <errno.h>
#include <unistd.h>
#include <sys/mman.h>
#include <pthread.h>
#include <semaphore.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/mman.h>
#include <unistd.h>
#elif WIN32
#endif

#define _DEFINE_SYS_ARPA_

/** transfer from stream to struct also can use:
 *  __builtin_bswap16(v)
 *  __builtin_bswap32(v)
 *  __builtin_bswap64(v)
 *  example: uint32_t content = __builtin_bswap32(content)
 *  **/

template <typename T>
inline T swap_endian(T u)
{
	union
	{
		T u;
		unsigned char u8[sizeof(T)];
	}source, dest;

	source.u = u;

	for(size_t k = 0; k < sizeof(T); k++)
		dest.u8[k] = source.u8[sizeof(T) - k - 1];

	return dest.u;
}

template <typename T>
inline T swap_endian_p(T u)
{
	union
	{
		T u;
		unsigned char u8[sizeof(T)];
	}source, dest;

	source.u = u;

	switch(sizeof(T))
	{
	case 1:
		{
			dest.u8[0]  = source.u8[0];
			break;
		}
	case 2:
		{
			dest.u8[0]  = source.u8[1];
			dest.u8[1]  = source.u8[0];
			break;
		}
	case 4:
		{
			dest.u8[0] = source.u8[3];
			dest.u8[1] = source.u8[2];
			dest.u8[2] = source.u8[1];
			dest.u8[3] = source.u8[0];
			break;
		}
	case 8:
		{
			dest.u8[0]  = source.u8[7];
			dest.u8[1]  = source.u8[6];
			dest.u8[2]  = source.u8[5];
			dest.u8[3]  = source.u8[4];
			dest.u8[4]  = source.u8[3];
			dest.u8[5]  = source.u8[2];
			dest.u8[6]  = source.u8[1];
			dest.u8[7]  = source.u8[0];
			break;
		}
	}

	return dest.u;
}

//expression macro
#define DATA_TYPE_SERIES 32
#define OPERATOR_TYPE_SERIES 32
#define CACHE_LINE_SIZE 64

#endif
