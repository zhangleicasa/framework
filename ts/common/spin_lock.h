/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_SYS_SPIN_LOCK_H_
#define TS_SYS_SPIN_LOCK_H_

namespace ts {
namespace sys {
    class spin_lock
    {
    public:
        spin_lock() : _l(0) { }

        inline void acquire()
        {
            while(tas(&_l)) {
#if defined(__i386__) || defined(__x86_64__)
            __asm__ __volatile__ ("pause\n");
#endif
            }
        }

        inline bool try_lock()
        {
            return !tas(&_l);
        }

        inline int fakelock()
        {
            return _l==0?0:1;
        }

        inline int getvalue()
        {
            return 0;
        }

        inline void release()
        {
            _l = 0;
        }

    private:
        inline int tas(volatile char* lock)
        {
            register char res = 1;
#if defined(__i386__) || defined(__x86_64__)
            __asm__ __volatile__ (
                "lock xchgb %0, %1\n"
                : "+q"(res), "+m"(*lock)
                :
                : "memory", "cc");
#elif defined(__sparc__)
            __asm__ __volatile__ (
                "ldstub [%2], %0"
                : "=r"(res), "+m"(*acquire)
                : "r"(acquire)
                : "memory");
#else

#endif
            return res;
        }

        volatile char _l ;
    };
}
}

#endif
