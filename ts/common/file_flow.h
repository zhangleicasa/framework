/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_COMMON_FILE_FLOW_H_
#define TS_COMMON_FILE_FLOW_H_

#include "flow.h"
#include "x_log.h"
#include "public.h"

namespace ts {
namespace common {
    class file_flow : public flow
    {
    public:
        file_flow(const char * file_name);
        virtual bool append(const char *object, int length);
        virtual int  get(int id, void *object, int length);
    private:
        FILE    *content_file_;
        FILE    *index_file_;
        fpos_t   position_;
        size_t   file_flow_iterms_;
    };
}
}

#endif
