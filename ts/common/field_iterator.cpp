#include "field_iterator.h"

namespace ts {
namespace common {
	field_iterator::field_iterator(char *curr, char *end)
	{
		data_ = NULL;
		curr_ = curr;
		end_  = end;
	}

	void field_iterator::next()
	{
		data_ = NULL;
		while(data_ == NULL)
		{
			get_header();
			curr_ += sizeof(field_header);
		}
	}

	void field_iterator::get_header()
	{
		memcpy(&header_, curr_, sizeof(field_header));
	}

	void field_iterator::retreive(ts::db::field_base *field)
	{

	}
}
}

