/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_COMMON_EVENT_QUEUE_H_
#define TS_COMMON_EVENT_QUEUE_H_

#include "public.h"
#include "synchronize.h"
#include "event_handler.h"

namespace ts {
namespace common {
    typedef struct event
    {
        ts::event::event_handler *handler_;
        int eventid_;
        int nparam_;
        void *params_;
    }event;
    
    typedef struct sync_event_node
    {
        ts::sys::mutex mutex_;
        event event_;
        sync_event_node *next_;
    }event_node;
    
    class event_queue
    {
    public:
        event_queue(int queue_size);
        virtual ~event_queue();
        
        //if add the event into queue successfully, return true;
        //   unless, return false;
        bool add_to_queue(ts::event::event_handler *handler, int event_id, int nparam, void *params);
        
        bool pop_head_event(event &evt);
    
    private:
        event *post_events_;
        
        int queue_size_;
        int head_node_;
        int tail_node_;
        
        sync_event_node *first_event_;
        sync_event_node *last_event_;
        
        ts::sys::mutex mutex_;
    };

}
}

#endif
