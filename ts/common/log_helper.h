/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_COMMON_LOG_HELPER_H_
#define TS_COMMON_LOG_HELPER_H_

#include "public.h"

static inline char GetVisibleAscii(char c) { return (c >= 33 && c <= 126) ? c : '.'; }

static std::string GetBinaryDumpInfo(const char *buf, int len, int indent = 4) {
    if (len <= 0) {
        return "";
    }
    std::string info;
    std::string strindent(indent, ' ');

    int line = len >> 4;
    int last = (len & 0xF);
    int i = 0;
    for (i = 0; i < line; ++i) {
        char szbuf[256] = {0};
        const unsigned char * base = (const unsigned char *)(buf + (i << 4));
        sprintf(szbuf,"%s[0X%04X]  %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x   %c%c %c%c %c%c %c%c %c%c %c%c %c%c %c%c\n",
            strindent.c_str(), i * 16, *(base),*(base+1),*(base+2),*(base+3),*(base+4),*(base+5),*(base+6),*(base+7),
            *(base + 8),*(base+9),*(base+10),*(base+11),*(base+12),*(base+13),*(base+14),*(base+15),
            GetVisibleAscii(*(base)),    GetVisibleAscii(*(base+1)),  GetVisibleAscii(*(base+2)),  GetVisibleAscii(*(base+3)),
            GetVisibleAscii(*(base+4)),  GetVisibleAscii(*(base+5)),  GetVisibleAscii(*(base+6)),  GetVisibleAscii(*(base+7)),
            GetVisibleAscii(*(base+8)),  GetVisibleAscii(*(base+9)),  GetVisibleAscii(*(base+10)), GetVisibleAscii(*(base+11)),
            GetVisibleAscii(*(base+12)), GetVisibleAscii(*(base+13)), GetVisibleAscii(*(base+14)), GetVisibleAscii(*(base+15)));
        info.append(szbuf);
    }

    if (last > 0) {
        const unsigned char * base = (const unsigned char *)(buf + (i << 4));
        char szhex[64] = {0};
        char szascii[64] = {0};
        int num = last >> 1; // last / 2 /
        int j = 0;
        for(; j < num; ++j) {
            sprintf(szhex + j * 5, " %02x%02x", *(base + 2*j), *(base + 2*j + 1));
            sprintf(szascii + j * 3, " %c%c", GetVisibleAscii(*(base + 2*j)), GetVisibleAscii(*(base + 2*j + 1)));
        }
        if ((last & 0X01) == 1) {
            sprintf(szhex + j * 5, " %02x", *(base + last-1));
            sprintf(szascii + j * 3, " %c", GetVisibleAscii(*(base + last-1)));
        }
        char szbuf[128] = {0};
        sprintf(szbuf, "%s[0X%04X] %-40s  %s", strindent.c_str(), i*16, szhex, szascii);
        info.append(szbuf);
        info.append(1,'\n');
    }
    return info;
}

#endif
