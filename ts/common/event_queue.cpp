#include "event_queue.h"

namespace ts {
namespace common {
    event_queue::event_queue(int queue_size) : queue_size_(queue_size)
    {
        post_events_ = new event[queue_size];
        head_node_   = 0;
        tail_node_   = 0;
        first_event_ = NULL;
        last_event_  = NULL;
    }
    
    event_queue::~event_queue()
    {
    
    }
    
    bool event_queue::add_to_queue(ts::event::event_handler *handler, int eventid, int nparam, void *params)
    {
        mutex_.acquire();
        if((tail_node_ + 1) % queue_size_ == head_node_)
        {
        	mutex_.release();
        	return false;//add failed.
        }
        
        event *evt    = post_events_ + tail_node_;
        evt->handler_ = handler;
        evt->eventid_ = eventid;
        evt->nparam_  = nparam;
        evt->params_  = params; 
        
        tail_node_++;
        if(tail_node_ >= queue_size_)
        	tail_node_ = 0;
        
        mutex_.release();
        return true;
    }
    
    bool event_queue::pop_head_event(event &evt)
    {
        mutex_.acquire();
        if(head_node_ != tail_node_)
        {
            evt = post_events_[head_node_];
            
            head_node_ ++;
            if(head_node_ >= queue_size_)
                head_node_ = 0;
            mutex_.release();
            return true;
        }
        
        mutex_.release();
        return false;
    }

}
}
