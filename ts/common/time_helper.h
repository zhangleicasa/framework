/* 
* author: zhanglei
*  email: zhangleicasa@gmail.com
*   note: this file is only used on Linux platform.
*/
#ifndef TS_COMMON_TIME_HELPER_H_
#define TS_COMMON_TIME_HELPER_H_

#include "public.h"

#ifndef WIN32	

static timespec diff(timespec start, timespec end)
{
    timespec temp;
    if ((end.tv_nsec - start.tv_nsec) < 0) 
    {
        temp.tv_sec  = end.tv_sec - start.tv_sec - 1;
        temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
    }
    else
    {
        temp.tv_sec  = end.tv_sec - start.tv_sec;
        temp.tv_nsec = end.tv_nsec - start.tv_nsec;
    }
    return temp;  
}

#define CPU_FREQUENCY 3500000000

typedef unsigned long long int ticks;

#ifdef __cplusplus
extern "C" {
#endif

#if !defined(__i386__) && !defined(__x86_64__) && !defined(__sparc__)
#pragma warning(No supported architecture found -- timers will return junk.)
#endif
  
    static inline unsigned long long curtick() {
        unsigned long long tick = 0;
#if defined(__i386__)
        unsigned long lo, hi;
        __asm__ __volatile__ (".byte 0x0f, 0x31" : "=a" (lo), "=d" (hi));
        tick = (unsigned long long) hi << 32 | lo;
#elif defined(__x86_64__)
        unsigned long lo, hi;
        __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
        tick = (unsigned long long) hi << 32 | lo;
#elif defined(__sparc__)
        __asm__ __volatile__ ("rd %%tick, %0" : "=r" (tick));
#endif
        return tick;
    }

    static inline void startTimer(unsigned long long* t) 
    {
            *t = curtick();
    }

    static inline void stopTimer(unsigned long long* t) 
    {
            *t = curtick() - *t;
    }

    static inline double getSecond(unsigned long long start_time)
    {
            return (curtick()-start_time)/(double)CPU_FREQUENCY;
    }

    static inline double getMilliSecond(unsigned long long start_time)
    {
            return (curtick()-start_time)/(double)CPU_FREQUENCY*1000;
    }

    static inline double getMicroSecond(unsigned long long start_time)
    {
            return (curtick()-start_time)/(double)CPU_FREQUENCY*1000000;
    }

    static inline double getNanoSecond(unsigned long long start_time)
    {
            return (curtick()-start_time)/(double)CPU_FREQUENCY*1000000000;
    }

    static inline double getSecondDuratoin(unsigned long long start_time, ticks end)
    {
            return (end-start_time)/(double)CPU_FREQUENCY;
    }

    static inline double convertCyclesToSecond(ticks tick)
    {
            return tick/(double)CPU_FREQUENCY;
    }

#ifdef __cplusplus
}
#endif

#endif
#endif
