#include "bplus_tree.h"

namespace ts {
namespace common {

    bplus_tree::bplus_tree(int max_unit, compare_func compareFunc, mem::allocator *allocator)
    {
        mem_table_      = new mem::mem_table(sizeof(bplus_node), max_unit, allocator);
        compare_func_   = compareFunc;
        mem_table_->alloc();
        /** create a node which is a leaf either a root. **/
        create_node(true, true);
    }

    bplus_tree::~bplus_tree()
    {
        delete mem_table_;
    }

    bool bplus_tree::t_insert(const void *object)
    {
        bplus_node *inserted_node = t_search(object);
        if(inserted_node->capacity_ < V_MAX)
            insert_into_leaf(inserted_node, object);
        else
            insert_into_leaf_with_split(inserted_node, object);
        /** todo: return true or false? whether to determine. **/
        return true;
    }
    
    bool bplus_tree::t_delete(const void *object)
    {
        void *value = NULL;
        bplus_node *leaf_node = t_search(object);
        /** if we can not find the object in b plus tree. **/
        if(leaf_node == NULL)
            return false;
        /** find the position of the object. **/
        int delete_position = 0;
        int position = binary_search(leaf_node, 0, leaf_node->capacity_, object, delete_position);
        if(position == leaf_node->capacity_)
            return false;
        else
            value = leaf_node->points_[position];
        /** delete the object from the leaf node. **/
        delete_node(leaf_node, object, value);
        return true;
    }

    bplus_node *bplus_tree::t_search(const void *object)
    {
        bplus_node *temp_object     = get_root();

        if(temp_object->capacity_ != 0)
            while(!temp_object->isleaf_)
            {
                int search_position = 0;
                int position = binary_search(temp_object, 0, temp_object->capacity_, object, search_position);
                /** if no one found, search_position is the position. **/
                if(position == temp_object->capacity_)
                    position = search_position;
                /** find the next level. **/
                temp_object = temp_object->points_[position];
            }

        return temp_object;
    }

    bplus_node *bplus_tree::t_update(const void *object)
    {
        return NULL;
    }
    
    bplus_node *bplus_tree::get_root()
    {
        return root_node_;
    }
    
    bool bplus_tree::insert_into_leaf(bplus_node *node, const void *object)
    {
        int position = 0;
        /** if there are records in the tree. **/
        if(node->capacity_ != 0)
        {
            int insert_position = 0;
            position = binary_search(node, 0, node->capacity_, object, insert_position);
            /** if no one found, search_position is the position. **/
            if(position == node->capacity_)
                position = insert_position;
    
            /** adjust the space of the node. **/
            if(position < node->capacity_)
                /** insert into middle of the node. **/
                for(int j = node->capacity_-1; j >= position; --j)
                    node->keys_[j+1] = node->keys_[j];
        }
    
        /** insert the object in leaf node certainly. **/
        node->keys_[position]   = object;
        node->points_[position] = NULL;
        node->capacity_ ++;
    
        /** todo: return true or false? whether to determine. **/
        return true;
    }
    
    bool bplus_tree::insert_into_leaf_with_split(bplus_node *node, const void *object)
    {
        /** confirm the father of the node. **/
        if(node->father_ == NULL)
            node->father_   = create_node(false, true);
    
        /** create a split node which is the leaf node, but not root node. **/
        bplus_node *split_node     = create_node(true, false);
        /** set the father of the split node as the node's father. **/
        split_node->father_        = node->father_;
        /** node's brother node as the own to split node. **/
        split_node->points_[V_MAX] = node->points_[V_MAX];
        /** todo: determine whether is 'split_node[0]' **/
        node->points_[V_MAX]       = split_node;
    
        /** find the position to insert the object. **/
        int position = compare_func_(object, node->keys_[V_MIN - 1]) == 1 ? V_MIN : V_MIN - 1;
    
        /** move the part objects of node to split node. **/
        for(int i = position, j = 0; i < V_MAX; ++i, ++j)
        {
            /** move the objects to split node. **/
            split_node->keys_[j] = node->keys_[i];
            split_node->capacity_++;
            /** delete the objects of node. **/
            node->keys_[i]       = NULL;
            node->capacity_--;
        }
    
        /** insert node into the leaf node, left or right. **/
        if(position == V_MIN - 1)
            insert_into_leaf(node, object);
        else
            insert_into_leaf(split_node, object);
    
        /** insert the left node, right node and middle key into the father node. **/
        insert_into_parent(node->father_, node, split_node, node->keys_[V_MIN - 1]);
    
        /** todo: return true or false? whether to determine. **/
        return true;
    }
    
    bool bplus_tree::insert_into_parent(bplus_node *pnode, bplus_node *lnode, bplus_node *rnode, const void *object)
    {
        /** insert the object into node if capacity of pnode less than V_MAX **/
        if(pnode->capacity_ < V_MAX)
            insert_into_node(pnode, lnode, rnode, object);
        /** insert the object into node with splitting if capacity of pnode more than V_MAX **/
        else
            insert_into_node_with_split(pnode, lnode, rnode, object);
        /** todo: return true or false? whether to determine. **/
        return true;
    }
    
    bool bplus_tree::insert_into_node(bplus_node *pnode, bplus_node *lnode, bplus_node *rnode, const void *object)
    {
        /** fint the position of father node to insert the object. **/
        int insert_position = 0;
        int position = binary_search(pnode, 0, pnode->capacity_, object, insert_position);
        if(position == pnode->capacity_)
            position = insert_position;
    
        /** move the object of the node. **/
        for(int j = pnode->capacity_-1; j >= position; --j)
        {
            pnode->keys_[j+1]      = pnode->keys_[j];
            pnode->points_[j+2]    = pnode->points_[j+1];
        }
    
        /** insert the object to the father node. **/
        pnode->keys_[position]     = object;
        pnode->points_[position]   = lnode;
        pnode->points_[position+1] = rnode;
        pnode->capacity_++;
    
        /** adjust the father of left and right node. **/
        lnode->father_             = pnode;
        rnode->father_             = pnode;
    
        /** todo: return true or false? whether to determine. **/
        return true;
    }
    
    bool bplus_tree::insert_into_node_with_split(bplus_node *pnode, bplus_node *lnode, bplus_node *rnode, const void *object)
    {
        /** confirm the father of the node. **/
        if(pnode->father_ == NULL)
            pnode->father_ = create_node(false, true);
    
        /** create a split node which is not leaf node, but not root node. **/
        bplus_node * split_node = create_node(false, false);
        /** set the father of the split node as the pnode's father. **/
        split_node->father_     = pnode->father_;
    
        /** find the position to insert the object. **/
        int position = compare_func_(object, pnode->keys_[V_MIN-1]) == 1 ? V_MIN : V_MIN-1;
        
        /** move the part objects of node to split node. **/
        for(int i = position, j=0; i < V_MAX; ++i, ++j)
        {
            split_node->keys_[j]            = pnode->keys_[i];
            split_node->points_[j]          = pnode->points_[i];
            split_node->capacity_ ++;
            pnode->keys_[i]                 = NULL;
            pnode->points_[i]               = NULL;
            pnode->capacity_ --;
        }
        split_node->points_[V_MAX-position] = pnode->points_[V_MAX];
        pnode->points_[V_MAX]               = NULL;
    
        /** adjust the father of the pnode and split nodes' children. **/
        for(int i=0; i < pnode->capacity_; ++i)
            pnode->points_[i]->father_      = pnode;
        for(int i=0; i < split_node->capacity_+1; ++i)
            if(split_node->points_[i] != NULL)
                split_node->points_[i]->father_ = split_node;
    
        /** insert node into the leaf node, left or right. **/
        if(position == V_MIN-1)
        {
            int no_equal_position = 0;
            int inter_position = binary_search(pnode, 0, pnode->capacity_, object, no_equal_position);
            if(inter_position == pnode->capacity_)
                inter_position = no_equal_position;
    
            /** handle with inter nodes situation. **/
            if(inter_position == pnode->capacity_)
            {
                pnode->keys_[inter_position]    = object;
                pnode->points_[inter_position]  = lnode;
                split_node->points_[0]          = rnode;
                pnode->capacity_ ++;
                /** adjust the father of left node and right node. **/
                lnode->father_          = pnode;
                rnode->father_          = split_node;
            }
            else
                /** handle with intra node situation. **/
                insert_into_node(pnode, lnode, rnode, object);
        }
        else
            insert_into_node(split_node, lnode, rnode, object);
    
        /** insert the left node, right node and middle key into the father node. **/
        insert_into_parent(pnode->father_, pnode, split_node, pnode->keys_[V_MIN-1]);
    
        /** todo: return true or false? whether to determine. **/
        return true;
    }
    
    bplus_node *bplus_tree::delete_node(bplus_node *node, const void *key, void *value)
    {
        /** delete the key from the node firstly. **/
        delete_from_node(node, key, value);
        
        /** if the node is the root node. **/
        if(node == root_node_)
        {
            /** adjust the root node of tree. **/
            if(node->capacity_ > 0)
                return node;
            /** the node is the root node and node is a leaf node. **/
            if(node->isleaf_)
                return NULL;
            else
            {
                root_node_          = node->points_[0];
                root_node_->father_ = NULL;
            }
            return root_node_;
        }
    
        /** delete the key of the node, return. **/
        if(node->capacity_ >= V_MIN)
            return root_node_;
        
        /** todo: node can be found in father_->points array? **/
        int neigh_index = 0;
        for(; neigh_index <= node->father_->capacity_; ++neigh_index)
            if(node->father_->points_[neigh_index] == node)
                break;
        
        /** find the key position of the father node. **/
        int key_position_in_father = neigh_index-1 == -1 ? 0 : neigh_index-1;
        /** find the value of reference key in father node. **/
        const void *key_in_father = node->father_->keys_[key_position_in_father];
        /** always find the left node as the neighbor node, unless left most node. **/
        bplus_node *neigh_node = neigh_index-1 == -1 ? node->father_->points_[1] : node->father_->points_[neigh_index-1];
        
        if(neigh_node->capacity_ + node->capacity_ <= V_MAX)
            return coalesce_nodes(node, neigh_node, neigh_index - 1, key_in_father);
        else
            return distribute_nodes(node, neigh_node, neigh_index - 1, key_position_in_father, key_in_father);
    
    }
    
    bplus_node *bplus_tree::delete_from_node(bplus_node *node, const void *key, void *value)
    {
        /** find the position of key which will be deleted, not the address.**/
        int delete_position = 0;
        int key_position    = binary_search(node, 0, node->capacity_, key, delete_position);
        
        /** move the objects of the node. **/
        for(++key_position; key_position < node->capacity_; ++key_position)
            node->keys_[key_position-1] = node->keys_[key_position];

        int point_position = 0;
        if(!node->isleaf_)
        {
            /** find the position of value which will be deleted. **/
            for(; point_position <= node->capacity_; ++point_position)
                /** todo: by using binary search to reduce the complexity. **/
                if(node->points_[point_position] == value)
                    break;
            /** move the points of the node. **/
            for(++point_position; point_position < P_MAX; ++point_position)
                node->points_[point_position-1] = node->points_[point_position];
        }
        
        /** decrease the capacity of the node. **/
        node->capacity_--;

        if(key_position == node->capacity_-1)
        {
            int node_position_in_father = 0;
            /** find the position of pointer in its father. **/
            for(; node_position_in_father < node->father_->capacity_; ++node_position_in_father )
                if(node->father_->points_[node_position_in_father] == node)
                    break;
            node->father_->keys_[node_position_in_father] = node->keys_[node->capacity_-1];
        }   
        
        /** clean the points as NULL in the node's points. **/
        if(node->isleaf_)
            for(point_position = node->capacity_; point_position < V_MAX-1; ++point_position)
                node->points_[point_position] = NULL;
        else
            for(point_position = node->capacity_+1; point_position < V_MAX; ++point_position)
                node->points_[point_position] = NULL;
    
        return node;
    }
    
    bplus_node *bplus_tree::coalesce_nodes(bplus_node *node, bplus_node *neighbor_node, int neighbor_offset, const void *object)
    {
        /** in this function. whatever, we reserve the left node, and delete the right node. **/
        bplus_node *temp_node = 0;
        /** if the neighbor offset equals '-1', swap node and neighbor_node. **/
        if(neighbor_offset == -1)
        {
            temp_node       = node;
            node            = neighbor_node;
            neighbor_node   = temp_node;
            neighbor_offset = 0;
        }
        /** next always merge the left node and right neighbor_node. **/
        
        int neighbor_insert_index = 0, i = 0, j =0;
        neighbor_insert_index = neighbor_node->capacity_;
        if(!node->isleaf_)
        {
            int node_key_number = node->capacity_;
            for(i = neighbor_insert_index, j = 0; j < node_key_number; i++, j++)
            {
                neighbor_node->keys_[i]     = node->keys_[j];
                neighbor_node->points_[i]   = node->points_[j];
                neighbor_node->capacity_++;
                node->capacity_--;
            }
            neighbor_node->points_[i]   = node->points_[j];
            for(i = 0; i < neighbor_node->capacity_+1; ++i)
                if(neighbor_node->points_[i] != NULL)
                {
                    temp_node           = (bplus_node *)neighbor_node->points_[i];
                    temp_node->father_  = neighbor_node;
                }
        }
        else
        {
            /** coalesce the node and neighbor node to the neighbor node. **/
            for(i = neighbor_insert_index, j = 0; j < node->capacity_; i++, j++)
            {
                neighbor_node->keys_[i]       = node->keys_[j];
                neighbor_node->points_[i]     = node->points_[j];
                neighbor_node->capacity_++;
            }
            neighbor_node->points_[V_MAX - 1] = node->points_[V_MAX - 1];
            neighbor_node->points_[V_MAX]     = node->points_[V_MAX];
            //if(neighbor_offset == neighbor_node->father_->capacity_-2)

            /** last node of every level excepted leaf level is special, move only one node and adjust the father. **/
            
            //if(neighbor_offset+1 < neighbor_node->father_->capacity_)
                //neighbor_node->father_->keys_[neighbor_offset+1] = neighbor_node->keys_[neighbor_node->capacity_-1];
            //common::xlog::log(LOG_ALL, "neighbor_offset: %d, neighbor_node->capacity_[%d], neighbor_node->father_->keys_[neighbor_offset+1][%s], "
            //   " neighbor_node->keys_[neighbor_node->capacity_-1][%s]", neighbor_offset, neighbor_node->capacity_,  neighbor_node->father_->keys_[neighbor_offset+1],neighbor_node->keys_[neighbor_node->capacity_-1]);
            //for(unsigned i = 0; i < V_MAX; i++)
                //common::xlog::log(LOG_ALL, "debug: ================%s\n", neighbor_node->father_->keys_[i]);


        }
        
        /** delete key: object and value: node. **/
        root_node_ = delete_node(node->father_, object, node);
        
        return root_node_;
    }
    
    bplus_node *bplus_tree::distribute_nodes(bplus_node *node, bplus_node *neighbor_node, int neighbor_offset, int key_offset, const void *object)
    {
        /** here, key_offset is the left one between node and neighbor node, neighbor_offset is the position of node minus 1. **/
        bplus_node *temp_node = 0;
        int i = 0;
    
        /** if node as the right one, and neighbor node as the left onde. **/
        if(neighbor_offset != -1)
        {
            /** reserve a place for other node, last node of every level excepted leaf level is specially.**/
            if(!node->isleaf_)
                node->points_[node->capacity_+1] = node->points_[node->capacity_];

            /** move the remaining objects of the node. **/
            for(i = node->capacity_; i > 0; --i)
            {
                node->keys_[i]   = node->keys_[i-1];
                node->points_[i] = node->points_[i-1];
            }
        
            if(!node->isleaf_)
            {
                /** last node of every level excepted leaf level is special, move only one node and adjust the father. **/
                node->points_[0]                 = neighbor_node->points_[neighbor_node->capacity_-1];
                temp_node                        = (bplus_node *)node->points_[0];
                temp_node->father_               = node;
               
                /** todo: set the key of the neighbor node as NULL. **/
                neighbor_node->points_[neighbor_node->capacity_-1] = NULL;
                node->keys_[0]                   = object;
                node->father_->keys_[key_offset] = neighbor_node->keys_[neighbor_node->capacity_-2];
                //[bug fix] decrease the capacity of the neighbor node.
            }
            else
            {
                /** adjust last node of leaf level. **/
                node->points_[0]                 = neighbor_node->points_[neighbor_node->capacity_-1];
                neighbor_node->points_[neighbor_node->capacity_ - 1] = NULL;
                node->keys_[0]                   = neighbor_node->keys_[neighbor_node->capacity_-1];
                node->father_->keys_[key_offset] = neighbor_node->keys_[neighbor_node->capacity_-2];
            }
        }
        /** if node as the left one, neighbor node as the right one. **/
        else
        {
            /** if node is the leaf node, move one key from neighbor node to node. **/
            if(node->isleaf_)
            {
                node->keys_[node->capacity_]        = neighbor_node->keys_[0];
                node->father_->keys_[key_offset]    = node->keys_[node->capacity_];
            }
            else
            {
                node->keys_[node->capacity_]        = neighbor_node->keys_[0];
                node->points_[node->capacity_]      = neighbor_node->points_[0];
                /** if node is middle node, adjust the father of the node's children. **/
                temp_node                           = (bplus_node *)node->points_[node->capacity_];
                temp_node->father_                  = node;
                node->father_->keys_[key_offset]    = node->keys_[node->capacity_];
            }

            for(i=0; i < neighbor_node->capacity_-1; ++i)
            {
                neighbor_node->keys_[i]     = neighbor_node->keys_[i+1];
                neighbor_node->points_[i]   = neighbor_node->points_[i+1];
            }
            /** last node of every level expected leaf level is speacial. **/
            if(!node->isleaf_)
                neighbor_node->points_[i]   = neighbor_node->points_[i+1];
        }

        /** move one node every time when distribute_nodes function invoked. **/
        node->capacity_++;
        neighbor_node->capacity_--;

        return root_node_;
    }
}
}
