/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 *   note: mvcc b plus tree, which only has insertion operation.
 */
#ifndef TS_COMMON_MVCC_BPTREE_H_
#define TS_COMMON_MVCC_BPTREE_H_

/* 
 *  1. b+ tree must be implemented on top of memory allocator
 *  2. we can define the key which has multiple attributes
 *  3. we must define the value which can be well organized in allocator
 *  4. deletion of b plus tree is not necessary.
 */ 

#include "allocator.h"
#include "mem_table.h"
#include "spin_lock.h"
#include "synchronize.h"
#include "time_helper.h"

//this is a M-btree
#define M 16
#define V_MAX M
#define V_MIN ((M+1)/2)
#define P_MAX V_MAX+1 
#define P_MIN V_MIN

namespace ts {
namespace common {
    typedef int (* compare_func)(const void *, const void *);
    
    struct mvcc_bplus_node
    {
        int capacity_;                                                              //how many keys in this node.
        bool isleaf_;                                                               //whether is leaf?
        const void *keys_[V_MAX];                                                   //key which 
        mvcc_bplus_node *points_[P_MAX];                                            //child nodes' points
        mvcc_bplus_node *father_;                                                   //father node
        mvcc_bplus_node *next_;                                                     //brother node, [todo] not be used currently.
    };
    
    class mvcc_bptree
    {
    public:
        mvcc_bptree(int max_unit, compare_func compareFunc, mem::allocator *allocator);
        virtual ~mvcc_bptree();
        
        bool t_insert(const void *object);
        bool t_delete(const void *object);
        mvcc_bplus_node *t_search(const void *object);
        mvcc_bplus_node *t_update(const void *object);
        
        mvcc_bplus_node *get_root();

        void display_time()
        {
            common::xlog::log(LOG_ALL, "t2: %f\n", time_spend_);
        }
    
    private:
        bool insert_into_leaf(mvcc_bplus_node *node, const void *object);
        
        mvcc_bplus_node* insert_into_leaf_with_split(mvcc_bplus_node *node, const void *object);
        mvcc_bplus_node* insert_into_parent(mvcc_bplus_node *pnode, mvcc_bplus_node *lnode, mvcc_bplus_node *rnode, const void *object);
        mvcc_bplus_node* insert_into_node(mvcc_bplus_node *pnode, mvcc_bplus_node *lnode, mvcc_bplus_node *rnode, const void *object);
        mvcc_bplus_node* insert_into_node_with_split(mvcc_bplus_node *pnode, mvcc_bplus_node *lnode, mvcc_bplus_node *rnode, const void *object);
    
    private:
        /* 
         * used in the b plus node, which decrease N to logN. 
         * if object can not be found, return the capacity of the node. no_equal_position is the position.
         * if object can be found, return the position.
         */
        inline int binary_search(mvcc_bplus_node *node, int start, int end, const void *object, int &no_equal_position)
        {
            int position    = 0;
            int left        = start;
            int right       = end-1;
            
            while(left <= right)
            {
                position = (left+right)/2;
                int compare_result = compare_func_(object, node->keys_[position]);
                
                if(compare_result == 0)
                    return position;
                if(compare_result == -1)
                    right = position - 1;
                if(compare_result == 1)
                    left  = position + 1;
            }
            no_equal_position  = left;
            return end;
        }
        
        /** create a new node which is a leaf or a root. **/
        inline mvcc_bplus_node * create_node(bool isleaf, bool isroot)
        {
            mvcc_bplus_node *node = NULL;
            if(isroot && isleaf)
                node = (mvcc_bplus_node *)(mem_table_->get_object_by_id(0));
            else
                node = (mvcc_bplus_node *)(mem_table_->alloc());
            
            node->capacity_ = 0;
            node->isleaf_   = isleaf;
            node->father_   = NULL;
            for(int i = 0; i < V_MAX; ++i)
                node->keys_[i]   = NULL;
            for(int i = 0; i < V_MAX+1; ++i)
                node->points_[i] = NULL;
            
            if(isroot)
            {
                root_node_  = node;
            }
            
            return node;
        }
        
    private:
        mem::mem_table                          *mem_table_;
        mvcc_bplus_node                              *root_node_;
        compare_func                             compare_func_;
        sys::spin_lock                           spin_lock_;
        double                                   time_spend_;

    };
}
}

#endif
