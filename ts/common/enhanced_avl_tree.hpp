/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 *   note: this file function can display whole tree by pass a template.
 */
#ifndef TS_COMMON_ENHANCED_AVL_TREE_HPP_
#define TS_COMMON_ENHANCED_AVL_TREE_HPP_

#include <queue>

namespace ts {
namespace common {

	template<typename K, typename V, typename Compare, typename Alloc>
	void
	enhanced_avl_tree<K, V, Compare, Alloc>::deep_traverse(_avl_node_type node)
	{
		int count = 0;
		int count1 = 0;
		std::queue<_avl_base_node_type> que;
		que.push(node);
		count++;
		while(true)
		{
			while(count-- != 0)
			{
				_avl_base_node_type n = que.front();
				que.pop();
				common::xlog::log(LOG_ALL, " %d ", (static_cast<_avl_node_type>(n))->value_field_);
				if(n->left_ != NULL)
				{
					que.push(n->left_);
					count1 ++;
				}
				if(n->right_!= NULL)
				{
					que.push(n->right_);
					count1 ++;
				}
			}
			common::xlog::log(LOG_ALL, "\n");
			if(count1 != 0)
			{
				count = count1;
				count1= 0;
			}
			else
				break;
		}
	}

}
}

#endif

