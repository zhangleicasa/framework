/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_COMMON_SMART_PTR_H_
#define TS_COMMON_SMART_PTR_H_
#include "x_log.h"

namespace ts {
namespace common {
	template<typename OBJ>
	class smart_ptr;

	template<typename OBJ>
	class u_ptr
	{
	private:
		friend class smart_ptr<OBJ>;
		u_ptr(OBJ *p) :p_(p), count_(1) {}
		~u_ptr() { delete p_; }
		OBJ *p_;
		int count_;
	};

	template<typename OBJ>
	class smart_ptr
	{
	public:
		smart_ptr(OBJ *ptr): ptr_(new u_ptr<OBJ>(ptr)) {
			common::xlog::log(common::LOG_ALL, "%d pointers left\n", ptr_->count_);
		}
		smart_ptr(const smart_ptr &str): ptr_(str.ptr_) {
			ptr_->count_++;
			common::xlog::log(common::LOG_ALL, "%d pointers left\n", ptr_->count_);
		}
		//valued pointer don't reference to the object, so --count;
		//value has a new pointer to reference to the object', so ++count;
		smart_ptr& operator=(const smart_ptr &str) {
			if(this == &str)
				return *this;
			++str.ptr_->count_;
			if(--ptr_->count_ == 0)
				delete ptr_;
			ptr_ = str.ptr_;
			return *this;
		}
		OBJ& operator*() { return *(ptr_->p_); }
		OBJ* operator->() { return ptr_->p_; }
		~smart_ptr() {
			if(--ptr_->count_ == 0)
				delete ptr_;
			else
				common::xlog::log(common::LOG_ALL, "%d pointers left\n", ptr_->count_);
		}
	private:
		u_ptr<OBJ> *ptr_;
	};
}
}

#endif /* TS_COMMON_SMART_PTR_H_ */
