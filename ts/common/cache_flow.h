/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_COMMON_CACHE_FLOW_H_
#define TS_COMMON_CACHE_FLOW_H_

#include "flow.h"
#include "buffer.h"
#include "public.h"
#include "event_interupter.h"

namespace ts {
namespace common {
    typedef struct cache_flow_node
    {
        const char *address_;
        int         size_;
    }cache_flow_node_;

    class cache_flow : public flow
    {
    public:
        cache_flow(unsigned buffer_chain, unsigned buffer_size);
        virtual ~cache_flow();
        virtual bool append(const char *object, int length);
        virtual int  get(int id, void *object, int length);
        virtual bool attach_under_flow(flow *file_flow);
        /** event notify mechanism to the other thread. **/
        virtual void attach_interupter(ts::event::event_interupter *interupter);
    private:
        typedef std::deque<cache_flow_node_> flow_node_list;
        flow_node_list flow_node_list_;
        typedef std::vector<ts::mem::buffer *> buffer_vector;
        buffer_vector buffer_vector_;
        unsigned                      buffer_chain_;
        unsigned                      buffer_size_;
        unsigned                      current_buffer_;
        flow                         *under_flow_;
        ts::event::event_interupter  *event_interupter_;
    };
}
}

#endif
