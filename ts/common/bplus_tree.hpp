/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 *   note: this file function can display whole tree by pass a template.
 */
#ifndef TS_COMMON_BPLUS_TREE_HPP_
#define TS_COMMON_BPLUS_TREE_HPP_

#include <queue>

namespace ts {
namespace common {

    template <typename T>
    void bplus_tree::display_tree(bplus_node *node)
    {
        std::queue<bplus_node *> df_traverse;
        df_traverse.push(node);
        
        int this_level = 1;
        int next_level = 0;
        
        bplus_node *temp_node = df_traverse.front();
        
        while(!temp_node->isleaf_)
        {
            for(int i=0; i<temp_node->capacity_+1; ++i)
            {
                if(i != temp_node->capacity_)
                {
                    common::xlog::log(LOG_ALL, " - ");
                    common::xlog::log(LOG_ALL, "%s", ((T *)temp_node->keys_[i])->m_nID);
                }
                bplus_node *temp = temp_node->points_[i];
                if(temp == NULL)
                    continue;
                df_traverse.push(temp);
                next_level++;
            }
            common::xlog::log(LOG_ALL, " | ");
            df_traverse.pop();
            if(--this_level == 0)
            {
                this_level = next_level;
                common::xlog::log(LOG_ALL, "\n");
                next_level=0;
            }
            temp_node = df_traverse.front();
        }
        
        /** print leaves of the b plus tree. **/
        int q_size = df_traverse.size();
        for(int i=0; i < q_size; ++i)
        {
            bplus_node *leaf_node = df_traverse.front();
            for(int j=0; j < leaf_node->capacity_; ++j)
            {
                common::xlog::log(LOG_ALL, " - ");
                common::xlog::log(LOG_ALL, "%s", ((T *)leaf_node->keys_[j])->m_nID);
            }
            common::xlog::log(LOG_ALL, " | ");
            df_traverse.pop();
        }
        common::xlog::log(LOG_ALL, "\n");
    }
}
}

#endif
