#include "enhanced_avl_tree.h"

namespace ts {
namespace common {
	_avl_tree_node_base* avl_tree_increment(_avl_tree_node_base *node)
	{
		if(node->right_ != NULL)
		{
			node = node->right_;
			while(node->left_ != NULL)
				node = node->left_;
		}
		else
		{
			_avl_tree_node_base *temp_node = node->parent_;
			while(temp_node != NULL && node == temp_node->right_)
			{
				node 	  = temp_node;
				temp_node = temp_node->parent_;
			}
			node = temp_node;
		}
		return node;
	}

	_avl_tree_node_base* avl_tree_decrement(_avl_tree_node_base *node)
	{
		if(node->left_ != NULL)
		{
			node = node->left_;
			while(node->right_ != NULL)
				node = node->right_;
		}
		else
		{
			_avl_tree_node_base *temp_node = node->parent_;
			while(temp_node != NULL && node == temp_node->left_)
			{
				node 	  = temp_node;
				temp_node = temp_node->parent_;
			}
			node = temp_node;
		}
		return node;
	}


}
}
