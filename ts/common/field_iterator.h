/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 *   note: 提供两种方式：
 *         1. 按照某一个field id从包中取出一堆field
 *         2. 按照某个field id从包中取出一个field，首先要确定的是其中只有一个field
 */
#ifndef TS_COMMON_FIELD_ITERATOR_H_
#define TS_COMMON_FIELD_ITERATOR_H_

#include "public.h"
#include "business_data_field.h"

namespace ts {
namespace common {

	class field_header
	{
	public:
		uint16_t field_id;
		uint16_t size;
	};

	class field_iterator
	{
	public:
		field_iterator(char *curr, char *end);
		void next();
		void retreive(ts::db::field_base *field);
		void get_header();
	private:
		field_header header_;
		char *curr_;
		char *end_;
		char *data_;
	};

}
}

#endif /* TS_COMMON_FIELD_ITERATOR_H_ */
