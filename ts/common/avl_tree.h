/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_COMMON_AVL_TREE_H_
#define TS_COMMON_AVL_TREE_H_

#include "public.h"
#include "allocator.h"
#include "mem_table.h"
#include "x_log.h"

#define MAX_(left,right)                                                \
        ((left >= right) ? left : right)    
#define LEFT_DEPTH(node)                                                \
        (node->left_ == NULL ? 0 : node->left_->depth_)
#define RIGHT_DEPTH(node)                                               \
        (node->right_ == NULL ? 0 : node->right_->depth_)
#define EVAL_DEPTH(node)                                                \
        (node->depth_ = MAX_(LEFT_DEPTH(node), RIGHT_DEPTH(node)) + 1)
#define SET_LEFT(node1, node2)                                          \
        {                                                               \
            node1->left_ = node2;                                       \
            if (node2 != NULL)                                          \
                node2->father_ = node1;                                 \
        }
#define SET_RIGHT(node1, node2)                                         \
        {                                                               \
            node1->right_ = node2;                                      \
            if (node2 != NULL)                                          \
                node2->father_ = node1;                                 \
        }
#define SET_FATHER(node1, node2, old_son)                               \
        {                                                               \
            node1->father_ = node2;                                     \
            if (node2 != NULL)                                          \
            {                                                           \
                if (node2->left_ == old_son)                            \
                    node2->left_ = node1;                               \
                else                                                    \
                    node2->right_=node1;                                \
            }                                                           \
            else                                                        \
                set_root(node1);                                        \
        }

namespace ts {
namespace common {

    typedef int (* compare_func)(const void *, const void *);

    struct avl_node
    {
        const void  *key_;
        avl_node    *left_;
        avl_node    *right_;
        avl_node    *father_;
        int          depth_;
    };

    class avl_tree
    {
    public:
        avl_tree(int max_unit, compare_func compareFunc, mem::allocator *allocator);
        virtual ~avl_tree();

        avl_node *t_insert(const void *object);
        bool t_delete(const void *object);
        avl_node *t_search(const void *object);
        avl_node *t_update(const void *object);

        void debug(avl_node *node);

        avl_node *get_root();
    private:
        void alter_tree(avl_node *node);
        void set_root(avl_node *node);
        void delete_node(avl_node *node);

    private:
        mem::mem_table                          *mem_table_;
        avl_node                                *root_node_;
        compare_func                             compare_func_;

    };
}
}

#endif
