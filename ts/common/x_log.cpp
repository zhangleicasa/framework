#include "x_log.h"

namespace ts {
namespace common {
	int xlog::level_  = LOG_ALL;
	FILE *xlog::file_ = stdout;

	void xlog::log(int loglevel, const char *fmt, ...)
	{
#ifndef WIN32
		struct timeval tv;
		gettimeofday(&tv, NULL);

		struct tm local_tm;
		localtime_r(&(tv.tv_sec), &local_tm);

		fprintf(file_, "%04d-%02d-%02d %02d:%02d:%02d %03d.%03d ", local_tm.tm_year + 1900, local_tm.tm_mon + 1, local_tm.tm_mday,
			local_tm.tm_hour, local_tm.tm_min, local_tm.tm_sec, (int)(tv.tv_usec/1000), (int)(tv.tv_usec % 1000));

		va_list va;
		va_start(va, fmt);
		vfprintf(file_, fmt, va);
		va_end(va);

		fflush(file_);
#endif
	}
}
}

