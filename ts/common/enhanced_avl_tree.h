/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_COMMON_ENHANCE_AVL_TREE_H_
#define TS_COMMON_ENHANCE_AVL_TREE_H_

#include "public.h"
#include "allocator.h"
#include "t_fix_mem.h"

#include <allocator.h>

/** one memtable has 256 records. **/
#define _MAX_UNIT_ 4

namespace ts {
namespace common {

	/***************************************************************************/
    /************************** avl tree operation. ****************************/
	/***************************************************************************/
	typedef struct _avl_tree_node_base
	{
		typedef _avl_tree_node_base*          _base_ptr;
		typedef const _avl_tree_node_base*    _const_base_ptr;//[todo: 1]
		typedef int                           _depth_type;

		_base_ptr    	parent_;
		_base_ptr    	left_;
		_base_ptr    	right_;
		_depth_type  	depth_;//[todo: 2]
	}_avl_tree_node_base;

	_avl_tree_node_base* avl_tree_increment(_avl_tree_node_base *node);
	_avl_tree_node_base* avl_tree_decrement(_avl_tree_node_base *node);

	/***************************************************************************/
    /************************** avl node operation. ****************************/
	/***************************************************************************/
	template<typename V>
	struct _avl_tree_node : public _avl_tree_node_base
	{
		V value_field_;
	};

	/***************************************************************************/
    /************************** iterator operation. ****************************/
	/***************************************************************************/
	template<typename V>
	struct _avl_tree_iterator
	{
		typedef _avl_tree_iterator<V> _self;
		typedef _avl_tree_node<V>*    _avl_node;

		typedef V 	 _value_type;
		typedef V&   _reference;
		typedef V*   _pointer;

		_avl_tree_iterator(_avl_tree_node<V>* _x): _base_node(_x) {};
		_avl_tree_iterator(): _base_node() {};

		_reference operator  *() const {return static_cast<_avl_node>(_base_node)->value_field_;}
		_pointer   operator ->() const {return static_cast<_avl_node>(_base_node)->value_field_;}

		_self operator ++()    { _base_node = avl_tree_increment(_base_node); return *this; }
		_self operator --()    { _base_node = avl_tree_decrement(_base_node); return *this; }
		_self operator ++(int) { _self _tmp = *this; _base_node = avl_tree_increment(_base_node); return _tmp;}
		_self operator --(int) { _self _tmp = *this; _base_node = avl_tree_decrement(_base_node); return _tmp;}

		_avl_tree_node_base* _base_node;
	};

	/***************************************************************************/
    /************************ avl tree implementation. *************************/
	/* 1. here K and V is the same type, so we transfer the same type.[TODO: 3]*/
	/* 2. interface is not compatible with the std::allocator.                 */
	/***************************************************************************/
	template<typename K, typename V = K, typename Compare = std::less<K>, typename Alloc = std::allocator<K> >
	class enhanced_avl_tree
	{
	public:
		typedef _avl_tree_node_base*   _avl_base_node_type;
		typedef _avl_tree_node<V>      _avl_node;
		typedef _avl_tree_node<V>*     _avl_node_type;
		typedef _avl_tree_iterator<V>  _iterator;

		typedef typename Alloc::template rebind<_avl_node>::other _node_allocator;
        typedef _node_allocator		   _chunk_manager;
        typedef Alloc	               _allocator_type;

	public:
		enhanced_avl_tree();
		~enhanced_avl_tree();

		/***************************************************************************/
		/********************the interfaces which avl tree provide.*****************/
		/***************************************************************************/
	public:
		_iterator find(const V &val) const;
		_iterator begin() const;
		_iterator end() const;

		void insert(const V &val);
		void erase(const V &val);
		void clear();

		bool empty() const;
		int  count(const V&val) const;

	public:
		_avl_node_type get_root();

		void print_binary_info()
		{
			chunk_.print_binary_info();
		}

        static void deep_traverse(_avl_node_type node);

	private:
		void adjust_avl_tree(_avl_base_node_type node);

		inline void set_parent(_avl_base_node_type _n1, _avl_base_node_type _n2, _avl_base_node_type _n3);
		inline void set_left(_avl_base_node_type _n1, _avl_base_node_type _n2);
		inline void set_right(_avl_base_node_type _n1, _avl_base_node_type _n2);
		inline int  depth_left(_avl_base_node_type _n);
		inline int  depth_right(_avl_base_node_type _n);
		inline void make_root(_avl_base_node_type node);
		inline int  eval_depth(_avl_base_node_type node);

	private:
		_chunk_manager      	chunk_;
		_avl_base_node_type 	root_;

	};

	/***************************************************************************/
    /************************ avl tree implementation. *************************/
	/***************************************************************************/
	template<typename K, typename V, typename Compare, typename Alloc>
	enhanced_avl_tree<K, V, Compare, Alloc>::enhanced_avl_tree()
	{
		make_root(NULL);
	}

	template<typename K, typename V, typename Compare, typename Alloc>
	enhanced_avl_tree<K, V, Compare, Alloc>::~enhanced_avl_tree()
	{

	}

	template<typename K, typename V, typename Compare, typename Alloc>
	void
	enhanced_avl_tree<K, V, Compare, Alloc>::adjust_avl_tree(_avl_base_node_type node)
	{
		while(node != NULL)
		{
			int left_depth, right_depth, this_depth = 0;
			_avl_base_node_type _parent_node = NULL;
			_avl_base_node_type _l_branch, _r_branch   = NULL;
			_avl_base_node_type _ll_branch, _lr_branch = NULL;
			_avl_base_node_type _rl_branch, _rr_branch = NULL;

			left_depth   = node->left_  == NULL ? 0 : node->left_->depth_;
			right_depth  = node->right_ == NULL ? 0 : node->right_->depth_;
			_parent_node = node->parent_;

			/** as four situations in rotate the tree. **/
			if(left_depth - right_depth >= 2)
			{
				/** left branch of node is deeper than right branch. **/
				_l_branch   = node->left_;
				_ll_branch  = _l_branch->left_;
				_lr_branch  = _l_branch->right_;

				int _ll_depth = _l_branch->left_   == NULL ? 0 : _l_branch->left_->depth_;
				int _lr_depth = _l_branch->right_  == NULL ? 0 : _l_branch->right_->depth_;

				if(_ll_depth >= _lr_depth)
				{
					/** left left situation. **/
					set_left(node, _lr_branch);
					set_right(_l_branch, node);
					set_parent(_l_branch, _parent_node, node);
					node->depth_       = eval_depth(node);
					_l_branch->depth_  = eval_depth(_l_branch);
					node 			   = _parent_node;
				}
				else
				{
					/** left right situation. **/
					int _lrl_depth = _lr_branch->left_  == NULL ? 0 : _lr_branch->left_->depth_;
					int _lrr_depth = _lr_branch->right_ == NULL ? 0 : _lr_branch->right_->depth_;
					_avl_base_node_type _lrl_branch, _lrr_branch = NULL;
					_lrl_branch = _lr_branch->left_;
					_lrr_branch = _lr_branch->right_;

					set_left(node, _lrr_branch);
					set_right(_l_branch, _lrl_branch);
					set_right(_lr_branch, node);
					set_left(_lr_branch, _l_branch);
					set_parent(_lr_branch, _parent_node, node);
					node->depth_       = eval_depth(node);
					_l_branch->depth_  = eval_depth(_l_branch);
					_lr_branch->depth_ = eval_depth(_lr_branch);
					node 			   = _parent_node;
				}
			}
			else if(right_depth - left_depth >= 2)
			{
				_r_branch  = node->right_;
				_rl_branch = _r_branch->left_;
				_rr_branch = _r_branch->right_;

				int _rl_depth = _r_branch->left_  == NULL ? 0 : _r_branch->left_->depth_;
				int _rr_depth = _r_branch->right_ == NULL ? 0 : _r_branch->right_->depth_;

				if(_rr_depth >= _rl_depth)
				{
					/** right right situation. **/
					set_right(node, _rl_branch);
					set_left(_r_branch, node);
					set_parent(_r_branch, _parent_node, node);
					node->depth_       = eval_depth(node);
					_r_branch->depth_  = eval_depth(_r_branch);
					node = _parent_node;
				}
				else
				{
					/** right left situation. **/
					int _rll_depth = _rl_branch->left_  == NULL ? 0 : _rl_branch->left_->depth_;
					int _rlr_depth = _rl_branch->right_ == NULL ? 0 : _rl_branch->right_->depth_;
					_avl_base_node_type _rll_branch, _rlr_branch = NULL;
					_rll_branch = _rl_branch->left_;
					_rlr_branch = _rl_branch->right_;

					set_right(node, _rll_branch);
					set_left(_r_branch, _rlr_branch);
					set_left(_rl_branch, node);
					set_right(_rl_branch, _r_branch);
					set_parent(_rl_branch, _parent_node, node);
					node->depth_       = eval_depth(node);
					_r_branch->depth_  = eval_depth(_r_branch);
					_rl_branch->depth_ = eval_depth(_rl_branch);
					node = _parent_node;
				}
			}
			else
			{
				this_depth = (left_depth > right_depth ? left_depth : right_depth) + 1;
				if(node->depth_ == this_depth)
					return;
				else
				{
					node->depth_ = this_depth;
					node		 = _parent_node;
				}
			}

		}
	}

	template<typename K, typename V, typename Compare, typename Alloc>
	typename enhanced_avl_tree<K, V, Compare, Alloc>::_iterator
	enhanced_avl_tree<K, V, Compare, Alloc>::find(const V& val) const
	{
		_avl_base_node_type temp = root_;
		int compare_result  = 0;
		while(temp != NULL)
		{
			_avl_node_type node = static_cast<_avl_node_type>(temp);
			if(val == node->value_field_)
				return node;
			if(val > node->value_field_)
				temp = temp->right_;
			if(val < node->value_field_)
				temp = temp->left_;
		}
		return NULL;
	}

	template<typename K, typename V, typename Compare, typename Alloc>
	void
	enhanced_avl_tree<K, V, Compare, Alloc>::insert(const V& val)
	{
		_avl_base_node_type _this_node, _last_node = NULL;
		_avl_node_type _new_node, _temp_node = NULL;

		/** allocate the space for storing a object. **/
		_new_node = (_avl_node_type)(chunk_.allocate(1));
		_new_node ->value_field_ = val;
		_new_node ->parent_      = NULL;
		_new_node ->left_		 = NULL;
		_new_node ->right_       = NULL;
		_new_node ->depth_       = 1;

		/** find the node which will be insert to. **/
		int _is_left = 0;
		_this_node  = get_root();
		if(_this_node == NULL)
		{
			set_parent(_new_node, _this_node, NULL);
			return;
		}
		while(_this_node != NULL)
		{
			_last_node = _this_node;
			_temp_node  = static_cast<_avl_node_type>(_this_node);
			if(val < _temp_node->value_field_)
			{
				_this_node = _this_node->left_;
				_is_left   = 1;
			}
			else
			{
				_this_node = _this_node->right_;
				_is_left   = 0;
			}
		}

		/** set the _last_node as the _new_node's parent. **/
		_new_node ->parent_  = _last_node;
		if(_is_left)
			_last_node->left_ = _new_node;
		else
			_last_node->right_= _new_node;

		/** rotate the avl tree for balance. **/
		adjust_avl_tree(_last_node);
		return;

	}

	template<typename K, typename V, typename Compare, typename Alloc>
	inline void
	enhanced_avl_tree<K, V, Compare, Alloc>::erase(const V& val)
	{
		/** find the node which will be deleted. **/
		_iterator node = find(val);
		/** [todo]: if not found return from function. **/
		if(node._base_node == NULL)
			return;

		_avl_base_node_type this_node, father_node, target_node = NULL;
        int left_depth, right_depth = 0;

        this_node   = node._base_node;
        father_node = this_node->parent_;

        if (this_node->depth_ != 1)
        {
            left_depth  = depth_left(this_node);
            right_depth = depth_right(this_node);
            if (left_depth > right_depth)
            {
                /** find the rightmost in the left tree **/
                target_node  = this_node->left_;
                while (target_node->right_!=NULL)
                    target_node = target_node->right_;
            }
            else
            {
                /** find the leftmost in the right tree **/
                target_node = this_node->right_;
                while (target_node->left_ != NULL)
                    target_node = target_node->left_;
            }

            /** move object. **/
            father_node = target_node->parent_;
            if (this_node->left_ == target_node)
            {
                set_right(target_node, this_node->right_);
                father_node = target_node;
            }
            else if (this_node->right_ == target_node)
            {
            	set_left(target_node, this_node->left_);
                father_node = target_node;
            }
            else
            {
                if (father_node->left_ == target_node)
                {
                	set_left(father_node, target_node->right_);
                }
                else
                {
                	set_right(father_node, target_node->left_);
                }
                set_right(target_node, this_node->right_);
                set_left(target_node, this_node->left_);
            }

            target_node->depth_ = this_node->depth_;
            set_parent(target_node, this_node->parent_, this_node);
        }
        else
        {
            if (father_node != NULL)
            {
                if (father_node->left_ == this_node)
                    father_node->left_ = NULL;
                else
                    father_node->right_ = NULL;
            }
            else
                make_root(NULL);
        }
        adjust_avl_tree(father_node);

        _avl_node_type node_deleted = static_cast<_avl_node_type>(node._base_node);
        V* pointer = &node_deleted->value_field_;
        common::xlog::log(LOG_ALL, "point address: %p\n", pointer);
//        ((_allocator_type)chunk_).destroy(pointer);
	}

	template<typename K, typename V, typename Compare, typename Alloc>
	inline void
	enhanced_avl_tree<K, V, Compare, Alloc>::set_parent(_avl_base_node_type _n1, _avl_base_node_type _root_node, _avl_base_node_type _n3)
	{
		_n1 ->parent_ = _root_node;
		if(_root_node != NULL)
			if(_root_node ->left_ == _n3)
				_root_node->left_ = _n1;
			else
				_root_node->right_ = _n1;
		else
			make_root(_n1);
	}

	template<typename K, typename V, typename Compare, typename Alloc>
	inline void
	enhanced_avl_tree<K, V, Compare, Alloc>::set_left(_avl_base_node_type _n1, _avl_base_node_type _n2)
	{
		_n1->left_ = _n2;
		if(_n2 != NULL)
			_n2 ->parent_ = _n1;
	}

	template<typename K, typename V, typename Compare, typename Alloc>
	inline void
	enhanced_avl_tree<K, V, Compare, Alloc>::set_right(_avl_base_node_type _n1, _avl_base_node_type _n2)
	{
		_n1->right_ = _n2;
		if(_n2 != NULL)
			_n2 ->parent_ = _n1;
	}

	template<typename K, typename V, typename Compare, typename Alloc>
	inline int
	enhanced_avl_tree<K, V, Compare, Alloc>::eval_depth(_avl_base_node_type node)
	{
		return (depth_left(node) >= depth_right(node) ? depth_left(node) : depth_right(node)) + 1;
	}

	template<typename K, typename V, typename Compare, typename Alloc>
	inline int
	enhanced_avl_tree<K, V, Compare, Alloc>::depth_left(_avl_base_node_type node)
	{
		return node->left_ == NULL ? 0 : node->left_->depth_;
	}

	template<typename K, typename V, typename Compare, typename Alloc>
	inline int
	enhanced_avl_tree<K, V, Compare, Alloc>::depth_right(_avl_base_node_type node)
	{
		return node->right_ == NULL ? 0 : node->right_->depth_;
	}

	template<typename K, typename V, typename Compare, typename Alloc>
	void
	enhanced_avl_tree<K, V, Compare, Alloc>::make_root(_avl_base_node_type node)
	{
		root_					 = node;
	}

	template<typename K, typename V, typename Compare, typename Alloc>
	typename enhanced_avl_tree<K, V, Compare, Alloc>::_avl_node_type
	enhanced_avl_tree<K, V, Compare, Alloc>::get_root()
	{
		return static_cast<_avl_node_type>(root_);
	}

}
}

#include "enhanced_avl_tree.hpp"

#endif
