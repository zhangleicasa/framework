/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 *   note: hash function is from open source.
 */
#ifndef TS_COMMON_HASH_FUNCTION_H_
#define TS_COMMON_HASH_FUNCTION_H_

#include "noncopyable.h"

#if defined(_MSC_VER) && (_MSC_VER < 1600)
    typedef unsigned char uint8_t;
    typedef unsigned int uint32_t;
    typedef unsigned __int64 uint64_t;
#else 
#include <stdint.h>
#endif

namespace ts {
namespace common {
    class hash_function : public ts::noncopyable
    {
    public:
        static uint32_t murmurhash1(const void *key, int len, uint32_t seed);
        static unsigned long tshash(const char *key, uint32_t seed);
    };
}
}

#endif
