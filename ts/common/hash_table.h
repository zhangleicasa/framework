/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 *   note: this hash table will store the bucket chain by hash function, the bucket chain will contain 
 *         multiple values which has the same hash key.
 */
#ifndef TS_COMMON_HASH_TABLE_H_
#define TS_COMMON_HASH_TABLE_H_

#include "public.h"
#include "log_helper.h"
#include "time_helper.h"

#define CACHE_LINE 64

namespace ts {
namespace common {
    
	class hash_table;

	class hash_table_iterator
	{
		friend class hash_table;
	public:
		hash_table_iterator(unsigned bucket_size, unsigned tuple_size)
			: bucket_size_(bucket_size), tuple_size_(tuple_size),
			  cur_(NULL), free_(NULL), next_(NULL)
		{
			printf("construct the hash table iterator.\n");
		}
		virtual ~hash_table_iterator()
		{
			printf("deconstruct the hash table iterator.\n");
		}
		void *next();
	private:
		void     *cur_;
		void     *free_;
		void     *next_;
		unsigned  bucket_size_;                                  /** size of the bucket size. **/
		unsigned  tuple_size_;                                   /** size of tuple hash table store. **/
	};

    class hash_table
    {
    public:
        hash_table(unsigned bucket_chain_count, unsigned bucket_size, unsigned tuple_size);
        virtual ~hash_table();
        void locate_hash_table_iterator(hash_table_iterator &itr, unsigned offset);
        void *allocate(unsigned offset);
        hash_table_iterator create_hash_table_iterator();

        void debug();
        void debug(uint32_t offset);

    private:
        inline void alloc_space_for_bucket_in_hashtable()
        {
            current_repo_start_      = (char *)malloc(memory_repo_size_);
            memset(current_repo_start_, 0, memory_repo_size_);
            memory_repo_list_.push_back(current_repo_start_);
            current_repo_distribute_ = 0;
        }

        inline unsigned get_cacheline_aligned_space(unsigned bucket_size)
        {
            return ((bucket_size + 2*sizeof(void *) -1) / CACHE_LINE + 1) * CACHE_LINE;
        }

    private:
        /** following variables will be transferred. **/
        unsigned bucket_chain_count_;                               /** how many bucket chains hash table has. **/
        unsigned bucket_size_;                                      /** size of the bucket size. **/
        unsigned tuple_size_;                                       /** size of tuple hash table store. **/
        void **  bucket_start_address_;                             /** address of start of buckets. **/
        unsigned memory_repo_size_;                                 /** size of memory repo once apply. **/
        unsigned current_repo_distribute_;                          /** size of memory the current repo distributes. **/
        char *current_repo_start_;                                  /** the start address of current repo. **/
        std::vector<char *> memory_repo_list_;                      /** the memory repo list. **/
    };

}
}

#endif
