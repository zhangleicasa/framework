/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_COMMON_xlog_H_
#define TS_COMMON_xlog_H_

#include "public.h"

namespace ts {
namespace common {
	typedef enum {
		LOG_FATAL		= 	0X01,
		LOG_ERROR		= 	0X02,
		LOG_WARNING		=   0X04,
		LOG_ALL			=   0XFF
	}LOG_LEVEL;

	class xlog
	{
	public:
		static void log(int loglevel, const char *fmt, ...);
	private:
		static int level_ ;
	private:
		static FILE *file_;
	};

}
}

#endif /* TS_COMMON_xlog_H_ */
