#include "avl_tree.h"

namespace ts {
namespace common {
    avl_tree::avl_tree(int max_unit, compare_func compareFunc, mem::allocator *allocator)
    {
        mem_table_      = new mem::mem_table(sizeof(avl_node), max_unit, allocator);
        compare_func_   = compareFunc;
        mem_table_->alloc();
        set_root(NULL);
    }

    avl_tree::~avl_tree()
    {
        delete mem_table_;
    }

    avl_node *avl_tree::t_insert(const void *object)
    {
        avl_node *new_node, *this_node, *last_node;
        int is_left;

        new_node            = (avl_node *)(mem_table_->alloc());
        new_node->key_      = object;
        new_node->left_     = NULL;
        new_node->right_    = NULL;
        new_node->depth_    = 1;

        this_node           = get_root();
        if (this_node==NULL)
        {
            SET_FATHER(new_node, get_root(), NULL);
            return new_node;
        }

        while (this_node != NULL)
        {
            last_node       = this_node;
            if (compare_func_(object, this_node->key_) <0 )
            {
                this_node   = this_node->left_;
                is_left     = 1;
            }
            else
            {
                this_node   = this_node->right_;
                is_left     = 0;
            }
        }

        new_node->father_   = last_node;
        if (is_left)
            last_node->left_= new_node;
        else
            last_node->right_= new_node;

        alter_tree(last_node);

        return new_node;
    }
        
    bool avl_tree::t_delete(const void *object)
    {
        avl_node *node = t_search(object);
        if(node != NULL)
        {
            delete_node(node);
            return true;
        }
        else
        {
            return false;
        }
    }
        
    avl_node *avl_tree::t_search(const void *object)
    {
        avl_node *temp_node = get_root();
        while(temp_node != NULL)
        {
            int comp_result = compare_func_(object, temp_node->key_);
            if(comp_result == 0)
                return temp_node;
            if(comp_result == 1)
                temp_node = temp_node->right_;
            if(comp_result == -1)
                temp_node = temp_node->left_;
        }
        return NULL;
    }
        
    avl_node *avl_tree::t_update(const void *object)
    {
        return NULL;
    }

    void avl_tree::debug(avl_node *node)
    {
        avl_node *temp_node = get_root();
		int count = 0;
		int count1 = 0;
		std::queue<avl_node*> que;
		que.push(temp_node);
		count++;
		while(true)
		{
			while(count-- != 0)
			{
				avl_node *n = que.front();
				que.pop();
				common::xlog::log(LOG_ALL, " %s  ", n->key_);
				if(n->left_ != NULL)
				{
					que.push(n->left_);
					count1 ++;
				}
				if(n->right_!= NULL)
				{
					que.push(n->right_);
					count1 ++;
				}
			}
			common::xlog::log(LOG_ALL, "\n");
			if(count1 != 0)
			{
				count = count1;
				count1= 0;
			}
			else
				break;
		}
    }

    void avl_tree::alter_tree(avl_node *node)
    {
        while (node != NULL)
        {
            int left_depth, right_depth, this_depth = 0;
            avl_node *l_tree, *ll_tree, *lr_tree = NULL;
            avl_node *r_tree, *rl_tree, *rr_tree = NULL;
            avl_node *father_node = NULL;
            
            left_depth      = LEFT_DEPTH(node);
            right_depth     = RIGHT_DEPTH(node);
            father_node     = node->father_;
            if (left_depth-right_depth>=2)
            {
                r_tree      = node->right_;
                l_tree      = node->left_;
                lr_tree     = l_tree->right_;
                ll_tree     = l_tree->left_;
                if (LEFT_DEPTH(l_tree) >= RIGHT_DEPTH(l_tree))
                {
                    /** left left type. **/
                    SET_LEFT(node, lr_tree);
                    SET_RIGHT(l_tree, node);
                    SET_FATHER(l_tree, father_node, node);
                    EVAL_DEPTH(node);
                    EVAL_DEPTH(l_tree);
                    node = father_node;
                }
                else
                {
                    avl_node *lrr_tree, *lrl_tree = NULL;
            
                    /** left right type. **/
                    lrr_tree    = lr_tree->right_;
                    lrl_tree    = lr_tree->left_;

                    SET_LEFT(node, lrr_tree);
                    SET_RIGHT(l_tree, lrl_tree);
                    SET_RIGHT(lr_tree, node);
                    SET_LEFT(lr_tree, l_tree);
                    SET_FATHER(lr_tree, father_node, node);
                    EVAL_DEPTH(node);
                    EVAL_DEPTH(l_tree);
                    EVAL_DEPTH(lr_tree);

                    node = father_node;
                }
            }
            else if (right_depth-left_depth>=2)
            {
                l_tree      = node->left_;
                r_tree      = node->right_;
                rl_tree     = r_tree->left_;
                rr_tree     = r_tree->right_;
                if (RIGHT_DEPTH(r_tree) >= LEFT_DEPTH(r_tree))
                {
                    /** right right type. **/
                    SET_RIGHT(node, rl_tree);
                    SET_LEFT(r_tree, node);
                    SET_FATHER(r_tree, father_node, node);
                    EVAL_DEPTH(node);
                    EVAL_DEPTH(r_tree);
                    node = father_node;
                }
                else
                {
                    avl_node *rll_tree, *rlr_tree = NULL;
            
                    /** right left type. **/
                    rll_tree        = rl_tree->left_;
                    rlr_tree        = rl_tree->right_;
                    SET_RIGHT(node,rll_tree);
                    SET_LEFT(r_tree,rlr_tree);
                    SET_LEFT(rl_tree,node);
                    SET_RIGHT(rl_tree,r_tree);
                    SET_FATHER(rl_tree,father_node,node);
                    EVAL_DEPTH(node);
                    EVAL_DEPTH(r_tree);
                    EVAL_DEPTH(rl_tree);
                    node = father_node;
                }
            }
            else
            {
                this_depth = MAX_(left_depth, right_depth) + 1;
                if (node->depth_ == this_depth)
                    return;
                else
                {
                    node->depth_ = this_depth;
                    node         = father_node;
                }
            }
        }
    }
        
    void avl_tree::set_root(avl_node *node)
    {
        avl_node *head = ((avl_node *)(mem_table_->get_object_by_id(0)));
        head->father_  = node;
    }
        
    avl_node *avl_tree::get_root()
    {
        root_node_ = ((avl_node *)(mem_table_->get_object_by_id(0)))->father_;
        return root_node_;
    }

    void avl_tree::delete_node(avl_node *node)
    {
        avl_node *this_node, *father_node, *target_node = NULL;
        int left_depth, right_depth = 0;

        this_node   = node;
        father_node = this_node->father_;
        if (this_node->depth_ != 1)
        {
            left_depth  = LEFT_DEPTH(this_node);
            right_depth = RIGHT_DEPTH(this_node);
            if (left_depth > right_depth)
            {
                /** find the rightmost in the left tree **/
                target_node  = this_node->left_;
                while (target_node->right_!=NULL)
                    target_node = target_node->right_;
            }
            else
            {
                /** find the leftmost in the right tree **/
                target_node = this_node->right_;
                while (target_node->left_ != NULL)
                    target_node = target_node->left_;
            }
            
            /** move object. **/
            father_node = target_node->father_;
            if (this_node->left_ == target_node)
            {
                SET_RIGHT(target_node, this_node->right_);
                father_node = target_node;
            }
            else if (this_node->right_ == target_node)
            {
                SET_LEFT(target_node, this_node->left_);
                father_node = target_node;
            }
            else
            {
                if (father_node->left_ == target_node)
                {
                    SET_LEFT(father_node, target_node->right_);
                }
                else
                {
                    SET_RIGHT(father_node, target_node->left_);
                }
                SET_RIGHT(target_node, this_node->right_);
                SET_LEFT(target_node, this_node->left_);
            }
            
            target_node->depth_ = this_node->depth_;
            SET_FATHER(target_node, this_node->father_, this_node);
        }
        else
        {
            if (father_node != NULL)
            {
                if (father_node->left_ == this_node)
                    father_node->left_ = NULL;
                else
                    father_node->right_ = NULL;
            }
            else
                set_root(NULL);
        }

        alter_tree(father_node);
        mem_table_->free(node);
    }

}
}
