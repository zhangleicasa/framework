#include "file_flow.h"

#define FILE_NAME_LENGTH   256
#define INDEX_BLOCK_OFFSET 2

namespace ts {
namespace common {
    file_flow::file_flow(const char *file_name)
    {
        char content_file_name[FILE_NAME_LENGTH] = "";
        char index_file_name[FILE_NAME_LENGTH]   = "";
        sprintf(content_file_name, "%s.con", file_name);
        sprintf(index_file_name, "%s.idx", file_name);
        content_file_     = fopen(content_file_name, "ab+");
        index_file_       = fopen(index_file_name, "ab+");
        file_flow_iterms_ = 0;
        FPOS_SET(position_, 0);
    }

    bool file_flow::append(const char *object, int length)
    {
        int len = length;
        fsetpos(content_file_, &position_);
        common::xlog::log(LOG_ALL, "pos: %d\n", len);
        fwrite(&len, sizeof(fpos_t), 1, content_file_);
        fwrite(object, 1, length, content_file_);
        FPOS_SET(position_, FPOS_GET(position_)+sizeof(fpos_t)+len);

        int offset = FPOS_GET(position_);
        if(++file_flow_iterms_%INDEX_BLOCK_OFFSET == 0)
        {
            /** insert the object into the index file. **/
            fwrite(&offset, sizeof(fpos_t), 1, index_file_);
        }
        return true;
    }

    int file_flow::get(int id, void *object, int length)
    {
        
        return 0;
    }
}
}

