/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_API_TRADE_API_H_
#define TS_API_TRADE_API_H_

namespace ts {
namespace api {
    class trade_api
    {
    public:
        trade_api();
        virtual ~trade_api();
    };
}
}

#endif /* TS_API_TRADE_API_H_ */