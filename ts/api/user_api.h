/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_API_USER_API_H_
#define TS_API_USER_API_H_

namespace ts {
namespace api {
    class user_api
    {
    public:
        user_api();
        virtual ~user_api();
    };
}
}

#endif /* TS_API_USER_API_H_ */