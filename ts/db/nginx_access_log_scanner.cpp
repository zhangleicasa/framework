#include "nginx_access_log_scanner.h"

static char access_log_buffer[1024] = "";
static int  access_log_buffer_offset[9] = {0, 32, 40, 48, 112, 240, 244, 248, 312};

#ifndef WIN32

namespace ts {
namespace db {
	void nginx_access_log_scanner::init()
	{
		my_con_ = (MYSQL *)malloc(sizeof(MYSQL));
		mysql_init(my_con_);
		my_con_ = mysql_real_connect(my_con_, "127.0.0.1", "root", "", "test", 3309, NULL, CLIENT_FOUND_ROWS);
		if(NULL == my_con_)
			common::xlog::log(common::LOG_ALL, "connect mysql failed.\n");
		else
			common::xlog::log(common::LOG_ALL, "connect mysql success.\n");
	}

	void nginx_access_log_scanner::scan_access_log()
	{
		FILE *file = fopen(access_log_path_.c_str(), "r");
		if(NULL == file)
		{
			printf("can not open the file '%s'!\n", access_log_path_.c_str());
			return;
		}
		fseek(file, current_location_, SEEK_SET);
		unsigned len = 0;
		while(!feof(file))
		{
			memset(access_log_buffer, 0x0, 1024);
			fgets(access_log_buffer, 1024, file);
			len = strlen(access_log_buffer);
			if(len == 0 || access_log_buffer[len-1] != '\n')
				continue;
			access_log_buffer[len-1] = '\n';
			current_location_ += len;
			parser_access_log(access_log_buffer);
		}
		fclose(file);
	}

	void nginx_access_log_scanner::parser_access_log(char *line)
	{
		char element[256] = "";
		char *p = line;
		char *q = element;
		bool close = true;
		int offset = 0;
		access_log_data data;
		while(*p != '\n')
		{
			if(*p == '"' && !close)
			{
				/* 如果是"且为开区间，证明区间已经关闭 */
				*q++ = *p++;
				close = true;
			}
			else if(*p == '"' && close)
			{
				/* 如果是"且为闭区间 */
				q = element;
				*q++ = *p++;
				close = false;
			}
			else if(*p == '[')
			{
				q = element;
				*q++ = *p++;
				close = false;
			}
			else if(*p == ']')
			{
				*q++ = *p++;
				close = true;
			}
			else if(*p == ' ' && !close)
			{
				/* 遇到' '如果是开区间，啥都不做 */
				*q++ = *p++;
			}
			else if(*p == ' ' && close)
			{
				/* 遇到' '如果是闭区间就打印 */
				p++;
				strcpy((char *)(&data)+access_log_buffer_offset[offset], element);
				offset++;
				q = element;
				memset(element, 0x0, 256);
			}
			else
				*q++ = *p++;
		}
		strcpy((char *)(&data)+access_log_buffer_offset[offset], element);

		printf("data    \n %s\n %s\n %s\n %s\n %s\n %d\n %d\n %s\n %s\n\n\n\n",
				data.remote_addr_,data.dot_1_, data.dot_2_, data.time_local_, data.request_,
				data.status_, data.body_bytes_send_, data.http_user_agent_, data.http_x_forwarded_for_);
		char buffer[1024] = "";
		sprintf(buffer, "insert into nginx_access_log values('%s','%s','%s','%s',%s,%d, %d,%s,'%s');",
				data.remote_addr_,data.dot_1_, data.dot_2_, data.time_local_, data.request_,
				data.status_, data.body_bytes_send_, data.http_user_agent_, data.http_x_forwarded_for_);
		mysql_query(my_con_, buffer);

	}
}
}

#endif