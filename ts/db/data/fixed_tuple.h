/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_DB_DATA_FIXED_TUPLE_H_
#define TS_DB_DATA_FIXED_TUPLE_H_

#include "schema.h"

namespace ts {
namespace data {
	class fixed_tuple
	{
	public:
		fixed_tuple(const char *buffer, schema *sch) {}
		virtual ~fixed_tuple() {}
		void dump_csv() {}
	private:
		char buffer_[1024];
		schema *schema_;
	};
}
}

#endif /* TS_DB_DATA_FIXED_TUPLE_H_ */
