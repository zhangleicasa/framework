/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_DB_DATA_SCHEMA_H_
#define TS_DB_DATA_SCHEMA_H_

#include "column_type.h"
#include "x_log.h"
/**
 * inctrade:
 * 		1.  TradingDay                      string         8+1        0
 * 		2.  SettlementGroupID               string         8+1        8
 * 		3.  SettlementID                    int                       16
 * 		4.  TradeID                         string         12+1       20
 * 		5.  Direction                       char           1          32
 * 		6.  OrderSysID                      string         12+1       33
 * 		7.  ParticipantID                   string         10+1       45
 * 		8.  ClientID                        string         10+1       55
 * 		9.  TradingRole                     char           1          65
 * 		10. AccountID                       string         12+1       66
 * 		11. InstrumentID                    string         30+1       78
 * 		12. OffsetFlag                      char           1          108
 * 		13. HedgeFlag                       char           1          109
 * 		14. Price                           double                    110
 * 		15. Volume                          int                       118
 * 		16. TradeTime                       string         8+1        122
 * 		17. TradeType                       char           1          130
 * 		18. PriceSource                     char           1          131
 * 		19. UserID                          string         15+1       132
 * 		20. OrderLocalID                    string         12+1       147
 * 		21. ClearingPartID                  string         10+1       159
 * 		22. BusinessUnit                    string         20+1       169
 * 		23. ClientHedgeFlag                 char           1          189
 * 		24. TradeSource                     char           1          190
 * 		25. PriceEx                         double                    191
 * 		26. TID                             int                       199
 * **/

static int inc_trade_field[26] = {0, 8, 16, };

namespace ts {
namespace data {
	class schema
	{
	public:
		schema(std::vector<column_type> &columns) : columns_(columns), total_size_(0)
		{
			for(unsigned i = 0, accum = 0 ; i < columns_.size(); ++i)
			{
				column_offsets_.push_back(accum);
				accum += columns_[i].get_length();
				total_size_ += columns_[i].get_length();
			}
		}
		schema(const schema &s)
		{
			this->columns_ = s.columns_;
			this->column_offsets_ = s.column_offsets_;
			this->total_size_ = s.total_size_;
		}
		void *get_column_address(int index, void *tuple)
		{
			return (char *)tuple + column_offsets_[index];
		}
		unsigned get_total_size()
		{
			return total_size_;
		}
		void show_offset()
		{
			for(unsigned i = 0; i < column_offsets_.size(); ++i)
				common::xlog::log(common::LOG_ERROR, "offset: %d.\n", column_offsets_[i]);
		}
	public:
		std::vector<column_type> columns_;
	private:
		unsigned total_size_;
		std::vector<unsigned> column_offsets_;
	};

}
}

#endif /* TS_DB_DATA_SCHEMA_H_ */
