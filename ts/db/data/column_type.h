/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_DB_DATA_COLUMN_TYPE_H_
#define TS_DB_DATA_COLUMN_TYPE_H_

#include "type_cast_func.h"

namespace ts {
namespace data {
	class column_type
	{
	public:
		column_type(expr::data_type dtype, int csize = 0, bool nullable = true)
			:data_type_(dtype), column_size_(csize), nullable_(nullable)
		{
			init_column_size_by_type();
		}
		column_type(const column_type &c)
		{
			this->data_type_ = c.data_type_;
			this->column_size_ = c.column_size_;
			this->nullable_ = c.nullable_;
		}
		void init_column_size_by_type()
		{
			switch(data_type_)
			{
			case expr::t_int:
				column_size_ = sizeof(uint32_t);
				break;
			case expr::t_float:
				column_size_ = sizeof(uint32_t);
				break;
			case expr::t_double:
				column_size_ = sizeof(uint64_t);
				break;
			case expr::t_string:
				break;
			default:
				//todo: add log output message.
				printf("no data type recognized.\n");
				return;
			}
		}
		int get_length() const
		{
			return column_size_;
		}
	public:
		int column_size_;
	private:
		expr::data_type data_type_;
		bool nullable_;
	};
}
}

#endif /* TS_DB_DATA_COLUMN_TYPE_H_ */
