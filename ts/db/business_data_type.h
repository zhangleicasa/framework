/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_DB_BUSINESS_DATA_TYPE_H_
#define TS_DB_BUSINESS_DATA_TYPE_H_

#include "public.h"

namespace ts {
namespace db {
	class int_type
	{
	public:
		int_type(void) { value = 0; }
		int_type(const int_type &i) { value = i; }
		void set_value(const int &i) { value = i; }
		const int get_value() const { return value; }
		const int_type & operator = (const int_type &i);
		const int operator = (const int &i);
	private:
		int value;
	};

	class char_type
	{
	public:
		char_type(void) { value = '\0'; }
		char_type(const char_type &c) { set_value(c.get_value()); }
		void set_value(const char c) { value = c; }
		const char get_value() const { return value; }
		const char_type & operator = (const char_type &c);
		const char operator = (const char c);
	private:
		char value;
	};

	template <int length> class string_type
	{
	public:
		string_type(void) { buffer[0] = '\0'; }
		string_type(const char *p) { set_value(p); }
		const char *get_value() const { return buffer; }
		const string_type & operator = (const string_type<length>& s);
		const char * operator = (const char *p);
		void set_value(const char *p);
	private:
		char buffer[length+1];
	};

	template <int length, int precision> class float_type
	{
	public:
		float_type(void) { value = 0.0; }
		float_type(const float_type &f) { set_value(f.get_value()); }
		void set_value(const double f) { value = f; }
		const double get_value() const { return value; }
		const float_type & operator = (const float_type &f);
		const double operator = (const double f);
	private:
		double value;
	};

	const int_type & int_type::operator = (const int_type &i)
	{
		set_value(i.get_value());
		return i;
	}

	const int int_type::operator = (const int &i)
	{
		set_value(i);
		return i;
	}

	const char_type & char_type::operator = (const char_type &c)
	{
		set_value(c.get_value());
		return c;
	}

	const char char_type::operator = (const char c)
	{
		set_value(c);
		return c;
	}

	template <int length>
	const string_type & string_type<length>::operator = (const string_type<length>& s)
	{
		set_value(s.get_value());
		return s;
	}

	template <int length>
	const char * string_type<length>::operator = (const char *p)
	{
		set_value(p);
		return p;
	}

	template <int length>
	void string_type<length>::set_value(const char *p)
	{
		if(NULL != p)
		{
			strncpy(buffer, p, length);
			buffer[length] = '\0';
		}
		else
			buffer[0] = '\0';
	}

	template <int length, int precision>
	const float_type & float_type<length, precision>::operator = (const float_type &f)
	{
		set_value(f.get_value());
		return f;
	}

	template <int length, int precision>
	const double float_type<length, precision>::operator = (const double f)
	{
		set_value(f);
		return f;
	}
}
}

#endif /* TS_DB_BUSINESS_DATA_TYPE_H_ */
