/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_DB_BUSINESS_DATA_FIELD_H_
#define TS_DB_BUSINESS_DATA_FIELD_H_

namespace ts {
namespace db {
	class field_base
	{
	public:
		uint16_t get_fid() { return field_id_; }
	private:
		uint16_t field_id_;
	};

	class field_order: public field_base
	{
	public:

	};
}
}

#endif /* TS_DB_BUSINESS_DATA_FIELD_H_ */
