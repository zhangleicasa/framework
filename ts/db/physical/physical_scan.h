/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_DB_PHYSICAL_SCAN_H_
#define TS_DB_PHYSICAL_SCAN_H_

#include "physical_plan.h"

namespace ts {
namespace db_physical {
	class full_table_scan : public physical_plan
	{
	public:
		full_table_scan();
		~full_table_scan();
		virtual bool prelude();
		virtual bool execute(void *);
		virtual bool postlude();
	};

	class index_scan : public physical_plan
	{
	public:
		index_scan();
		~index_scan();
		virtual bool prelude();
		virtual bool execute(void *);
		virtual bool postlude();
	};
}
}

#endif /* TS_DB_PHYSICAL_SCAN_H_ */
