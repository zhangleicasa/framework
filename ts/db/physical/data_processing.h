//#ifndef TS_PHYSICAL_DATA_PROCESSING_H_
//#define TS_PHYSICAL_DATA_PROCESSING_H_
//
//#include "hash_table.h"
//
//#include <string>
//#include <stdio.h>
//#include <sys/types.h>
//#include <sys/stat.h>
//#include <fcntl.h>
//#include <stdlib.h>
//#include <errno.h>
//#include <sys/mman.h>
//#include <unistd.h>
//#include <map>
//
//using namespace ts::common;
//
//typedef struct processing_data
//{
//	char partid[10];
//	char clientid[10];
//	char instrumentid[30];
//	void debug()
//	{
//		printf("partid: %s, clientid: %s, instrumentid: %s\n\n\n",partid, clientid, instrumentid);
//	}
//}processing_data_;
//
//struct compare_key
//{
//	bool operator()(const processing_data_ &a, const processing_data_ &b) const
//	{
//		int b1 = strcmp(a.partid, b.partid);
//		if(b1 != 0)
//			return b1 < 0;
//		int b2 = strcmp(a.clientid, b.clientid);
//		if(b2 != 0)
//			return b2 < 0;
//		int b3 = strcmp(a.instrumentid, b.instrumentid);
//		if(b3 != 0)
//			return b3 < 0;
//		return false;
//	}
//};
//
//typedef struct l_s_position
//{
//	int long_position;
//	int short_position;
//	l_s_position &operator +=(l_s_position &p)
//	{
//		this->long_position += p.long_position;
//		this->short_position += p.short_position;
//		return *this;
//	}
//
//}l_s_position_;
//
//static void parse_(char *buffer, processing_data_ &trade, int &l, int &s)
//{
//	char volume[8] = "";
//	char offsetflag[2] = "";
//	char *p = buffer;
//	trade.partid[0] = p[45];
//	trade.partid[1] = p[46];
//	trade.partid[2] = p[47];
//	trade.partid[3] = p[48];
//
//	trade.clientid[0] = p[50];
//	trade.clientid[1] = p[51];
//	trade.clientid[2] = p[52];
//	trade.clientid[3] = p[53];
//	trade.clientid[4] = p[54];
//	trade.clientid[5] = p[55];
//	trade.clientid[6] = p[56];
//	trade.clientid[7] = p[57];
//
//	char *q = buffer + 68;
//	int offset = 0;
//	while(*q != ',')
//	{
//		trade.instrumentid[offset++] = *q;
//		q++;
//	}
//
//	q++;
//	offsetflag[0] = *q;
//	q++;
//
//	int o = 11;
//	while(*q != '\n')
//	{
//		if(*q == ',' && ++o == 14)
//			break;
//		q++;
//	}
//
//	q++;
//	offset = 0;
//	while(*q != ',')
//	{
//		volume[offset++] = *q;
//		q++;
//	}
//
//	if(p[29] == '0' && offsetflag[0] == '0')
//		l = atoi(volume);
//	else if(p[29] == '1' && offsetflag[0] == '0')
//		s = atoi(volume);
//	else if(p[29] == '0' && offsetflag[0] == '1')
//		l = -atoi(volume);
//	else
//		s = -atoi(volume);
//
//}
//
//class data_processing
//{
//public:
//	data_processing(const char *in, const char *out);
//	virtual ~data_processing();
//	virtual void open();
//	virtual void next();
//private:
//	std::string   in_file_;
//	std::string   out_file_;
//	processing_data_ data_;
//	std::map<processing_data_, l_s_position, compare_key> m_trade_;
//};
//
//#endif /* TS_PHYSICAL_DATA_PROCESSING_H_ */
