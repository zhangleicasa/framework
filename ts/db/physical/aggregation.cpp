#include "aggregation.h"

#define BUCKET_CHAIN_SIZE 4096
#define OFFSET 6

namespace ts {
namespace physical {

	void add_long_pos(int &l, int &s, int vol)
	{
		l = vol;
	}
	void del_long_pos(int &l, int &s, int vol)
	{
		l = -vol;
	}
	void add_short_pos(int &l, int &s, int vol)
	{
		s = vol;
	}
	void del_short_pos(int &l, int &s, int vol)
	{
		s = -vol;
	}
	void (*pos_func[64][64])(int &l, int &s, int vol);

	static char *parse(char *buffer, i_trade &trade, int &l, int &s)
	{

		int o = 11;
		int offset = 0;
		char *p = buffer;
		char *q = NULL;
		char volume[8] = "";
		char offsetflag;

		char direaction = p[29];

		trade.tradeid = 0;
		for(int i = 16; i < 28; i++)
		{
			if(p[i] != ' ')
				trade.tradeid = 10 * trade.tradeid + (p[i] - '0');
		}
		trade.partid = 0;
		for(int i = 44; i < 48; i++)
		{
			trade.partid = 10 * trade.partid + (p[i] - '0');
		}
		trade.clientid = 0;
		for(int i = 49; i < 57; i++)
		{
			trade.clientid = 10 * trade.clientid + (p[i] - '0');
		}
		trade.product = product[buffer[67]][buffer[68]];

		trade.instrument = 0;
		trade.ins_offset = p + 67;
		trade.ins_len = offsets[buffer[68]];
		int endpos = 67 + trade.ins_len;
		for(int i = 69; i < endpos; i++)
		{
			trade.instrument = 10 * trade.instrument + (p[i] - '0');
		}

#ifndef WIN32
		rte_memcpy(trade.insname, trade.ins_offset, trade.ins_len);
#endif	
		trade.insname[trade.ins_len] = '\0';

		offsetflag = p[68+offsets[buffer[68]]];
		char *v = strchr(&(p[72+offsets[buffer[68]]]), ',');

		int vol = 0;
		++v;
		while(*v != ',')
			vol = 10 * vol + (*v++ - '0');
		(*pos_func[direaction][offsetflag])(l, s, vol);

		return v;
	}

	aggregation::aggregation(const char *path, const char *cp, const char *trade)
		:path_(path), cp_path_(cp), trade_path_(trade), trade_p_(NULL) { }

	aggregation::~aggregation() {}

	bool aggregation::open()
	{
		offsets['O'] = 13;
		offsets['F'] = 6;
		offsets['1'] = 5; // T1701

		product['I']['F'] = 1;
		product['I']['O'] = 2;
		product['T']['1'] = 3;
		product['T']['F'] = 4;

		pos_func['0']['0'] = &add_long_pos;
		pos_func['0']['1'] = &del_long_pos;
		pos_func['1']['0'] = &add_short_pos;
		pos_func['1']['1'] = &del_short_pos;

		int fd = 0;
#ifndef WIN32
		if((fd = ::open(path_.c_str(), O_RDWR)) == -1)
		{
			common::xlog::log(common::LOG_ERROR, "open file failed.\n");
			exit(1);
		}
#endif
		int r = 0;
		struct stat st;
		if((r = fstat(fd, &st)) == -1)
		{
			common::xlog::log(common::LOG_ERROR, "get file size failed.\n");
			exit(1);
		}
		uint64_t len = st.st_size;
#ifndef WIN32
		trade_p_ = (char *)::mmap(NULL, len, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
		if(trade_p_ == NULL || trade_p_ == (void *) - 1)
		{
			common::xlog::log(common::LOG_ERROR, "mmap failed.\n");
			exit(1);
		}

		if(-1 == madvise(trade_p_, len, MADV_WILLNEED | MADV_SEQUENTIAL))
		{
			common::xlog::log(common::LOG_ERROR, "madvise failed.\n");
			exit(1);
		}


		//create SelfTrade.csv
		FILE *self_trade_fp = NULL;
		if((self_trade_fp = ::fopen("./SelfTrade.csv", "w+")) == NULL)
		{
			common::xlog::log(common::LOG_ERROR, "create SelfTrade file failed.\n");
			exit(1);
		}
		fwrite("TradeID\n", 8, 1, self_trade_fp);

		l_s_position_ position;
		char *begin, *end = NULL;
		begin = trade_p_;
		i_trade temp;
		while(begin != NULL)
		{
			position.reset_position();
			begin = parse(begin, temp, position.long_position, position.short_position);
//			temp.debug();
			m_trade_l_[temp] += position;
			if(temp.compare(i_trade_)) {
				fprintf(self_trade_fp, "%12d\n", temp.tradeid);
			}
			i_trade_ = temp;
			end = strchr(begin, '\n');
			begin = end + 1;
			if(begin - trade_p_ >= len)
				break;


		}

		fclose(self_trade_fp);
#endif
		return true;
	}

	bool aggregation::next(void *tuple)
	{//select partid, clientid, instrumentid, long_position, short_position
	 //from trade groupy by partid, clientid, instrumentid;
		//std::map<i_trade, l_s_position_, compare_key>::iterator itr = m_trade_l_.begin();
		//char buf[128];
		//for(; itr != m_trade_l_.end(); ++itr)
		//{
		//	snprintf(buf, 128, "%d,%08d,%s,%d,%d", (itr->first).partid, (itr->first).clientid, (itr->first).insname,
		//		(itr->second).long_position, (itr->second).short_position);
		//	printf("%s\n", buf);
		//}
		save_client_position();
		save_client_position_2();
		return false;
	}

	bool aggregation::close()
	{
#ifndef WIN32
		munmap(trade_p_, 4096);
#endif		
		return true;
	}

	void aggregation::save_client_position_2()
	{
		int fd = 0;
#ifndef WIN32
		if((fd = ::open("./ClientPosition2.csv", O_CREAT|O_RDWR, 00600)) == -1)
		{
			common::xlog::log(common::LOG_ERROR, "open file failed.\n");
			exit(1);
		}
#endif		
		uint64_t file_size = 1024*9;
#ifndef WIN32
		ftruncate(fd, file_size);
#endif		

#ifndef WIN32
		char *output = (char *)::mmap(NULL, file_size, PROT_WRITE, MAP_SHARED, fd, 0);
		if(output == NULL || output == (void *) - 1)
		{
			common::xlog::log(common::LOG_ERROR, "mmap failed.\n");
			exit(1);
		}
#endif		
		uint64_t len = 0;
		std::map<i_trade, l_s_position_, compare_key>::iterator itr = m_trade_l_.begin();
		char buf[128];
#ifndef WIN32
		for(; itr != m_trade_l_.end(); ++itr)
		{
			char *p = output + len;
			int buf_len = snprintf(buf, 128, "%d,%08d,%s,%d,%d\n", (itr->first).partid, (itr->first).clientid, (itr->first).insname,
				(itr->second).long_position, (itr->second).short_position);
			printf("%s", buf);
			memcpy(p, buf, buf_len + 1);
			len += buf_len;
		}

		munmap(output, file_size);
		ftruncate(fd, len);
		::close(fd);
#endif	
	}

	void aggregation::save_client_position()
	{
		FILE *fp = NULL;
#ifndef WIN32
		if((fp = ::fopen("./ClientPosition.csv", "w+")) == NULL)
		{
			common::xlog::log(common::LOG_ERROR, "create ClientPosition file failed.\n");
			exit(1);
		}

		fwrite("TradingDay,ParticipantID,ClientID,InstrumentID,LongPosition,ShortPosition\n", 74, 1, fp);
		std::map<i_trade, l_s_position_, compare_key>::iterator itr = m_trade_l_.begin();
		char buf[128];
		for(; itr != m_trade_l_.end(); ++itr)
		{
			int len = snprintf(buf, 128, "%d,%08d,%s,%d,%d\n", (itr->first).partid, (itr->first).clientid, (itr->first).insname,
				(itr->second).long_position, (itr->second).short_position);
			//printf("%s", buf);
			fwrite(buf, 1, len, fp);
		}
		fclose(fp);
#endif	
	}

	void aggregation::save_temp_file()
	{}

}
}
