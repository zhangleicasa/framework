/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_DB_PHYSICAL_ORDERMATCH_H_
#define TS_DB_PHYSICAL_ORDERMATCH_H_

#include "x_log.h"
#include "ioperator.h"

#include <stdio.h>
#include <string>

namespace ts {
namespace physical {
	class ordermatch: public ioperator
	{
	public:
		ordermatch(const char *path);
		virtual ~ordermatch();
		virtual bool open();
		virtual bool next(void *tuple);
		virtual bool close();
	private:
		std::string   path_;
		FILE         *file_;
	};

}
}

#endif /* TS_DB_PHYSICAL_ORDERMATCH_H_ */
