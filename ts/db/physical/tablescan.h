/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_DB_PHYSICAL_TABLESCAN_H_
#define TS_DB_PHYSICAL_TABLESCAN_H_

#include "x_log.h"
#include "ioperator.h"
#include "time_helper.h"

#include <stdio.h>
#include <string>

namespace ts {
namespace physical {
	typedef struct p_inc_trade
	{
		char tradeid[13];
		char direaction[2];
		char partid[11];
		char clientid[11];
		char instrumentid[31];
		int  volume;
	}p_inc_trade;

	typedef struct v_p_inc_trade
	{
		char tradeid[13];
		char direaction[2];
		char partid[11];
		char clientid[11];
		char instrumentid[31];
		char volume[4];

		void debug()
		{
			printf("tradeid: %s, direction: %s, partid: %s, clientid: %s, instrumentid: %s, volume: %s\n\n\n",
					tradeid, direaction, partid, clientid, instrumentid, volume);
		}
	}v_p_inc_trade;

	class tablescan: public ioperator
	{
	public:
		tablescan(const char *r_path, const char *w_path);
		virtual ~tablescan();
		virtual bool open();
		virtual bool open1();
		virtual bool next(void *tuple);
		virtual bool close();
	private:
		std::string     r_path_;
		std::string     w_path_;
		FILE           *r_file_;
		FILE           *w_file_;
		p_inc_trade     inc_trade_;
		v_p_inc_trade   v_inc_trade_;
	};

}
}

#endif /* TS_DB_PHYSICAL_TABLESCAN_H_ */
