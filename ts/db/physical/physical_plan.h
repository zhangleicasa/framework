/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_DB_PHYSICAL_PLAN_H_
#define TS_DB_PHYSICAL_PLAN_H_

namespace ts {
namespace db_physical {
	class physical_plan
	{
	public:
		virtual ~physical_plan() {}
		virtual bool prelude() = 0;
		virtual bool execute(void *) = 0;
		virtual bool postlude() = 0;
	};
}
}

#endif /* TS_DB_PHYSICAL_PLAN_H_ */
