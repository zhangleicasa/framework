/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_DB_PHYSICAL_AGGREGATION_H_
#define TS_DB_PHYSICAL_AGGREGATION_H_

#include "x_log.h"
#include "ioperator.h"
#include "rte_memcpy.h"
#include "log_helper.h"
#include "time_helper.h"
#include "hash_function.h"
#include "public.h"

#include <algorithm>
#include <string>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <fcntl.h>

#include <map>

using namespace std;
namespace ts {
namespace physical {

	static int offsets[256] = {0};
	static int product[256][256] = {0};

	typedef struct l_s_position
	{
		int long_position;
		int short_position;
		l_s_position &operator +=(l_s_position &p)
		{
			this->long_position  += p.long_position;
			this->short_position += p.short_position;
			return *this;
		}
		l_s_position &operator -=(l_s_position &p)
		{
			this->long_position  -= p.long_position;
			this->short_position -= p.short_position;
			return *this;
		}
		l_s_position &operator =(l_s_position &p)
		{
			this->long_position  = p.long_position;
			this->short_position = p.short_position;
			return *this;
		}
		inline void reset_position()
		{
			this->long_position  = 0;
			this->short_position = 0;
		}
	}l_s_position_;

	typedef struct i_trade
	{
		uint32_t partid;
		uint32_t clientid;
		uint32_t product;
		uint32_t instrument;
		uint32_t tradeid;

		l_s_position_ ls_pos;

		char *ins_offset;
		uint32_t ins_len;
		char insname[16];

		i_trade() : partid(0), clientid(0), product(0), instrument(0), ins_offset(NULL), ins_len(0), tradeid(0) {}

		bool compare(i_trade &p)
		{
			if(partid==p.partid && clientid==p.clientid /*&& product==p.product && instrument==p.instrument && tradeid==p.tradeid*/)
				return true;
			else
				return false;
		}

		void clear()
		{
			partid = 0;
		}

		i_trade& operator =(i_trade &p)
		{
			this->partid  = p.partid;
			this->clientid = p.clientid;
			this->product = p.product;
			this->instrument = p.instrument;
			this->tradeid = p.tradeid;
			return *this;
		}

		void debug()
		{
			printf("partid: %d, clientid: %d, tradeid: %d\n\n\n",partid, clientid, tradeid);
		}
	}i_trade;

	typedef struct compare_key
	{
		bool operator()(const i_trade &a, const i_trade &b) const
		{
			if(a.partid != b.partid)
				return a.partid < b.partid;
			if(a.clientid != b.clientid)
				return a.clientid < b.clientid;
			if(a.product != b.product)
				return a.product < b.product;
			if(a.instrument != b.instrument)
				return a.instrument < b.instrument;
			return false;
		}
	}compare_key;

	class aggregation: public ioperator
	{
	public:
		aggregation(const char *path, const char *cp = "", const char *trade = "");
		virtual ~aggregation();
		virtual bool open();
		virtual bool next(void *tuple);
		virtual bool close();

	private:
		void save_client_position();
		void save_client_position_2();
		void save_temp_file();

	private:
		uint32_t temp_id_;
		uint32_t interval_;
		std::string            path_;
		std::string            cp_path_;
		std::string            trade_path_;
		i_trade                i_trade_;
		char *                 trade_p_;
		std::map<i_trade, l_s_position_, compare_key> m_trade_l_;
	};

}
}

#endif /* TS_DB_PHYSICAL_AGGREGATION_H_ */
