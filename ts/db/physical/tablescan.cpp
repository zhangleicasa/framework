#include "tablescan.h"

namespace ts {
namespace physical {

	static char line_[1024]   = "";
	static char buffer_[1024] = "";

	static void parse(char *buffer, v_p_inc_trade &trade)
	{
		int count = 0;
		int offset= 0;
		char *p = buffer;
		while(*p != '\0')
		{
			char c = *p;
			if(c == ',')
			{
				count ++;
				offset = 0;
				p++;
				continue;
			}
			switch(count)
			{
			case 3:
				trade.tradeid[offset++] = c;
				break;
			case 4:
				trade.direaction[offset++] = c;
				break;
			case 6:
				trade.partid[offset++] = c;
				break;
			case 7:
				trade.clientid[offset++] = c;
				break;
			case 10:
				trade.instrumentid[offset++] = c;
				break;
			case 14:
				trade.volume[offset++] = c;
				break;
			default:
				break;
			};
			p++;
		}
	}

	static void parse_LiuMang(char *buffer, v_p_inc_trade &trade)
	{
		char *p = buffer;
		trade.tradeid[0] = p[16];
		trade.tradeid[1] = p[17];
		trade.tradeid[2] = p[18];
		trade.tradeid[3] = p[19];
		trade.tradeid[4] = p[20];
		trade.tradeid[5] = p[21];
		trade.tradeid[6] = p[22];
		trade.tradeid[7] = p[23];
		trade.tradeid[8] = p[24];
		trade.tradeid[9] = p[25];
		trade.tradeid[10] = p[26];
		trade.tradeid[11] = p[27];

		trade.direaction[0] = p[29];

		trade.partid[0] = p[44];
		trade.partid[1] = p[45];
		trade.partid[2] = p[46];
		trade.partid[3] = p[47];

		trade.clientid[0] = p[49];
		trade.clientid[1] = p[50];
		trade.clientid[2] = p[51];
		trade.clientid[3] = p[52];
		trade.clientid[4] = p[53];
		trade.clientid[5] = p[54];
		trade.clientid[6] = p[55];
		trade.clientid[7] = p[56];

		char *q = buffer + 67;
		int offset = 0;
		while(*q != ',')
		{
			trade.instrumentid[offset++] = *q;
			q++;
		}

		int o = 10;
		while(*q != '\0')
		{
			if(*q == ',' && ++o == 14)
				break;
			q++;
		}

		q++;
		offset = 0;
		while(*q != ',')
		{
			trade.volume[offset++] = *q;
			q++;
		}

	}



	tablescan::tablescan(const char *r_path, const char *w_path)
		:r_path_(r_path), w_path_(w_path), r_file_(NULL), w_file_(NULL) {}

	tablescan::~tablescan() {}

	bool tablescan::open()
	{
		if((r_file_ = fopen(r_path_.c_str(), "r")) == NULL)
		{
			common::xlog::log(common::LOG_ERROR, "open file[%s] failed.\n", r_path_.c_str());
			exit(1);
		}

		if((w_file_ = fopen(w_path_.c_str(), "w")) == NULL)
		{
			common::xlog::log(common::LOG_ERROR, "open file[%s] failed.\n", w_path_.c_str());
			exit(1);
		}

		while(fgets(line_, 1024, r_file_) != NULL)
		{
			memset(&inc_trade_, 0x00, sizeof(p_inc_trade));

			int count = sscanf(line_, "%*[^,],%*[^,],%*[^,],%[^,],%[^,],%*[^,],%[^,],%[^,],%*[^,],%*[^,],%[^,],"//11
			"%*[^,],%*[^,],%*[^,],%d,%*[^,],%*[^,],%*[^,],%*[^,],%*[^,],%*[^,],%*[^,],%*[^,],%*[^,],%*[^,],%*[^,],",
			inc_trade_.tradeid, inc_trade_.direaction, inc_trade_.partid, inc_trade_.clientid, inc_trade_.instrumentid, &(inc_trade_.volume));

			fwrite(&inc_trade_, sizeof(p_inc_trade), 1, w_file_);
		}
		return true;
	}

	bool tablescan::open1()
	{
		if((r_file_ = fopen(r_path_.c_str(), "r")) == NULL)
		{
			common::xlog::log(common::LOG_ERROR, "open file[%s] failed.\n", r_path_.c_str());
			exit(1);
		}

		if((w_file_ = fopen(w_path_.c_str(), "w")) == NULL)
		{
			common::xlog::log(common::LOG_ERROR, "open file[%s] failed.\n", w_path_.c_str());
			exit(1);
		}

		while(fgets(line_, 1024, r_file_) != NULL)
		{
			memset(&v_inc_trade_, 0x00, sizeof(v_p_inc_trade));
			parse_LiuMang(line_, v_inc_trade_);
		}
		return true;
	}

	bool tablescan::next(void *tuple)
	{

		return false;
	}

	bool tablescan::close()
	{
		fclose(r_file_);
		fclose(w_file_);
		return true;
	}

}
}
