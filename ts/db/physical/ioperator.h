/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_DB_PHYSICAL_IOPERATOR_H_
#define TS_DB_PHYSICAL_IOPERATOR_H_

namespace ts {
namespace physical {
	class ioperator
	{
	public:
		virtual ~ioperator() {}
		virtual bool open() = 0;
		virtual bool next(void *tuple) = 0;
		virtual bool close() = 0;
	};
}
}

#endif /* TS_DB_PHYSICAL_IOPERATOR_H_ */
