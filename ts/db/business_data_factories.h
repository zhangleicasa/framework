/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_DB_BUSINESS_DATA_FACTORIES_H_
#define TS_DB_BUSINESS_DATA_FACTORIES_H_

#include "allocator.h"
#include "mem_table.h"

namespace ts {
namespace db {
	class data_factory {
	public:
		data_factory(int unit, int size, ts::mem::allocator *alcr);
		virtual ~data_factory();
		void *create_object();
		void delete_object(const void *object);
		void print_memory_layout() { mem_table_->print_binary_info(); }
	private:
		ts::mem::mem_table *mem_table_;
	};

	template <typename data_struct>
	class business_data_factory : public data_factory
	{
	public:
		business_data_factory(int unit, int size, ts::mem::allocator *alcr);
		~business_data_factory();
		data_struct* inernal_insert(data_struct* data);
		data_struct* inernal_delete(data_struct* data);
	};

	template <typename data_struct>
	business_data_factory<data_struct>::business_data_factory(int unit, int size, ts::mem::allocator *alcr)
		: data_factory(unit, size, alcr)
	{}

	template <typename data_struct>
	business_data_factory<data_struct>::~business_data_factory()
	{}

	template <typename data_struct>
	data_struct*
	business_data_factory<data_struct>::inernal_insert(data_struct* data)
	{
		data_struct *p = (data_struct *)create_object();
		memcpy(p, data, sizeof(data_struct));
		return p;
	}

	template <typename data_struct>
	data_struct*
	business_data_factory<data_struct>::inernal_delete(data_struct* data)
	{
		delete_object(data);
		return data;
	}

}
}

#endif /* TS_DB_BUSINESS_DATA_FACTORIES_H_ */
