/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_DB_TYPE_BANDING_EVAL_H_
#define TS_DB_TYPE_BANDING_EVAL_H_

#include "public.h"
#include "type_cast_func.h"

namespace ts {
namespace expr {

	enum expression_type { t_expr_const, t_expr_column, t_expr_cal, t_expr_cmp, t_expr_func };

	enum operator_type {t_add, t_minus, t_large_then, t_less_than};

	typedef struct type_func_info
	{
		int expressions_size_;
		void* expression_[4];
		void* result_;
	}type_func_info;

	typedef void (*type_banding_eval_func)(type_func_info &info);

	class type_banding_eval
	{
	public:
		static type_banding_eval_func type_banding_eval_func_map[DATA_TYPE_SERIES][OPERATOR_TYPE_SERIES];
	};

	/** integer related function which calculate arguments info. **/
	void _int_func_add_(type_func_info &info);
	void _int_func_minus_(type_func_info &info);

	/** float related function which calculate arguments info. **/
	void _float_func_add_(type_func_info &info);
	void _float_func_float_(type_func_info &info);
	void _float_func_bool_large(type_func_info &info);
	void _float_func_bool_less(type_func_info &info);

	/** double related function which calculate arguments info. **/
	void _double_func_add_(type_func_info &info);
	void _double_func_minus_(type_func_info &info);

	/** aggregation func. **/

	/** init function with calling the function points. **/
	void init_type_banding_eval_func();
}
}

#endif /* TS_DB_TYPE_BANDING_EVAL_H_ */
