/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_DB_COLUMN_EXPRESSION_H_
#define TS_DB_COLUMN_EXPRESSION_H_

#include "public.h"
#include "expression.h"

namespace ts {
namespace expr {
	class column_expression : public expression
	{
	public:
		int table_id_;
		int column_id_;
		std::string table_name_;
		std::string column_name_;
		entity_desc *entity_;//todo: delete entity.
	public:
		//todo: to support the alias then.
		column_expression(data_type atype, const char *table, const char *column, entity_desc *entity);
		virtual ~column_expression();
		virtual void output();
		virtual void init_expression_execution(data_type return_type);
		//todo: strong schema.
		virtual void* evaluate(entity_desc *entity);
	};

}
}

#endif /* TS_DB_COLUMN_EXPRESSION_H_ */
