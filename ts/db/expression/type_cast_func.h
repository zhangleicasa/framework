/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_DB_TYPE_CAST_FUNC_H_
#define TS_DB_TYPE_CAST_FUNC_H_

#include "public.h"

namespace ts {
namespace expr {
	enum data_type {t_smallInt, t_int, t_u_long, t_float, t_double, t_string,
		t_decimal, t_boolean, };

	typedef void *(*type_cast_func)(void *value, void *tovalue);

	class type_cast
	{
	public:
		static type_cast_func type_cast_func_map[DATA_TYPE_SERIES][DATA_TYPE_SERIES];
	};

	/** error cast **/
	void *_error_cast(void *value, void *tovalue);

	/** integer cast to other types. **/
	void *_int_to_float(void *value, void *tovalue);
	void *_int_to_double(void *value, void *tovalue);

	/** string cast to other types. **/
	void *_string_to_int(void *value, void *tovalue);
	void *_string_to_float(void *value, void *tovalue);

	/** float cast to other types. **/
	void *_float_to_float(void *value, void *tovalue);

	/** bool cast to bool type. **/
	void *_bool_to_bool(void *value, void *tovalue);

	void init_type_cast_func();
}
}

#endif /* TS_DB_TYPE_CAST_FUNC_H_ */
