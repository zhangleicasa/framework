#include "type_cast_func.h"

namespace ts {
namespace expr {

	type_cast_func type_cast::type_cast_func_map[DATA_TYPE_SERIES][DATA_TYPE_SERIES];

	void *_error_cast(void *value, void *tovalue)
	{
		printf("the type cast can not be support.\n");
		return NULL;
	}

	void *_int_to_float(void *value, void *tovalue)
	{//todo: consider that error situation.
		*(float *)tovalue = *(int *)value;
		return tovalue;
	}

	void *_int_to_double(void *value, void *tovalue)
	{//todo: consider that error situation.
		*(double *)tovalue = *(int *)value;
		return tovalue;
	}

	void *_string_to_int(void *value, void *tovalue)
	{
		*(int *)tovalue = atoi((char *)value);
		return tovalue;
	}

	void *_string_to_float(void *value, void *tovalue)
	{
		*(float *)tovalue = atof((char *)value);
		return tovalue;
	}

	void *_float_to_float(void *value, void *tovalue)
	{
		*(float *)tovalue = *(float *)value;
		return tovalue;
	}

	void *_bool_to_bool(void *value, void *tovalue)
	{
		*(bool *)tovalue = *(bool *)value;
		return tovalue;
	}

	void init_type_cast_func()
	{
		//integer
		type_cast::type_cast_func_map[t_int][t_float] = _int_to_float;
		type_cast::type_cast_func_map[t_int][t_double] = _int_to_double;

		//float
		type_cast::type_cast_func_map[t_float][t_float] = _float_to_float;

		//string
		type_cast::type_cast_func_map[t_string][t_int] = _string_to_int;
		type_cast::type_cast_func_map[t_string][t_float] = _string_to_float;

		//bool
		type_cast::type_cast_func_map[t_boolean][t_boolean] = _bool_to_bool;

		//error
		type_cast::type_cast_func_map[t_float][t_int] = _error_cast;
	}
}
}
