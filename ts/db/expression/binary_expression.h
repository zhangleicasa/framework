/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_DB_BINARY_EXPRESSION_H_
#define TS_DB_BINARY_EXPRESSION_H_

#include "public.h"
#include "expression.h"
#include "type_cast_func.h"
#include "type_banding_eval.h"

namespace ts {
namespace expr{
	class binary_expression : public expression
	{
	public:
		expression *left_;
		expression *right_;
		operator_type op_type_;//+ - * / > <
		type_banding_eval_func type_eval_func_; //const expression has no type_eval_func.
	public:
		binary_expression(data_type atype, data_type gtype, expression_type etype,
				operator_type ptype, expression *left, expression *right);
		virtual ~binary_expression();
		virtual void output();
		virtual void init_expression_execution(data_type retrun_type);
		virtual void* evaluate(entity_desc *entity);
	};

}
}

#endif /* TS_DB_BINARY_EXPRESSION_H_ */
