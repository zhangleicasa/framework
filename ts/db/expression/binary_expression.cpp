#include "binary_expression.h"

namespace ts {
namespace expr {
	binary_expression::binary_expression(data_type atype, data_type gtype, expression_type etype,
		operator_type ptype, expression *left, expression *right): expression(atype, gtype, etype),
		op_type_(ptype), left_(left), right_(right), type_eval_func_(NULL) {}

	binary_expression::~binary_expression()
	{
		free(value_);
	}

	void binary_expression::output()
	{
		left_->output();
		right_->output();
		printf(" + \n");
	}

	void binary_expression::init_expression_execution(data_type return_type)
	{
		return_type_ = return_type;
		left_->init_expression_execution(get_type_);
		right_->init_expression_execution(get_type_);
		value_size_ = std::max(left_->value_size_, right_->value_size_);
#ifndef WIN32	
		value_ = memalign(CACHE_LINE_SIZE, value_size_);
#endif
		type_eval_func_ = type_banding_eval::type_banding_eval_func_map[get_type_][op_type_];
		type_cast_func_ = type_cast::type_cast_func_map[actual_type_][return_type_];
	}

	void* binary_expression::evaluate(entity_desc *entity)
	{
		type_func_info info;
		info.expressions_size_ = 2;
		info.expression_[0] = left_->evaluate(entity);
		info.expression_[1] = right_->evaluate(entity);
		info.result_ = value_;
		type_eval_func_(info);
		return type_cast_func_(info.result_, value_);
	}

}
}
