/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_DB_EXPRESSION_H_
#define TS_DB_EXPRESSION_H_

#include "public.h"
#include "type_cast_func.h"
#include "temp_class_impl.h"                  //temp class implemetation.
#include "type_banding_eval.h"

namespace ts {
namespace expr {
	class expression
	{
	public:
		int value_size_;
		void *value_;
		data_type actual_type_;               //actual type of this expression.
		data_type get_type_;                  //get type of this expression.
		data_type return_type_;               //return type which cast to.
		expression_type expression_type_;
		type_cast_func type_cast_func_;
	public:
		expression(data_type atype, data_type gtype, expression_type etype)
			:value_size_(0), value_(NULL), type_cast_func_(NULL),
			 actual_type_(atype), get_type_(gtype), return_type_(atype), expression_type_(etype){}
		virtual ~expression() {}
		/** ouput the expression identity. **/
		virtual void output() {}
		/** before evaluate(), cal value_size, allocate space. **/
		virtual void init_expression_execution(data_type retrun_type) {}
		/** evaluate expression tree by recurse. **/
		virtual void* evaluate(entity_desc *entity) { return NULL; }
		/** evaluate expression tree by recurse only by aggregation expression. **/
		virtual void* evaluate(entity_desc *entity, void *last_value) { return NULL; }
	};

}
}

#endif /* TS_DB_EXPRESSION_H_ */
