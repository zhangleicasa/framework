#include "const_expression.h"

namespace ts {
namespace expr {
	const_expression::const_expression(data_type atype, const char *value)
		:expression(atype, atype, t_expr_const), const_value_(value) {}

	const_expression::~const_expression()
	{
		free(value_);
	}

	void const_expression::output()
	{
		printf(" %s \n", const_value_.c_str());
	}

	void const_expression::init_expression_execution(data_type return_type)
	{//todo: then check the function.
		return_type_ = return_type;
		value_size_ = std::max((int)const_value_.length(), CACHE_LINE_SIZE);
#ifndef WIN32
		value_ = ::memalign(CACHE_LINE_SIZE, value_size_);
#endif
		::strcpy((char *)value_, const_value_.c_str());
		type_cast_func_ = type_cast::type_cast_func_map[t_string][return_type_];
		type_cast_func_(value_, value_);
	}

	void* const_expression::evaluate(entity_desc *entity)
	{
		return value_;
	}

}
}

