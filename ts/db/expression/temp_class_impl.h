/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_DB_TEMP_CLASS_IMPL_H_
#define TS_DB_TEMP_CLASS_IMPL_H_

#include "buffer.h"
#include "schema.h"
#include "column_type.h"

namespace ts {
namespace expr {
	class entity_desc
	{
	public:
		entity_desc(unsigned event_id, ts::mem::buffer *p, data::schema *s)
			:event_id_(event_id), buffer_(p), psh(s) {}
	public:
		data::schema *psh;
		ts::mem::buffer *buffer_;
	private:
		unsigned event_id_;
	};
}
}

#endif /* TS_DB_TEMP_CLASS_IMPL_H_ */
