#include "column_expression.h"

namespace ts {
namespace expr {
	column_expression::column_expression(data_type atype, const char *table, const char *column, entity_desc *entity)
		: expression(atype, atype, t_expr_column), table_name_(table), column_name_(column), entity_(entity), column_id_(0), table_id_(0)
	{
		column_id_ = (strcmp(column_name_.c_str(), "limit_price") == 0) ? 1 : 0;
	}

	column_expression::~column_expression()
	{
		free(value_);
	}

	void column_expression::output()
	{
		printf(" %s.%s \n", table_name_.c_str(), column_name_.c_str());
	}

	void column_expression::init_expression_execution(data_type return_type)
	{
		return_type_ = return_type;
		value_size_  = entity_->psh->columns_[column_id_].column_size_;
#ifndef WIN32
		value_       = memalign(CACHE_LINE_SIZE, value_size_);
#endif
		type_cast_func_ = type_cast::type_cast_func_map[actual_type_][return_type_];
	}

	void* column_expression::evaluate(entity_desc *entity)
	{
		void *result = entity->psh->get_column_address(column_id_, entity->buffer_->address());
		return type_cast_func_(result, value_);
	}

}
}

