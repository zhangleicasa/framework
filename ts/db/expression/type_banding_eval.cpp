#include "type_banding_eval.h"

namespace ts {
namespace expr {
	type_banding_eval_func type_banding_eval::type_banding_eval_func_map[DATA_TYPE_SERIES][OPERATOR_TYPE_SERIES];

	void _int_func_add_(type_func_info &info)
	{
		assert(info.expressions_size_ == 2);
		*(int *)info.result_ = *(int *)info.expression_[0] + *(int *)info.expression_[1];
	}

	void _int_func_minus_(type_func_info &info)
	{
		assert(info.expressions_size_ == 2);
		*(int *)info.result_ = *(int *)info.expression_[0] - *(int *)info.expression_[1];
	}

	void _float_func_add_(type_func_info &info)
	{
		assert(info.expressions_size_ == 2);
		*(float *)info.result_ = *(float *)info.expression_[0] + *(float *)info.expression_[1];
	}

	void _float_func_minus_(type_func_info &info)
	{
		assert(info.expressions_size_ == 2);
		*(float *)info.result_ = *(float *)info.expression_[0] - *(float *)info.expression_[1];
	}

	void _float_func_bool_large(type_func_info &info)
	{
		assert(info.expressions_size_ == 2);
		*(bool *)info.result_ = *(float *)info.expression_[0] > *(float *)info.expression_[1];
	}

	void _float_func_bool_less(type_func_info &info)
	{
		assert(info.expressions_size_ == 2);
		*(bool *)info.result_ = *(float *)info.expression_[0] < *(float *)info.expression_[1];
	}

	void _double_func_add_(type_func_info &info)
	{
		assert(info.expressions_size_ == 2);
		*(double *)info.result_ = *(double *)info.expression_[0] + *(double *)info.expression_[1];
	}

	void _double_func_minus_(type_func_info &info)
	{
		assert(info.expressions_size_ == 2);
		*(double *)info.result_ = *(double *)info.expression_[0] - *(double *)info.expression_[1];
	}

	void init_type_banding_eval_func()
	{
		//int
		type_banding_eval::type_banding_eval_func_map[t_int][t_add] = _int_func_add_;
		type_banding_eval::type_banding_eval_func_map[t_int][t_minus] = _int_func_minus_;

		//float
		type_banding_eval::type_banding_eval_func_map[t_float][t_add] = _float_func_add_;
		type_banding_eval::type_banding_eval_func_map[t_float][t_minus] = _float_func_minus_;
		type_banding_eval::type_banding_eval_func_map[t_float][t_large_then] = _float_func_bool_large;
		type_banding_eval::type_banding_eval_func_map[t_float][t_less_than] = _float_func_bool_less;

		//double
		type_banding_eval::type_banding_eval_func_map[t_double][t_add] = _double_func_add_;
		type_banding_eval::type_banding_eval_func_map[t_double][t_minus] = _double_func_minus_;
	}

}
}

