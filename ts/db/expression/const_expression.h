/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_DB_CONST_EXPRESSION_H_
#define TS_DB_CONST_EXPRESSION_H_

#include "public.h"
#include "expression.h"

namespace ts {
namespace expr {
	class const_expression : public expression
	{
	public:
		std::string const_value_;
	public:
		const_expression(data_type atype, const char *value);
		virtual ~const_expression();
		virtual void output();
		virtual void init_expression_execution(data_type return_type);
		virtual void* evaluate(entity_desc *entity);
	};

}
}

#endif /* TS_DB_CONST_EXPRESSION_H_ */
