/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_DB_NGINX_ACCESS_LOG_SCANNER_H_
#define TS_DB_NGINX_ACCESS_LOG_SCANNER_H_

#include "x_log.h"

#include <stdio.h>
#include <string>
#include <string.h>

#ifndef WIN32

#include <mysql.h>

namespace ts {
namespace db {
	typedef struct access_log_data
	{
		char remote_addr_[32];
		char dot_1_[8];
		char dot_2_[8];
		char time_local_[64];
		char request_[128];
		int  status_;
		int  body_bytes_send_;
		char http_user_agent_[64];
		char http_x_forwarded_for_[256];
	}access_log_data;

	class nginx_access_log_scanner
	{
	public:
		nginx_access_log_scanner(const char *access_log_path)
			:access_log_path_(access_log_path), current_location_(0), my_con_(NULL) {}
		~nginx_access_log_scanner() {}
		void init();
		void scan_access_log();
		void parser_access_log(char *line);
	private:
		MYSQL *my_con_;
		std::string access_log_path_;
		unsigned long current_location_;
	};
}
}

#endif

#endif /* TS_DB_NGINX_ACCESS_LOG_SCANNER_H_ */
