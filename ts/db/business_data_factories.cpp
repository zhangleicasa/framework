#include "business_data_factories.h"

namespace ts {
namespace db {
	data_factory::data_factory(int unit, int size, ts::mem::allocator *alcr)
	{
        mem_table_ = new ts::mem::mem_table(unit, size, alcr);
	}

	data_factory::~data_factory()
	{
		delete mem_table_;
	}

	void *data_factory::create_object()
	{
		return mem_table_->alloc();
	}

	void data_factory::delete_object(const void *object)
	{
		mem_table_->free(object);
	}

}
}
