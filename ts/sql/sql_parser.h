#ifndef PARSER_H_
#define PARSER_H_
#include <string>
#include <vector>
#include "ast_node.h"

using std::string;
using std::vector;


class Parser
{
public:
	Parser();
	Parser(string SQL_statement);
	virtual ~Parser();
	AstNode* GetRawAST();
private:
	AstNode* CreateRawAST(string SQL_statement);
	string SQL_statement_;
	AstNode* AST_root_;
};

#endif /* PARSER_H_ */
