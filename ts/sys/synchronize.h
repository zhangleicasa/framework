/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_SYS_H_
#define TS_SYS_H_

#include "public.h"

namespace ts {
namespace sys {
	class mutex
	{
	public:
		mutex()
		{
#ifndef WIN32
			pthread_mutex_init(&lock_, 0);
#endif
		};
		~mutex()
		{
#ifndef WIN32
			pthread_mutex_destroy(&lock_);
#endif
		};
		inline void acquire()
		{
#ifndef WIN32
			pthread_mutex_lock(&lock_);
#endif
		}
		inline void release()
		{
#ifndef WIN32
			pthread_mutex_unlock(&lock_);
#endif
		}
	private:
#ifndef WIN32
		pthread_mutex_t lock_;
#endif
	};

	class rw_lock
	{
	public:
		rw_lock()
		{
#ifndef WIN32
			pthread_rwlock_init(&rw_lock_, 0);
#endif
		}
		~rw_lock()
		{
#ifndef WIN32
			pthread_rwlock_destroy(&rw_lock_);
#endif
		}
		inline void r_lock()
		{
#ifndef WIN32
			pthread_rwlock_rdlock(&rw_lock_);
#endif
		}
		inline void w_lock()
		{
#ifndef WIN32
			pthread_rwlock_wrlock(&rw_lock_);
#endif
		}
		inline void release()
		{
#ifndef WIN32
			pthread_rwlock_unlock(&rw_lock_);
#endif
		}
	private:
#ifndef WIN32
		pthread_rwlock_t rw_lock_;
#endif
	};

	class event_semaphore
	{
	public:
		event_semaphore(int value = 1)
		{
#ifndef WIN32
			sem_init(&semaphore_, 0, value);
#endif
		}
		~event_semaphore()
		{
#ifndef WIN32
			sem_destroy(&semaphore_);
#endif
		}
		inline bool unlock()
		{
#ifndef WIN32
			return sem_post(&semaphore_) == 0 ? true : false;
#else
			return true;
#endif
		}
		inline bool time_wait(int micro_sec)
		{
#ifndef WIN32
			struct timespec ts_time;
			clock_gettime(CLOCK_REALTIME, &ts_time);
			ts_time.tv_sec += (micro_sec / 1000000);
			micro_sec = micro_sec % 1000000;
			ts_time.tv_nsec += (micro_sec * 1000);
			ts_time.tv_sec += (ts_time.tv_nsec / 1000000000);
			ts_time.tv_nsec = (ts_time.tv_nsec % 1000000000);
			return sem_timedwait(&semaphore_, &ts_time) == 0 ? true : false;
#else
			return true;
#endif
		}
	private:
#ifndef WIN32
		sem_t semaphore_;
#endif
	};

	class pthread_barrier
	{
	public:
		pthread_barrier(int num) : num_(num), count_(0)
		{
#ifndef WIN32
			pthread_cond_init(&condition, NULL);
			pthread_mutex_init(&mutex, NULL);
#endif
		}
		~pthread_barrier() 
		{
#ifndef WIN32
			pthread_cond_destroy(&condition);
			pthread_mutex_destroy(&mutex);
#endif
		}
		void arrive()
		{
#ifndef WIN32
			pthread_mutex_lock(&mutex);
			if(++count_ == num_)
			{
				printf("最后一个线程到达\n");
				pthread_cond_broadcast(&condition);
				count_ = 0;
			}
			else
			{
				printf("等待线程到达, count[%d]\n", count_);
				pthread_cond_wait(&condition, &mutex);
			}
			pthread_mutex_unlock(&mutex);
#endif
		}
	private:
		int num_;
#ifndef WIN32
		pthread_cond_t condition;
		pthread_mutex_t mutex;
#endif
		volatile int count_;
	};

}
}

#endif /* TS_SYS_H_ */
