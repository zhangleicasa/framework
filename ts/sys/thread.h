/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_SYS_THREAD_H_
#define TS_SYS_THREAD_H_

#include "public.h"
#include "noncopyable.h"

namespace ts {
namespace sys {
    //thread can not be copied.
    class thread : public noncopyable
    {
#ifdef LINUX
    public:
    	thread(): thread_((pthread_t)0), threadid_(0) {}
    	virtual ~thread() {}
        virtual bool init_instance() {return true;}
        virtual void exit_instance() {}
        /** create a thread by executing thread func(). **/
        virtual bool create()
        {
            if(thread_ != (pthread_t)0)
                return true;
            bool ret = true;
            ret = (::pthread_create(&thread_, NULL, &thread_func, this) == 0);
            return ret;
        }
        /** a function that thread can execute. **/
        static void *thread_func(void *params)
        {//params defines to class thread
            thread *pthread = (thread *)params;
            if(pthread->init_instance())
                pthread->run();
            pthread->exit_instance();
            pthread->thread_ = (pthread_t)0;
            return NULL;
        }
        /** join a thread. **/
        inline bool join()
        {
            return pthread_join(thread_, NULL) == 0;
        }
        //relay a run function, let thread to be virtual.
        virtual void run() = 0;
    private:
        pthread_t thread_;
        unsigned long threadid_;
#endif
    };
}//sys
}//ts

#endif
