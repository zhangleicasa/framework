#include "event_interupter.h"

namespace ts {
namespace event {
	event_interupter::event_interupter(business_reactor *dispatcher)
	{
		semaphore_ = new sys::event_semaphore();
		dispatcher->register_semaphore(semaphore_);
	}

	event_interupter::~event_interupter()
	{
		delete semaphore_;
	}

	void event_interupter::notify()
	{
		semaphore_->unlock();
	}
}
}


