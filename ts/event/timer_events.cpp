#include "timer_events.h"

namespace ts {
namespace event {
	void timer_events::register_timer(event_handler *handler, int id, int elapse)
	{
		timer_node node;
		node.handler_ = handler;
		node.timer_event_id_ = id;
		node.elapse_time_ = elapse;
		node.expire_ = node.expire_ + elapse;
		push(node);
	}

	void timer_events::remove_timer(event_handler *handler, int id)
	{
		for(int i = 0; i < size(); i++)
		{
			timer_node node = c[i];
			if(node.handler_ == handler)
			{
				node.handler_ = NULL;
			}
		}
	}

	void timer_events::expire(unsigned clock)
	{
		while(size() > 0)
		{
			timer_node node = top();
			if(node.expire_ > clock)
			{
				break;
			}
			pop();
			if(node.handler_ != NULL)
			{
				node.expire_ = clock + node.elapse_time_;
				push(node);
				node.handler_->on_timer(node.timer_event_id_);
			}
		}
	}
}
}

