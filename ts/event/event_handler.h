/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_EVENT_EVENT_HANDLER_H_
#define TS_EVENT_EVENT_HANDLER_H_

namespace ts {
namespace event {
	class event_dispatcher;
    class event_handler
    {
    public:
        event_handler(event_dispatcher *dispatcher);
        virtual ~event_handler();
        /** set the timer event by id and elapse, register it to event_dispatcher. **/
        void set_timer(int id, int elapse);
        /** post the event by id and parameter to event_dispatcher. **/
        void post_event(int eventid, int nparam, void *params);
        /** the followings is the interfaces to event handler. **/
		virtual int on_timer(int eventid) = 0;
        virtual int handle_input() = 0;
        virtual int handle_output() = 0;
        virtual int handle_event(int eventid, int nparam, void *params) = 0;

    private:
        event_dispatcher *event_dispatcher_;
    };

}
}

#endif
