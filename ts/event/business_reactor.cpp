#include "business_reactor.h"

namespace ts {
namespace event {
	business_reactor::business_reactor(int time_interval): time_interval_(time_interval)
	{
		semaphore_ = NULL;
	}

	business_reactor::~business_reactor()
	{

	}

	void business_reactor::register_semaphore(sys::event_semaphore *interupter)
	{
		semaphore_ = interupter;
	}

	void business_reactor::remove_semaphore(sys::event_semaphore *interupter)
	{
		semaphore_ = NULL;
	}

	void business_reactor::dispatch_io()
	{
		if(NULL != semaphore_)
		{
			semaphore_->time_wait(time_interval_);
		}
		handle_business();
	}
}
}
