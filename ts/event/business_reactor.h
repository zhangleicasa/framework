/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_EVENT_BUSINESS_REACTOR_H_
#define TS_EVENT_BUSINESS_REACTOR_H_

#include "synchronize.h"
#include "event_dispatcher.h"

namespace ts {
namespace event {
	class business_reactor: public event_dispatcher {
	public:
		business_reactor(int time_interval);
		virtual ~business_reactor();
		void register_semaphore(sys::event_semaphore *semaphore);
		void remove_semaphore(sys::event_semaphore *semaphore);
		virtual void dispatch_io();
	protected:
		virtual void handle_business() = 0;
	private:
		int time_interval_;
		sys::event_semaphore *semaphore_;
	};

}
}

#endif /* TS_EVENT_BUSINESS_REACTOR_H_ */
