#include "event_handler.h"
#include "epoll_reactor.h"

namespace ts {
namespace event {
    event_handler::event_handler(event_dispatcher *dispatcher)
    {
    	event_dispatcher_ = dispatcher;
    }
    
    event_handler::~event_handler()
    {
        
    }
    
    void event_handler::post_event(int eventid, int nparam, void *params)
    {//todo: we define post_event function always success.
    	event_dispatcher_->post_event(this, eventid, nparam, params);
    }

    void event_handler::set_timer(int id, int elapse)
    {
    	event_dispatcher_->register_timer(this, id, elapse);
    }

}
}
