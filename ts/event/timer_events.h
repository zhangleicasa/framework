/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_EVENT_TIMER_EVENTS_H_
#define TS_EVENT_TIMER_EVENTS_H_

#include "public.h"
#include "event_handler.h"

namespace ts {
namespace event {
	/** timer event node of the heap. **/
	typedef struct timer_node
	{
		event_handler *handler_;
		int timer_event_id_;
		int elapse_time_;          //time interval
		unsigned expire_;          //expire time
	}timer_node;

	/** compare time struct of timer event heap. **/
	struct compare_time : public std::binary_function<timer_node, timer_node, bool>
	{
		bool operator()(const timer_node &x, const timer_node &y)
		{
			return x.expire_ > y.expire_;
		}
	};

	typedef std::priority_queue<timer_node, std::vector<timer_node>, compare_time> ts_priority_queue;

	class timer_events : public ts_priority_queue
	{
	public:
		/** register timer event to timer event queue, elapse is the time interval. **/
		void register_timer(event_handler *handler, int id, int elapse);
		/** remove a timer event from timer event queue by id. **/
		void remove_timer(event_handler *handler, int id);
		/** check whether the timer events are over time, clock is now time. **/
		void expire(unsigned clock);
	};

}
}

#endif /* TS_EVENT_TIMER_EVENTS_H_ */
