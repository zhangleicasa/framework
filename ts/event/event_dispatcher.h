/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_EVENT_EVENT_DISPATCHER_H_
#define TS_EVENT_EVENT_DISPATCHER_H_

#include "public.h"
#include "thread.h"
#include "event_queue.h"
#include "timer_events.h"
#include "event_handler.h"

#define EVENT_QUEUE_SIZE 2048

namespace ts {
namespace event {
    class event_dispatcher : public ts::sys::thread
    {
    public:
        event_dispatcher();
        virtual ~event_dispatcher();
        /** execute events, io events and timer events. **/
        virtual void run();
        /** interface of dispatch io implementation. **/
        virtual void dispatch_io() = 0;
        /** dispatch ordinary events and execute. **/
        virtual void dispatch_events();
        /** register the timer event to timer event queue. **/
        void register_timer(event_handler *handler, int id, int elapse);
        /** post a ordinary event to the event queue. **/
        bool post_event(event_handler * handler, int eventid, int nparam, void *params);
    private:
        /** check whether the timer evens is over time. **/
        void check_timer();
    private:
        ts::common::event_queue *queue_;              //event queue.
        ts::event::timer_events *timer_events_;       //timer event queue.
    };

}
}

#endif
