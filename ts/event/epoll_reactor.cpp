#include "epoll_reactor.h"

namespace ts {
namespace event {
	epoll_reactor::epoll_reactor(const char *ip, int port)
    {
    	server_ = new net::ts_server(ip, port);
    }
    
	epoll_reactor::~epoll_reactor()
    {
    	delete server_;
    }

    void epoll_reactor::dispatch_io()
    {
    	server_->get_ids();
		server_->dispatch_io();
    }

    void epoll_reactor::register_flow(common::cache_flow *cache_flow)
    {
    	server_->register_flow(cache_flow);
    }

}
}

