/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_EVENT_REACTOR_H_
#define TS_EVENT_REACTOR_H_

#include "public.h"
#include "ts_server.h"
#include "socket_types.h"
#include "event_dispatcher.h"

namespace ts {
namespace event {
    class epoll_reactor : public event_dispatcher
    {
    public:
    	epoll_reactor(const char *ip, int port);
        virtual ~epoll_reactor();
        /** register the flow into the epoll reactor. **/
        void register_flow(common::cache_flow *cache_flow);
        /** implement dispatch_io interface of event dispatcher. **/
        virtual void dispatch_io();
    private:
        net::ts_server *server_;
    };

}
}

#endif
