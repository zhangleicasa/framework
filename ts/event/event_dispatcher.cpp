#include "event_dispatcher.h"

namespace ts {
namespace event {
    event_dispatcher::event_dispatcher()
    {
        queue_ = new ts::common::event_queue(EVENT_QUEUE_SIZE);
        timer_events_ = new ts::event::timer_events();
    }
    
    event_dispatcher::~event_dispatcher()
    {
    	delete queue_;
    	delete timer_events_;
    }
    
    void event_dispatcher::run()
    {
        while(true)
        {
            dispatch_events();
            dispatch_io();
            check_timer();
        }
    }
    
    void event_dispatcher::dispatch_events()
    {
        ts::common::event evt;
        while(queue_->pop_head_event(evt))
        {
            evt.handler_->handle_event(evt.eventid_, evt.nparam_, evt.params_);
        }
    }
    
    bool event_dispatcher::post_event(event_handler * handler, int eventid, int nparam, void *params)
    {
        queue_->add_to_queue(handler, eventid, nparam, params);
        return true;
    }

    void event_dispatcher::check_timer()
    {
#ifndef WIN32	
    	struct timeval timeout;
    	gettimeofday(&timeout, 0);
    	int clock = (timeout.tv_sec)*1000 + timeout.tv_usec/1000;
    	timer_events_->expire(clock);
#endif
    }

    void event_dispatcher::register_timer(event_handler *handler, int id, int elapse)
    {
    	timer_events_->register_timer(handler, id, elapse);
    }

}
}
