/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_EVENT_EVENT_INTERUPTER_H_
#define TS_EVENT_EVENT_INTERUPTER_H_

#include "public.h"
#include "business_reactor.h"

namespace ts {
namespace event {
	class event_interupter {
	public:
		event_interupter(business_reactor *dispatcher);
		virtual ~event_interupter();
		void notify();
	private:
		sys::event_semaphore *semaphore_;
	};
}
}

#endif /* TS_EVENT_EVENT_INTERUPTER_H_ */
