/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_SINGLETON_H_
#define TS_SINGLETON_H_

#include "noncopyable.h"
#include <stdio.h>

namespace ts {
    template<typename T>
    class singleton : public ts::noncopyable
    {
    protected:
        singleton() {};
        virtual ~singleton() {};
    
    public:
        static T* get_instance()
        {
            if(instance_ == NULL)
                instance_ = new T;
            return instance_;
        }
        
        static void release()
        {
            if(instance_ != NULL)
            {
                delete instance_;
                instance_ = NULL;
            }
        }
    
    private:
        static T* instance_;
    };
    
    template<typename T> T* singleton<T>::instance_ = NULL;

    template <typename TT>
    class tsingleton
    {
    protected:
    	tsingleton() {}
    	virtual ~tsingleton() {}
    public:
    	static TT* get_instance() {
    		static TT instance_;
    		return &instance_;
    	}
    };

}

#endif
