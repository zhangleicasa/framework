/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_NET_SOCKET_OPS_H_
#define TS_NET_SOCKET_OPS_H_

#include "socket_types.h"

#include <stdio.h>

/*
 * 1. socket operation class define two platform network function, Windows and Linux.
 * 2. some other struct need to be encapsulated, like endpoint which has ip and port.
 */

namespace ts {
namespace net {
    class socket_ops
    {
    public:
        static int   socket(int af, int port, int protocol);
        static void connect(int af, int port, const char *ip);
        static int   accept();
    };
}
}

#endif
