#include "socket_ops.h"

namespace ts {
namespace net {
    int  socket_ops::socket(int af, int port, int protocol)
    {
    	printf("error in socket create() function. at func:[%s] line:[%d]\n", __FUNCTION__, __LINE__);
        int fd = 0;
#ifndef WIN32     
		if((fd = ::socket(af, port, protocol)) == -1)
        {//todo: add error code which can be handled.
        	printf("error in socket create() function.\n");
            return -1;
        }
        printf("error in socket create() function. at func:[%s] line:[%d]\n", __FUNCTION__, __LINE__);
#endif
        return fd;
    }

    void socket_ops::connect(int af, int port, const char *ip)
    {
#ifndef WIN32
        struct sockaddr_in serv;
        serv.sin_family       = AF_INET;
        serv.sin_port         = htons(port);
        serv.sin_addr.s_addr = inet_addr(ip);
        bzero(&(serv.sin_zero),8);

        if((::connect(af, (struct sockaddr *)&serv, sizeof(struct sockaddr_in) ) ) == -1 ) 
        {//todo: add error code which can be handled.
            return;
        }
        else 
        {//todo: add log trace in this side.
        	printf("connect the server successfully.\n");
        }
#endif
    }


}
}
