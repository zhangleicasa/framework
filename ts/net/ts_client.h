/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_NET_TS_CLIENT_H_
#define TS_NET_TS_CLIENT_H_

#include "base_package.h"
#include "public.h"

namespace ts {
namespace net {
    class ts_client 
    {
    public:
        ts_client();
        void ts_send(protocol::base_package *data);
    private:
        int client_fd_;
    };
}
}

#endif /* TS_NET_TS_CLIENT_H_ */
