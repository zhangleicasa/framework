#include "channel.h"
#include "socket_types.h"

namespace ts {
namespace net {

	channel::channel(int id)
	{
		id_ = id;
	}

	int channel::read_data(mem::buffer *pbuffer)
	{
#ifndef WIN32
		return read(id_, pbuffer->address(), pbuffer->get_length());
#else
		return 0;
#endif
	}

}
}
