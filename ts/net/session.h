/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_NET_SESSION_H_
#define TS_NET_SESSION_H_

#include "channel.h"
#include "base_protocol.h"
#include "base_package.h"
#include "event_handler.h"

namespace ts {
namespace net {
	class session {
	public:
		session(channel *pchannel, common::cache_flow* cache_flow);
		~session();
		bool handle_input();
	private:
		channel *channel_;
		protocol::base_protocol *base_protocol_;
	};

}
}

#endif /* TS_NET_SESSION_H_ */
