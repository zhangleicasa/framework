/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_NET_SOCKET_TYPES_H_
#define TS_NET_SOCKET_TYPES_H_

#ifdef LINUX
    #include <sys/ioctl.h>
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <fcntl.h>
    #include <sys/socket.h>
    #include <sys/uio.h>
    #include <sys/un.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <netdb.h>
    #include <net/if.h>
    #include <limits.h>
    #include <unistd.h>
    #include <sys/types.h>
    #include <sys/ioctl.h>
    #include <netinet/tcp.h>
    #include <ifaddrs.h>
#else
    #include <winsock2.h>
#endif

namespace ts {
namespace net {
#ifdef LINUX
    typedef sockaddr_in net_v4;
#elif WIN32
#endif
}
}

#endif
