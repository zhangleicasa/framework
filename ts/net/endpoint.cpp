#include "endpoint.h"

namespace ts {
namespace net {
    endpoint::endpoint(int port)
    {
#ifdef LINUX
        /** this function willl be used in server side. **/
        memset(&net_type_, sizeof(net_type_), 0);
        net_type_.v4.sin_family         = AF_INET;
        net_type_.v4.sin_port           = port;
        net_type_.v4.sin_addr.s_addr    = INADDR_ANY;
#elif WIN32
#endif
    }

    endpoint::endpoint(const char *ip, int port)
    {
#ifdef LINUX
        /** this function will be used in client side. **/
        memset(&net_type_, sizeof(net_type_), 0);
        /* 
         * 1. sin_addr handle with the ip address.
         * 2. sin_family handle with AF_INET or AF_INET6.
         * 3. sin_port handle with the port server listern or client connect.
         * 4. sin_zero will be memset to 0.
         */
        net_type_.v4.sin_addr.s_addr    = inet_addr(ip);
        net_type_.v4.sin_family         = AF_INET;
        net_type_.v4.sin_port           = port;
#elif WIN32
#endif
    }

    endpoint::~endpoint()
    {
        
    }
}
}
