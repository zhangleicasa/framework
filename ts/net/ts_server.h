/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_NET_TS_SERVER_H_
#define TS_NET_TS_SERVER_H_

#include "public.h"
#include "channel.h"
#include "session.h"
#include "socket_ops.h"
#include "base_package.h"
#include "socket_types.h"

#define PACKAGE_SIZE  32

namespace ts {
namespace net {
	class ts_server {
	public:
		ts_server(const char *ip, int port);
		~ts_server();
		void get_ids();
		void dispatch_io();
		void register_flow(common::cache_flow *cache_flow);
	private:
		channel *create_channel();
	private:
		int epoll_fd_;
		unsigned listen_fd_;
		std::vector<net::session *> session_;
		common::cache_flow *cache_flow_;
	};
}
}

#endif /* TS_NET_TS_SERVER_H_ */
