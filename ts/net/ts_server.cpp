#include "ts_server.h"

namespace ts {
namespace net {
	ts_server::ts_server(const char *ip, int port)
		: epoll_fd_(0), listen_fd_(0), session_(NULL)
	{
#ifndef WIN32
		/** mostly support 1024 connections. **/
		epoll_fd_  = ::epoll_create(1024);
        listen_fd_ = ::socket(AF_INET, SOCK_STREAM, 0);
        int opt    = ::fcntl(listen_fd_, F_GETFL);
        if(opt < 0)
        	printf("error in fcntl() by listen fd.\n");
        else
        	opt    = opt | O_NONBLOCK;

        if(::fcntl(listen_fd_, F_SETFL, opt) < 0)
        	printf("error in fcntl() by listen fd.\n");

        struct sockaddr_in serveraddr;
        ::bzero(&serveraddr, sizeof(serveraddr));
        serveraddr.sin_family = AF_INET;
        serveraddr.sin_port   = htons(port);
        ::inet_aton(ip, &(serveraddr.sin_addr));
        ::bind(listen_fd_, (struct sockaddr *)&serveraddr, sizeof(serveraddr));
        ::listen(listen_fd_, 20);

        struct epoll_event evt;
        evt.data.fd = listen_fd_;
        evt.events  = EPOLLIN | EPOLLET;
        epoll_ctl(epoll_fd_, EPOLL_CTL_ADD, listen_fd_, &evt);

        cache_flow_ = NULL;
#endif
	}

	ts_server::~ts_server()
	{
#ifndef WIN32
		close(epoll_fd_);
		close(listen_fd_);
#endif
	}

	void ts_server::get_ids()
	{
#ifndef WIN32
		struct epoll_event event, events[20];
        socklen_t clilen;
		int nfds = ::epoll_wait(epoll_fd_, events, 20, 0);
		for(int i = 0; i< nfds; ++i)
		{
		   if(events[i].data.fd == listen_fd_)
			{
				channel *pchannel = create_channel();
				session_.push_back(new session(pchannel, cache_flow_));
			}
			else if(events[i].events & EPOLLIN)
			{
				if(events[i].data.fd < 0)
					continue;
			}
			else if(events[i].events & EPOLLOUT)
			{
				printf("hello EPOLLOUT\n");
			}
		}
#endif
	}

	void ts_server::dispatch_io()
	{
		std::vector<net::session *>::iterator it;
		for(it = session_.begin(); it != session_.end(); )
		{
			if(false == (*it)->handle_input())
				it = session_.erase(it);
			else
				it++;
		}
	}

	channel * ts_server::create_channel()
	{
#ifndef WIN32
        struct sockaddr_in clientaddr;
        socklen_t clilen;
		int socket_cons = ::accept(listen_fd_, (struct sockaddr *)&clientaddr, &clilen);
		if(socket_cons < 0)
			printf("[ERROR] add a socket file descriptor.\n");
		char *addr    = ::inet_ntoa(clientaddr.sin_addr);
		printf("receive the client address: [%s]\n", addr);
		struct epoll_event event;
		event.data.fd = socket_cons;
		event.events  = EPOLLIN | EPOLLET;
		epoll_ctl(epoll_fd_, EPOLL_CTL_ADD, socket_cons, &event);
		return new channel(socket_cons);
#else
		return NULL;
#endif
	}

	void ts_server::register_flow(common::cache_flow *cache_flow)
	{
		cache_flow_ = cache_flow;
	}
}
}

