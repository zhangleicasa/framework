/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_NET_SERVICE_BASE_H_
#define TS_NET_SERVICE_BASE_H_

class service_name {
public:
	service_name();
	virtual ~service_name();
};

class service_base {
public:
	service_base(service_name *name);
	virtual ~service_base();
};

#endif /* TS_NET_SERVICE_BASE_H_ */
