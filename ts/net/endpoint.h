/* 
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_NET_ENDPOINT_H_
#define TS_NET_ENDPOINT_H_

#include "socket_types.h"

/* encapsulate ip and port of the host. */
namespace ts {
namespace net {
    class endpoint
    {
    public:
        endpoint(int port);
        endpoint(const char *ip, int port);
        virtual ~endpoint();

    private:
        union net_type
        {
#ifdef LINUX
            struct sockaddr_in v4;
#elif WIN32
#endif
        }net_type_;
    };
}
}

#endif