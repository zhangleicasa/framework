/*
 * author: zhanglei
 *  email: zhangleicasa@gmail.com
 */
#ifndef TS_NET_CHANNEL_H_
#define TS_NET_CHANNEL_H_

#include "buffer.h"
#include "public.h"

namespace ts {
namespace net {
//	class c_channel {
//	public:
//		c_channel(int id);
//		virtual ~c_channel();
//		int read(int number, char *buffer);
//		int write(int number, char *buffer);
//	};

	class channel {
	public:
		channel(int id);
		int read_data(mem::buffer *pbuffer);
	private:
		int id_;
	};
}
}

#endif /* TS_NET_CHANNEL_H_ */
