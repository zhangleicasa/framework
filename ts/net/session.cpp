#include "session.h"

namespace ts {
namespace net {

	session::session(channel *pchannel, common::cache_flow* cache_flow)
	{
		channel_ = pchannel;
		base_protocol_ = new protocol::base_protocol(pchannel, cache_flow);
	}

	session::~session()
	{
		delete base_protocol_;
	}

	bool session::handle_input()
	{
		return base_protocol_->handle_input();
	}

}
}
