#include "ts_client.h"

namespace ts {
namespace net {
    ts_client::ts_client()
    {
#ifndef WIN32
        if((client_fd_ = socket(AF_INET, SOCK_STREAM, 0)) == -1)
        {//todo: add error code which can be handled.
        	printf("error in socket create() function.\n");
        }
        struct sockaddr_in serv;
        serv.sin_family       = AF_INET;
        serv.sin_port         = htons(5555);
        serv.sin_addr.s_addr  = ::inet_addr("127.0.0.1");
        ::bzero(&(serv.sin_zero), 8);
        if((::connect(client_fd_, (struct sockaddr *)&serv, sizeof(struct sockaddr_in) ) ) == -1 )
        {// todo: add error code which can be handled.
        	printf("connect the server failly.\n");
        }
        else
        {//todo: add log trace in this side.
        	printf("connect the server successfully.\n");
        }
#endif
    }

    void ts_client::ts_send(protocol::base_package *data)
    {
#ifndef WIN32
    	printf("%s:%d send in ts.\n", __FUNCTION__, __LINE__);
    	::send(client_fd_, data->address(), data->get_length(), 0);
#endif
    }
}
}
